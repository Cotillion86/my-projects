package com.webshop_backend.services;

import com.webshop_backend.dto.EditProductDto;
import com.webshop_backend.dto.ProductDto;
import com.webshop_backend.entities.ProductEntity;
import com.webshop_backend.repositories.ProductCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService {

    @Autowired
    ProductCrudRepository productCrudRepository;

    public ProductDto createProduct(EditProductDto editProductDto) {
        ProductEntity productEntity = new ProductEntity(editProductDto.getTitle(),
                editProductDto.getDescription(),
                editProductDto.getPrice(),
                editProductDto.getImageUrl());
        productCrudRepository.save(productEntity);

        return new ProductDto(productEntity.getProductId(),
                productEntity.getCreatedAt(),
                productEntity.getUpdatedAt(),
                productEntity.getTitle(),
                productEntity.getDescription(),
                productEntity.getPrice(),
                productEntity.getImageUrl());


    }

    public List getProductList() {
        Iterable<ProductEntity> productEntityIterable = productCrudRepository.findAll();
        List productDtoList = new ArrayList<ProductDto>();
        for (ProductEntity productEntity : productEntityIterable) {
            ProductDto productDto = new ProductDto(productEntity.getProductId(),
                    productEntity.getCreatedAt(),
                    productEntity.getUpdatedAt(),
                    productEntity.getTitle(),
                    productEntity.getDescription(),
                    productEntity.getPrice(),
                    productEntity.getImageUrl());

            productDtoList.add(productDto);
        }

        return productDtoList;
    }

    public ProductDto getProductById(int productId) {
        Optional<ProductEntity> optionalProductEntity = productCrudRepository.findById(productId);
        ProductEntity productEntity;
        if (optionalProductEntity.isPresent()) {
            productEntity = optionalProductEntity.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        return new ProductDto(productEntity.getProductId(),
                productEntity.getCreatedAt(),
                productEntity.getUpdatedAt(),
                productEntity.getTitle(),
                productEntity.getDescription(),
                productEntity.getPrice(),
                productEntity.getImageUrl());
    }

    public void deleteProductById(int productId) {
        try {
            productCrudRepository.deleteById(productId);
        } catch (Exception err) {
            System.out.println(err.getMessage());
        }

    }

    public ProductDto editProductById(int productId, EditProductDto editProductDto) {
        Optional<ProductEntity> optionalProductEntity = productCrudRepository.findById(productId);
        ProductEntity productEntity;
        if (optionalProductEntity.isPresent()) {
            productEntity = optionalProductEntity.get();
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }

        productEntity.setTitle(editProductDto.getTitle());
        productEntity.setDescription(editProductDto.getDescription());
        productEntity.setPrice(editProductDto.getPrice());
        productEntity.setImageUrl(editProductDto.getImageUrl());
        productEntity.setUpdatedAt(new Timestamp(System.currentTimeMillis()));

        productCrudRepository.save(productEntity);

        return new ProductDto(productEntity.getProductId(),
                productEntity.getCreatedAt(),
                productEntity.getUpdatedAt(),
                productEntity.getTitle(),
                productEntity.getDescription(),
                productEntity.getPrice(),
                productEntity.getImageUrl());

    }

}






