package com.webshop_backend.controller;


import com.webshop_backend.dto.ProductDto;
import com.webshop_backend.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.webshop_backend.dto.*;

import java.util.List;

@RestController
@RequestMapping("api/products")
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping
    private ProductDto postProduct(@RequestBody EditProductDto editProductDto) {
        return (productService.createProduct(editProductDto));
    }

    @GetMapping
    private List getProductList() {
        return productService.getProductList();
    }

    @GetMapping("{productId}")
    private ProductDto getProductById(@PathVariable int productId) {
        return productService.getProductById(productId);
    }

    @DeleteMapping("{productId}")
    private void deleteProductById(@PathVariable int productId) {
        productService.deleteProductById(productId);
    }

    @PutMapping("{productId}")
    private ProductDto editProductById(@PathVariable int productId, @RequestBody EditProductDto editProductDto) {
        return (productService.editProductById(productId, editProductDto));

    }


}
