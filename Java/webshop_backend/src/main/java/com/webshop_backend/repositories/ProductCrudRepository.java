package com.webshop_backend.repositories;

import com.webshop_backend.entities.ProductEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductCrudRepository extends CrudRepository<ProductEntity, Integer> {
}
