package com.webshop_backend.dto;

public class EditBasketDto {
    private AddressDto invoiceAddress;
    private AddressDto deliveryAddress;
    private String shippingType;

    public EditBasketDto() {
    }

    public EditBasketDto(AddressDto invoiceAddress, AddressDto deliveryAddress, String shippingType) {
        this.invoiceAddress = invoiceAddress;
        this.deliveryAddress = deliveryAddress;
        this.shippingType = shippingType;
    }

    public AddressDto getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(AddressDto invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public AddressDto getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressDto deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }
}
