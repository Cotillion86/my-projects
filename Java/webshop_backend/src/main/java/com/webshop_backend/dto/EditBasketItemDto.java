package com.webshop_backend.dto;

public class EditBasketItemDto {

    private int amount;
    private String remark;

    public EditBasketItemDto() {
    }

    public EditBasketItemDto(int amount, String remark) {
        this.amount = amount;
        this.remark = remark;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
