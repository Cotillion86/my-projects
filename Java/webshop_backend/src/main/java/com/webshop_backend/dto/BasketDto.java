package com.webshop_backend.dto;

import java.sql.Timestamp;

public class BasketDto {

    private int basketId;
    private int userId;
    private BasketItemDto items;
    private Timestamp createdAt;
    private Timestamp updatedAt;
    private AddressDto invoiceAddress;
    private AddressDto deliveryAddress;
    private String shippingType;

    public BasketDto(int basketId, int userId, BasketItemDto items, Timestamp createdAt, Timestamp updatedAt, AddressDto invoiceAddress, AddressDto deliveryAddress, String shippingType) {
        this.basketId = basketId;
        this.userId = userId;
        this.items = items;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
        this.invoiceAddress = invoiceAddress;
        this.deliveryAddress = deliveryAddress;
        this.shippingType = shippingType;
    }

    public BasketDto() {
    }

    public int getBasketId() {
        return basketId;
    }

    public void setBasketId(int basketId) {
        this.basketId = basketId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public BasketItemDto getItems() {
        return items;
    }

    public void setItems(BasketItemDto items) {
        this.items = items;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public AddressDto getInvoiceAddress() {
        return invoiceAddress;
    }

    public void setInvoiceAddress(AddressDto invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
    }

    public AddressDto getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(AddressDto deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getShippingType() {
        return shippingType;
    }

    public void setShippingType(String shippingType) {
        this.shippingType = shippingType;
    }
}
