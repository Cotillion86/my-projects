package com.webshop_backend.dto;

public class AuthenticationDto {

    private String jwt;
    private UserDto user;

    public AuthenticationDto(String jwt, UserDto user) {
        this.jwt = jwt;
        this.user = user;
    }

    public AuthenticationDto() {
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
