package com.webshop_backend.dto;

public class BasketItemDto {

    private int productId;
    private int amount;
    private String remark;

    public BasketItemDto(int productId, int amount, String remark) {
        this.productId = productId;
        this.amount = amount;
        this.remark = remark;
    }

    public BasketItemDto() {
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
