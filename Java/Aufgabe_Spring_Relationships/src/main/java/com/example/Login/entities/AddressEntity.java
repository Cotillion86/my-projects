package com.example.Login.entities;

import jakarta.persistence.*;

@Entity
@Table(name = "Address")
public class AddressEntity {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private int addressId;
    private String street;
    private String houseNumber;
    private String stairway;
    private String doorNumber;
    private String postCode;
    private String city;

    @ManyToOne
    @JoinColumn(name = "userId")
    private UserEntity userEntity;

    public AddressEntity() {
    }

    public AddressEntity( String street, String houseNumber, String stairway, String doorNumber, String postCode, String city, UserEntity userEntity) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.stairway = stairway;
        this.doorNumber = doorNumber;
        this.postCode = postCode;
        this.city = city;
        this.userEntity = userEntity;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getStairway() {
        return stairway;
    }

    public void setStairway(String stairway) {
        this.stairway = stairway;
    }

    public String getDoorNumber() {
        return doorNumber;
    }

    public void setDoorNumber(String doorNumber) {
        this.doorNumber = doorNumber;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }
}
