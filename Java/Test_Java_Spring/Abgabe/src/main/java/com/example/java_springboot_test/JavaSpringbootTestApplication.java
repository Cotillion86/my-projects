package com.example.java_springboot_test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaSpringbootTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaSpringbootTestApplication.class, args);
    }

}
