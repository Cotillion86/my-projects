package com.example.java_springboot_test.entities;


import com.example.java_springboot_test.enums.SubjectEnum;
import jakarta.persistence.*;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class TeacherEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int teacherId;

    private String name;
    private int age;
    private String subject;

    @OneToMany(mappedBy = "teacherEntity")
    private List<LessonEntity> lessonEntityList = new ArrayList<>();


    public TeacherEntity() {
    }

    public TeacherEntity(String name, int age, String subject) {
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public List<LessonEntity> getLessonEntityList() {
        return lessonEntityList;
    }

    public void setLessonEntityList(List<LessonEntity> lessonEntityList) {
        this.lessonEntityList = lessonEntityList;
    }
}
