package com.example.java_springboot_test.dto;

public class CreateTeacherDto {


    private String name;
    private int age;
    private String subject;

    public CreateTeacherDto(String name, int age, String subject) {
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public CreateTeacherDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
