package com.example.java_springboot_test.dto;

import java.util.ArrayList;
import java.util.List;

public class TeacherListDto {

    private List teacherList = new ArrayList<TeacherDto>();

    public TeacherListDto(List teacherList) {
        this.teacherList = teacherList;
    }

    public List getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List teacherList) {
        this.teacherList = teacherList;
    }
}
