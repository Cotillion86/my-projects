package com.example.java_springboot_test.dto;

public class CreateStudentDto {

    private String name;
    private int age;
    private int socialSecurityNumber;
    private int lessonId;

    public CreateStudentDto() {
    }

    public CreateStudentDto(String name, int age, int socialSecurityNumber, int lessonId) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
        this.lessonId = lessonId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(int socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }


}
