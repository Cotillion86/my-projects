package com.example.java_springboot_test.services;

import com.example.java_springboot_test.dto.CreateTeacherDto;
import com.example.java_springboot_test.dto.EditTeacherDto;
import com.example.java_springboot_test.dto.TeacherDto;
import com.example.java_springboot_test.dto.TeacherListDto;
import com.example.java_springboot_test.entities.TeacherEntity;
import com.example.java_springboot_test.repositories.TeacherCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@Service
public class TeacherService {

    @Autowired
    TeacherCrudRepository teacherCrudRepository;

    public TeacherDto createTeacher(CreateTeacherDto createTeacherDto) {
        TeacherEntity teacherEntity = new TeacherEntity(
                createTeacherDto.getName(),
                createTeacherDto.getAge(),
                createTeacherDto.getSubject());

        teacherCrudRepository.save(teacherEntity);

        return (createTeacherDtoFromTeacherEntity(teacherEntity));
    }

    public TeacherListDto getTeacherList() {
        Iterable<TeacherEntity> teacherEntityIterable = teacherCrudRepository.findAll();
        TeacherListDto teacherListDto = new TeacherListDto(new ArrayList<TeacherDto>());

        for (TeacherEntity userEntity: teacherEntityIterable) {
            teacherListDto.getTeacherList().add(createTeacherDtoFromTeacherEntity(userEntity));
        }
        return teacherListDto;
    }


    public TeacherDto getTeacherById(int teacherId) {
        TeacherEntity teacherEntity = getTeacherEntity(teacherId);

        return createTeacherDtoFromTeacherEntity(teacherEntity);
    }


    public TeacherDto editTeacher(EditTeacherDto editTeacherDto, int teacherId) {
        TeacherEntity teacherEntity = getTeacherEntity(teacherId);

        teacherEntity.setAge(editTeacherDto.getAge());
        teacherEntity.setName(editTeacherDto.getName());

        if (!teacherEntity.getSubject().isBlank() && !teacherEntity.getSubject().equals(editTeacherDto.getSubject())) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Das Unterrichtsfach darf nicht geändert werden");
        } else {

            teacherEntity.setSubject(editTeacherDto.getSubject());
        }
        teacherCrudRepository.save(teacherEntity);

        return createTeacherDtoFromTeacherEntity(teacherEntity);

    }



    private TeacherEntity getTeacherEntity(int teacherId) {
        TeacherEntity teacherEntity;

        Optional<TeacherEntity> optionalTeacherEntity = teacherCrudRepository.findById(teacherId);

        if (optionalTeacherEntity.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
             teacherEntity = optionalTeacherEntity.get();
        }
        return teacherEntity;
    }

    private TeacherDto createTeacherDtoFromTeacherEntity(TeacherEntity teacherEntity) {
        return new TeacherDto(teacherEntity.getTeacherId(),
                teacherEntity.getName(),
                teacherEntity.getAge(),
                teacherEntity.getSubject());
    }
}
