package com.example.java_springboot_test.controller;

import com.example.java_springboot_test.dto.CreateStudentDto;
import com.example.java_springboot_test.dto.StudentDto;
import com.example.java_springboot_test.dto.UpdateStudentDto;
import com.example.java_springboot_test.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/student")

public class StudentController {

    @Autowired
    StudentService studentService;

    @PostMapping
    public ResponseEntity<StudentDto> createStudent(@RequestBody CreateStudentDto createStudentDto) {
        return new ResponseEntity<>(studentService.createStudent(createStudentDto), HttpStatus.CREATED);
    }

    @PutMapping("{studentId}")
    public ResponseEntity<StudentDto> updateStudent(@RequestBody UpdateStudentDto updateStudentDto, @PathVariable int studentId) {
        return new ResponseEntity<>(studentService.updateStudent(updateStudentDto, studentId), HttpStatus.ACCEPTED);
    }

}
