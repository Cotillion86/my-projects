package com.example.java_springboot_test.enums;

public enum SubjectEnum {
    JAVA("Java"),
    WEB("Webtechnologie"),
    DB("Datenbanken");

    private String subject;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    SubjectEnum(String subject) {
        this.subject = subject;


    }
}
