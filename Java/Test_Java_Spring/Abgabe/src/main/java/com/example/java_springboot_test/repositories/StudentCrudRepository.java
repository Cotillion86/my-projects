package com.example.java_springboot_test.repositories;

import com.example.java_springboot_test.entities.StudentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface StudentCrudRepository extends CrudRepository<StudentEntity, Integer> {
}
