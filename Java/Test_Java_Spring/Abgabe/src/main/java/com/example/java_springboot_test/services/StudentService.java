package com.example.java_springboot_test.services;

import com.example.java_springboot_test.dto.*;
import com.example.java_springboot_test.entities.LessonEntity;
import com.example.java_springboot_test.entities.StudentEntity;
import com.example.java_springboot_test.entities.TeacherEntity;
import com.example.java_springboot_test.repositories.LessonCrudRepository;
import com.example.java_springboot_test.repositories.StudentCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentCrudRepository studentCrudRepository;

    @Autowired
    LessonCrudRepository lessonCrudRepository;

    public StudentDto createStudent(CreateStudentDto createStudentDto) {

        LessonEntity lessonEntity = getLessonEntity(createStudentDto.getLessonId());


        StudentEntity studentEntity = new StudentEntity();

        studentEntity.setAge(createStudentDto.getAge());
        studentEntity.setName(createStudentDto.getName());
        studentEntity.setSocialSecurityNumber(createStudentDto.getSocialSecurityNumber());
        studentEntity.setLessonEntity(lessonEntity);

        lessonEntity.getStudentEntityList().add(studentEntity);
        lessonCrudRepository.save(lessonEntity);
        studentCrudRepository.save(studentEntity);



        return createStudentDtoFromStudentEntity(studentEntity);


    }

    public StudentDto updateStudent(UpdateStudentDto updateStudentDto, int studentId) {
        StudentEntity studentEntity;
        Optional<StudentEntity> optionalStudentEntity = studentCrudRepository.findById(studentId);
        if (!optionalStudentEntity.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            studentEntity = optionalStudentEntity.get();
        }

        LessonEntity lessonEntity = getLessonEntity(updateStudentDto.getLessonId());
        studentEntity.setLessonEntity(lessonEntity);
        studentCrudRepository.save(studentEntity);

        return createStudentDtoFromStudentEntity(studentEntity);
    }


    private LessonEntity getLessonEntity(int lessonId) {
        LessonEntity lessonEntity;
        Optional<LessonEntity> lessonEntityOptional = lessonCrudRepository.findById(lessonId);
        if (!lessonEntityOptional.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            lessonEntity = lessonEntityOptional.get();
        }

        return lessonEntity;

    }

    private StudentDto createStudentDtoFromStudentEntity(StudentEntity studentEntity) {
        StudentDto studentDto = new StudentDto(
                studentEntity.getStudentId(),
                studentEntity.getName(),
                studentEntity.getAge(),
                studentEntity.getSocialSecurityNumber(),
                studentEntity.getLessonEntity().getLessonId());


        return studentDto;
    }
}
