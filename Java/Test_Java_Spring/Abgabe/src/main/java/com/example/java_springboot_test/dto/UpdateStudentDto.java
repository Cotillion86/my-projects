package com.example.java_springboot_test.dto;

public class UpdateStudentDto {

    private int lessonId;

    public UpdateStudentDto(int lessonId) {
        this.lessonId = lessonId;
    }

    public UpdateStudentDto() {
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }
}
