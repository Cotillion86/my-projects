package com.example.java_springboot_test.entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table
public class LessonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int lessonId;

    private String startDate;
    private int duration;
    private String subject;

    @ManyToOne
    @JoinColumn(name = "teacherId")
    private TeacherEntity teacherEntity;

    @OneToMany(mappedBy = "lessonEntity")
    private List<StudentEntity> studentEntityList = new ArrayList<>();

    public LessonEntity() {
    }

    public LessonEntity(String startDate, int duration, String subject, TeacherEntity teacherEntity) {
        this.lessonId = lessonId;
        this.startDate = startDate;
        this.duration = duration;
        this.subject = subject;
        this.teacherEntity = teacherEntity;
    }

    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public TeacherEntity getTeacherEntity() {
        return teacherEntity;
    }

    public void setTeacherEntity(TeacherEntity teacherEntity) {
        this.teacherEntity = teacherEntity;
    }

    public List<StudentEntity> getStudentEntityList() {
        return studentEntityList;
    }

    public void setStudentEntityList(List<StudentEntity> studentEntityList) {
        this.studentEntityList = studentEntityList;
    }
}
