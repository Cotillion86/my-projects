package com.example.java_springboot_test.dto;

public class TeacherDto {

    private int teacherId;
    private String name;
    private int age;
    private String subject;

    public TeacherDto() {
    }

    public TeacherDto(int teacherId, String name, int age, String subject) {
        this.teacherId = teacherId;
        this.name = name;
        this.age = age;
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
