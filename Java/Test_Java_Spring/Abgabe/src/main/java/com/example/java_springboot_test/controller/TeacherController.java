package com.example.java_springboot_test.controller;

import com.example.java_springboot_test.dto.CreateTeacherDto;
import com.example.java_springboot_test.dto.EditTeacherDto;
import com.example.java_springboot_test.dto.TeacherDto;
import com.example.java_springboot_test.dto.TeacherListDto;
import com.example.java_springboot_test.services.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/teacher")
public class TeacherController {

    @Autowired
    TeacherService teacherService;

    @PostMapping
    public ResponseEntity<TeacherDto> createTeacher(@RequestBody CreateTeacherDto createTeacherDto) {
        return new ResponseEntity<>(teacherService.createTeacher(createTeacherDto), HttpStatus.CREATED);

    }

    @GetMapping
    public ResponseEntity<TeacherListDto> getTeacherList() {
        return new ResponseEntity<>(teacherService.getTeacherList(), HttpStatus.ACCEPTED);
    }

    @GetMapping("{teacherId}")
    public ResponseEntity<TeacherDto> getTeacherById(@PathVariable int teacherId) {
        return new ResponseEntity<>(teacherService.getTeacherById(teacherId), HttpStatus.FOUND);

    }

    @PutMapping("{teacherId}")
    public ResponseEntity<TeacherDto> editTeacher(@RequestBody EditTeacherDto editTeacherDto, @PathVariable int teacherId) {
        return new ResponseEntity<>(teacherService.editTeacher(editTeacherDto, teacherId), HttpStatus.ACCEPTED);

    }
}
