package com.example.java_springboot_test.services;

import com.example.java_springboot_test.dto.*;
import com.example.java_springboot_test.entities.LessonEntity;
import com.example.java_springboot_test.entities.TeacherEntity;
import com.example.java_springboot_test.repositories.LessonCrudRepository;
import com.example.java_springboot_test.repositories.TeacherCrudRepository;
import org.apache.catalina.authenticator.SingleSignOn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class LessonService {

    @Autowired
    LessonCrudRepository lessonCrudRepository;
    @Autowired
    TeacherCrudRepository teacherCrudRepository;

    public LessonDto createLesson(CreateLessonDto createLessonDto) {
        TeacherEntity teacherEntity = getTeacherEntity(createLessonDto.getTeacherId());

        if (!teacherEntity.getSubject().equals(createLessonDto.getSubject())) {

            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        LessonEntity lessonEntity = new LessonEntity();

        lessonEntity.setTeacherEntity(teacherEntity);
        lessonEntity.setDuration(createLessonDto.getDuration());
        lessonEntity.setStartDate(createLessonDto.getStartDate());
        lessonEntity.setSubject(createLessonDto.getSubject());

        teacherEntity.getLessonEntityList().add(lessonEntity);
        teacherCrudRepository.save(teacherEntity);


        lessonCrudRepository.save(lessonEntity);

        return createLessonDtoFromLessonEntity(lessonEntity);


    }

    public LessonListDto getLessonList() {
        Iterable<LessonEntity> lessonEntityIterable = lessonCrudRepository.findAll();
        LessonListDto lessonListDto = new LessonListDto(new ArrayList<LessonDto>());

        for (LessonEntity lessonEntity : lessonEntityIterable) {
            lessonListDto.getLessonList().add(createLessonDtoFromLessonEntity(lessonEntity));
        }
        return lessonListDto;
    }

    public void deleteLesson(int lessonId) {
        LessonEntity lessonEntity;
        Optional<LessonEntity> optionalLessonEntity= lessonCrudRepository.findById(lessonId);
        if (!optionalLessonEntity.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            lessonEntity = optionalLessonEntity.get();
        }

        if (lessonEntity.getStudentEntityList().isEmpty()) {
            lessonCrudRepository.delete(lessonEntity);
        } else {
            throw new ResponseStatusException(HttpStatus.CONFLICT);
        }


    }


    private LessonDto createLessonDtoFromLessonEntity(LessonEntity lessonEntity) {
        return new LessonDto(lessonEntity.getLessonId(),
                lessonEntity.getStartDate(),
                lessonEntity.getDuration(),
                lessonEntity.getSubject(),
                lessonEntity.getTeacherEntity().getTeacherId());

    }

    private TeacherEntity getTeacherEntity(int teacherId) {
        TeacherEntity teacherEntity;

        Optional<TeacherEntity> optionalTeacherEntity = teacherCrudRepository.findById(teacherId);

        if (optionalTeacherEntity.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } else {
            teacherEntity = optionalTeacherEntity.get();
        }
        return teacherEntity;

    }

}
