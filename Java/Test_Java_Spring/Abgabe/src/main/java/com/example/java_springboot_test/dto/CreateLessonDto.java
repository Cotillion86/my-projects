package com.example.java_springboot_test.dto;

public class CreateLessonDto {

    private String startDate;
    private int duration;
    private String subject;
    private int teacherId;

    public CreateLessonDto() {
    }

    public CreateLessonDto(String startDate, int duration, String subject, int teacherId) {
        this.startDate = startDate;
        this.duration = duration;
        this.subject = subject;
        this.teacherId = teacherId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }
}
