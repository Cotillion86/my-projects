package com.example.java_springboot_test.dto;

import java.util.ArrayList;
import java.util.List;

public class LessonListDto {

    private List<LessonDto> lessonList = new ArrayList<>();

    public LessonListDto() {
    }

    public LessonListDto(List<LessonDto> lessonList) {
        this.lessonList = lessonList;
    }

    public List<LessonDto> getLessonList() {
        return lessonList;
    }

    public void setLessonList(List<LessonDto> lessonList) {
        this.lessonList = lessonList;
    }
}
