package com.example.java_springboot_test.controller;

import com.example.java_springboot_test.dto.CreateLessonDto;
import com.example.java_springboot_test.dto.LessonDto;
import com.example.java_springboot_test.dto.LessonListDto;
import com.example.java_springboot_test.services.LessonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/lesson")
public class LessonController {

     @Autowired
    LessonService lessonService;

    @PostMapping
    public ResponseEntity<LessonDto> createLesson(@RequestBody CreateLessonDto createLessonDto) {
        return new ResponseEntity<>(lessonService.createLesson(createLessonDto), HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<LessonListDto> getLessonList() {
        return new ResponseEntity<>(lessonService.getLessonList(), HttpStatus.FOUND);
    }

    @DeleteMapping("{lessonId}")
    public void deleteLesson(@PathVariable int lessonId) {
        lessonService.deleteLesson(lessonId);

    }
}
