package animals;

public class Chicken extends Animal {

    private int eggs;
    private  final int minimumWeight = 1;
    private  final int minimumAge = 1;
    private  final int maximumWeight = 6;
    private  final int maximumAge = 10;

    public Chicken(String name, char sex, int age, int weight) {
        super(name, sex, age, weight);
        this.eggs = 0;
    }

    public Chicken() {
        this.eggs = 0;
    }

    @Override
    public int feed(int food) {
        while (food > 0 && getWeight() < getMaximumWeight()) {
            if (getSex() == 'm') {
            decreaseHunger(95);
            } else {
                decreaseHunger(90);
            }
            if (getHunger() < 0) {
                increaseWeight();
                increaseHunger(100);
            }
            food--;
        }
        return food;
    }
    @Override
    public String toString() {
        return "Spezies: Huhn || " + super.toString() + " || Eier: " + this.eggs + " Stück";
    }

    /*----------------------------------------------INCREASER/DECREASER----------------------------------------*/

    public void increaseProduct() {
        if (getSex() == 'w') {
            this.eggs++;
        }
    }
    public void increaseProduct(int amount) {
        if (getSex() == 'w') {
            this.eggs += amount;
        }
    }

    public  void decreaseProduct(int amount) {
        this.eggs -= amount;
    }

    /*-----------------------------------------------GETTER/SETTER---------------------------------------------*/

    public double getMeat() {
        return (this.weight * 0.5);
    }
    public  int getMaximumAge() {
        return maximumAge;
    }
    public  int getMaximumWeight() {
        return maximumWeight;
    }
    public  int getMinimumAge() {
        return minimumAge;
    }
    public  int getMinimumWeight() {
        return minimumWeight;
    }

    public void setProduct(int product) {
        setEggs(product);
    }

    public int getProduct() {
        return getEggs();
    }

    public int getEggs() {
        return eggs;
    }


    public void setEggs(int eggs) {
        if (sex == 'w') {
            this.eggs = eggs;
        } else {
            System.out.println("Das Tier ist kein Huhn");
        }
    }


}
