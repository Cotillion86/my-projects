package animals;

import java.util.Random;

public abstract class Animal {
    protected String name;
    protected char sex;
    protected int age;
    protected int hunger;
    protected int weight;

    public Animal(String name, char sex, int age, int weight) {
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.weight = weight;
        Random rand = new Random();
        this.hunger = rand.nextInt(100 - 25) + 25;
    }

    public Animal() {
        Random rand = new Random();
        this.hunger = rand.nextInt(100 - 25) + 25;
    }

    public int feed(int food) {
        return 0;
    }

    public String alertStatusAnimal() {
        if (getHunger() > 15 && getWeight() <= getMinimumWeight()) {
                return getName();
        }
        return "";
    }

    public String isStarved() {
        if (getWeight() < getMinimumWeight()) {
            return getName();
        }
        return "";
    }

    public String toString() {
        return "Name: " + name + " || Geschlecht: " + sex + " || Alter: " + age + " Jahre || Hunger: " + hunger + " || Gewicht: " + weight + "kg";
    }

    /* ---------------------------------------------------- INCREASER/DECREASER --------------------------------*/

    public void increaseHunger() {
        this.hunger += 20;
    }

    public void increaseHunger(int amount) {
        this.hunger += amount;
    }

    public void decreaseHunger() {
        this.hunger -= 10;
    }

    public void decreaseHunger(int amount) {
        this.hunger -= amount;
    }

    public void increaseWeight() {
        this.weight++;
    }

    public void decreaseWeight() {
        this.weight--;
    }

    public abstract void increaseProduct(int amount);
    public abstract void increaseProduct();

    public abstract void decreaseProduct(int amount);



    /* ------------------------------------------------ GETTER/SETTER/-------------------------------------*/

    public abstract double  getMeat();
    public abstract int getMinimumWeight();

    public abstract int getMinimumAge();

    public abstract int getMaximumAge();

    public abstract int getMaximumWeight();

    public abstract void setProduct(int amount);

    public abstract int getProduct();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
