package animals;

public class Sheep extends Animal {

    public int wool;
    private  final int minimumWeight = 3;
    private  final int minimumAge = 1;
    private  final int maximumWeight = 200;
    private  final int maximumAge = 12;

    public Sheep(String name, char sex, int age, int weight) {
        super(name, sex, age, weight);
        this.wool = 0;
    }
    public Sheep() {
        this.wool = 0;
    }
    @Override
    public int feed(int food) {
        while (food > 0 && getWeight() < getMaximumWeight()) {
            decreaseHunger(70);
            if (getHunger() < 0) {
                increaseWeight();
                increaseHunger(100);
            }
            food--;
        }
        return food;
    }

    @Override
    public String toString() {
        return "Spezies: Schaf || " + super.toString() + " ||  Wolle: " + this.wool + "kg";
    }

    /*-------------------------------------------------INCREASER/DECREASER-----------------------------------------------------*/

    public void increaseProduct(int amount) {
        this.wool += amount;
    }

    public void increaseProduct() {
        this.wool ++;
    }

    public  void decreaseProduct(int amount) {
        this.wool -= amount;
    }

    /*-------------------------------------------------GETTER/SETTER------------------------------------------------------------*/
    public double getMeat() {
        return (this.weight * 0.7);
    }
    public  int getMaximumAge() {
        return maximumAge;
    }
    public  int getMaximumWeight() {
        return maximumWeight;
    }
    public  int getMinimumAge() {
        return minimumAge;
    }
    public int getMinimumWeight() {
        return minimumWeight;
    }
    public void setProduct(int product) {
        setWool(product);
    }
    public int getProduct() {
        return getWool();
    }
    public int getWool() {
        return wool;
    }
    public void setWool(int wool) {
        this.wool = wool;
    }
}
