package animals;

public class Cow extends Animal {
    private int milk;
    private  final int minimumWeight = 5;
    private  final int minimumAge = 1;
    private  final int maximumWeight = 700;
    private  final int maximumAge = 6;

    public Cow(String name, char sex, int age, int weight) {
        super(name, sex, age, weight);
        this.milk = 0;
    }
    public Cow() {
        this.milk = 0;

    }
    @Override
    public int feed(int food) {
        while (food > 0 && getWeight() < getMaximumWeight()) {
            if (getSex() == 'm') {
            decreaseHunger(60);
            } else {
                decreaseHunger(50);
            }
            if (getHunger() < 0) {
                increaseWeight();
                increaseHunger(100);
            }
            food--;
        }
        return food;
    }


    @Override
    public String toString() {
        return "Spezies: Kuh || " + super.toString() + " ||  Milch: " + this.milk + " Liter";
    }

    /*------------------------------------------INCREASER/DECREASER-----------------------------------*/

    public void increaseProduct(int amount) {
        if (getSex() == 'w') {
            this.milk += amount;
        }
    }
    public void increaseProduct( ) {
        if (getSex() == 'w') {
            this.milk ++;
        }
    }

    public  void decreaseProduct(int amount) {
        this.milk -= amount;
    }
    /*------------------------------------------GETTER/SETTER-----------------------------------------*/
    public double getMeat() {
        return (this.weight * 0.9);
    }
    public  int getMaximumAge() {
        return maximumAge;
    }
    public  int getMaximumWeight() {
        return maximumWeight;
    }
    public  int getMinimumAge() {
        return minimumAge;
    }
    public  int getMinimumWeight() {
        return minimumWeight;
    }

    public void setProduct(int milk) {
        setMilk(milk);
    }

    public int getProduct() {
        return getMilk();
    }

    public int getMilk() {
        return milk;
    }


    public void setMilk(int milk) {
        if (sex == 'w') {
            this.milk = milk;
        } else {
        }

    }
}
