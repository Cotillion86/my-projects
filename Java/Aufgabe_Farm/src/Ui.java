import animals.*;

import gebaeude.*;

import java.util.ArrayList;

import java.util.Scanner;

public class Ui {
    private Stall stall;
    private Lager lager;
    private Markt markt;
    private int count = 0;

    public Ui(Stall stall, Lager lager, Markt markt) {
        this.stall = stall;
        this.lager = lager;
        this.markt = markt;
    }

    public void startUI() {
        String input;
        do {
            Scanner scan = new Scanner(System.in);
            System.out.println("Runde: " + count);
            System.out.println("Was möchten Sie tun?");
            System.out.println("1: Gehe zum Stall.");
            System.out.println("2: Gehe zum Lager. Achten Sie darauf, dass die Tiere ausreichend gefüttert sind!");
            System.out.println("3: Gehe zum Markt. Achten Sie darauf, dass die Tiere ausreichend gefüttert sind!");
            System.out.println("9: Hilfe");
            System.out.println("0: Beenden");
            input = scan.nextLine();
        } while (!isNumber(input));

        switch (input) {
            case ("1"):
                passTimePresent();
                stallUI();
                break;
            case ("2"):
                passTimeAbsent();
                System.out.println(lager.toString());
                startUI();
                break;
            case ("3"):
                passTimePresent();
                marktUI();
                startUI();
            case ("9"):
                helpUI();
                break;
            case ("0"):
                break;
            default:
                System.out.println("Eingabe " + input + " ist keine wählbare Option.");
                startUI();
        }
    }

    public void stallUI() {
        String inputAnimal = "";
        do {
            Scanner scanimal = new Scanner(System.in);
            System.out.println("Runde: " + count);
            stall.listAnimals();
            System.out.println("0: Zurück zum Hauptmenu");
            System.out.println("Welches Tier (Name) soll bearbeitet werden?");
            inputAnimal = scanimal.nextLine();
            if (inputAnimal.equals("0")) {
                startUI();
            }
        } while (!stall.isOnList(inputAnimal));

        Animal animalObject = stall.getAnimalObject(inputAnimal);
        toDoUI(inputAnimal);
    }

    public void marktUI() {
        String inputMarkt = "";
        do {
            Scanner scanMarkt = new Scanner(System.in);
            System.out.println("Runde: " + count);
            System.out.println("Was möchten Sie tun?");
            System.out.println("1: Kaufen");
            System.out.println("2: Verkaufen");
            System.out.println("0: Zurück");
            inputMarkt = scanMarkt.nextLine();
        } while (!isNumber(inputMarkt));

        switch (inputMarkt) {
            case ("1"):
                passTimeAbsent();
                buyMarktUI();
                break;
            case ("2"):
                passTimeAbsent();
                sellMarktUI();
                break;
            case ("0"):
                passTimeAbsent();
                startUI();
                break;
            default:
                System.out.println("Eingabe " + inputMarkt + " ist keine wählbare Option.");
                marktUI();
        }

    }

    public void sellMarktUI() {
        Scanner scanSell = new Scanner(System.in);
        String input = "";
        do {
            System.out.println("Runde: " + count);
            System.out.println("Was möchten Sie verkaufen?");
            System.out.println("1: Fleisch");
            System.out.println("2: Milch");
            System.out.println("3: Eier");
            System.out.println("4: Wolle");
            System.out.println("0: Zurück");
            input = scanSell.nextLine();
        } while (!isNumber(input));

        switch (input) {
            case ("1"):
                passTimeAbsent();
                sellMeatUI();
                break;
            case ("2"):
                passTimeAbsent();
                sellMilkUI();
                break;
            case ("3"):
                passTimeAbsent();
                sellEggsUI();
                break;
            case ("4"):
                passTimeAbsent();
                sellWoolUI();
                break;
            case ("0"):
                marktUI();
                break;
            default:
                System.out.println("Keine wählbare Option");
                sellMarktUI();
                break;
        }
    }

    public void sellMeatUI() {
        String input;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("Sie haben " + lager.getMeat() + "kg Fleisch im Lager. ");
            System.out.println("Der Händler gibt ihnen " + markt.getPriceMeat() + "€ pro kg");
            System.out.println("Wieviel kg möchten Sie verkaufen?(Nur ganze kg möglich)");
            input = scan.nextLine();
        } while (!checkLimits(0, Integer.valueOf((int) lager.getMeat()), input));

        lager.increaseMoney(Integer.valueOf(input) * markt.getPriceMeat());
        lager.decreaseMeat(Integer.valueOf(input));
        sellMarktUI();
    }

    public void sellMilkUI() {
        String input;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("Sie haben " + lager.getMilk() + "Liter Milch im Lager. ");
            System.out.println("Der Händler gibt ihnen " + markt.getPriceMilk() + "€ pro Liter.");
            System.out.println("Wieviel Liter möchten Sie verkaufen?");
            input = scan.nextLine();
        } while (!checkLimits(0, lager.getMilk(), input));

        lager.increaseMoney(Integer.valueOf(input) * markt.getPriceMilk());
        lager.decreaseMeat(Integer.valueOf(input));
        sellMarktUI();
    }

    public void sellEggsUI() {
        String input;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("Sie haben " + lager.getEggs() + " Eier im Lager. ");
            System.out.println("Der Händler gibt ihnen " + markt.getPriceEgg() + "€ pro Stück.");
            System.out.println("Wieviel Stück möchten Sie verkaufen?");
            input = scan.nextLine();
        } while (!checkLimits(0, lager.getMilk(), input));

        lager.increaseMoney(Integer.valueOf(input) * markt.getPriceMilk());
        lager.decreaseMeat(Integer.valueOf(input));
        sellMarktUI();
    }

    public void sellWoolUI() {
        String input;
        Scanner scan = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("Sie haben " + lager.getWool() + "kg Wolle im Lager. ");
            System.out.println("Der Händler gibt ihnen " + markt.getPriceEgg() + "€ pro kg.");
            System.out.println("Wieviel kg möchten Sie verkaufen?");
            input = scan.nextLine();
        } while (!checkLimits(0, lager.getWool(), input));

        lager.increaseMoney(Integer.valueOf(input) * markt.getPriceWool());
        lager.decreaseMeat(Integer.valueOf(input));
        sellMarktUI();
    }

    public void toDoUI(String animal) {

        Scanner scanimal = new Scanner(System.in);
        String input = "";
        do {
            System.out.println("Runde: " + count);
            System.out.println("Was soll mit " + animal + " getan werden?");
            System.out.println("1: Schlachten");
            System.out.println("2: Füttern");
            System.out.println("3: Sammeln");
            System.out.println("0: Zum Hauptmenu");
            input = scanimal.nextLine();
        } while (!isNumber(input));

        switch (input) {
            case ("1"):
                passTimePresent();
                butcherAnimalUI(animal);
                startUI();
                break;
            case ("2"):
                feedAnimalUI(animal);
                startUI();
                break;
            case ("3"):
                passTimePresent();
                harvestProductUI(animal);
                startUI();
                break;
            case ("0"):
                startUI();
                break;
            default:
                System.out.println("Eingabe " + input + " ist keine wählbare Option.");
                toDoUI(animal);
                break;
        }
    }

    public void butcherAnimalUI(String animal) {
        Animal animalObject = stall.getAnimalObject(animal);
        double meat = animalObject.getMeat();
        lager.increaseMeat(meat);
        stall.kill(animalObject.getName());
        System.out.println("Runde: " + count);
        System.out.println("Es wurden " + meat + "kg Fleisch ins Lager eingelagert.");
        System.out.println("");
        stallUI();
    }

    public void feedAnimalUI(String animal) {
        Animal animalObject = stall.getAnimalObject(animal);
        String inputFutter;
        do {
            Scanner scan = new Scanner(System.in);
            System.out.println("Verfügbare Futtermenge: " + lager.getFood() + "kg.");
            System.out.println("Wieviel kg Futter möchten Sie " + stall.getAnimalClassName(animal) + " " + animal + " geben? ");
            inputFutter = scan.nextLine();
        } while (!checkLimits(0, lager.getFood(), inputFutter));

        int futterAsInt = Integer.valueOf(inputFutter);
        lager.setFood(lager.getFood() - futterAsInt);
        int restFood = (animalObject.feed(futterAsInt));
        if (restFood != 0) {
            System.out.println("Das Tier hat sein Maximalgewicht von " + animalObject.getMaximumWeight() + " erreicht. " + restFood + "kg überschüssiges Futter wird ins Lager zurückgegeben.");
            lager.increaseFood(restFood);
        }
    }

    public void harvestProductUI(String animal) {
        String input = "";
        Animal animalObject = stall.getAnimalObject(animal);
        do {
            System.out.println("Runde: " + count);
            Scanner scan = new Scanner(System.in);
            System.out.println("Verfügbare Menge: " + animalObject.getProduct());
            System.out.println("Wieviel wollen Sie einlagern?");
            input = scan.nextLine();
        } while (!checkLimits(0, animalObject.getProduct(), input));

        if (animalObject instanceof Chicken) {
            lager.increaseEggs(Integer.valueOf(input));
            animalObject.decreaseProduct(Integer.valueOf(input));

        } else if (animalObject instanceof Cow) {
            lager.increaseMilk(Integer.valueOf(input));
            animalObject.decreaseProduct(Integer.valueOf(input));

        } else if (animalObject instanceof Sheep) {
            lager.increaseWool(Integer.valueOf(input));
            animalObject.decreaseProduct(Integer.valueOf(input));
        }

        stallUI();
    }

    public void buyMarktUI() {
        String input;
        Scanner scanNewAnimal = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("1: Kuh(verfügar: " + markt.getAmountCows() + ")");
            System.out.println("2: Schaf(verfügar: " + markt.getAmountSheeps() + ")");
            System.out.println("3: Huhn(verfügar: " + markt.getAmountChickens() + ")");
            System.out.println("4: Futter");
            System.out.println("0: Zurück");
            System.out.println("Welches Tier möchten sie kaufen?");
            input = scanNewAnimal.nextLine();
        } while (!isNumber(input));

        switch (input) {
            case ("1"):
                passTimeAbsent();
                if (markt.getAmountCows() > 0) {
                    buyAnimalUI(new Cow());
                    break;
                } else {
                    System.out.println("Keine Kühe mehr vorhanden.");
                }
            case ("2"):
                passTimeAbsent();
                if (markt.getAmountSheeps() > 0) {
                    buyAnimalUI(new Sheep());
                } else {
                    System.out.println("Keine Schafe mehr vorhanden.");
                }
                break;
            case ("3"):
                passTimeAbsent();
                if (markt.getAmountChickens() > 0) {
                    buyAnimalUI(new Chicken());
                } else {
                    System.out.println("Keine Hühner mehr vorhanden.");
                }
            case ("4"):
                passTimeAbsent();
                buyFoodUI();
                break;
            case ("0"):
                passTimeAbsent();
                marktUI();
                break;
            default:
                System.out.println("Eingabe " + input + " ist keine wählbare Option.");
                buyMarktUI();
                break;
        }
    }

    public void buyFoodUI() {
        String input;
        int compareValue;
        do {
            System.out.println("Runde: " + count);
            Scanner scanFood = new Scanner(System.in);
            System.out.println("Verfügare Menge " + markt.getAmountFood() + "kg");
            System.out.println("Preis pro kg: " + markt.getPriceFood() + "€");
            System.out.println("Du hast " + lager.getMoney() + "€ am Konto");
            System.out.println("Wieviel kg Futter möchtest du kaufen? ");
            input = scanFood.nextLine();
            compareValue = markt.getAmountFood();
            if (lager.getMoney() < markt.getAmountFood()) {
                compareValue = lager.getMoney();
            }
        } while (!checkLimits(0, compareValue, input));

        lager.increaseFood(Integer.valueOf(input));
        lager.decreaseMoney(Integer.valueOf(input));
        markt.decreaseFood(Integer.valueOf(input));
        marktUI();
    }

    public void buyAnimalUI(Animal animal) {
        char input;
        String name;
        Scanner scanAnimal = new Scanner(System.in);
        do {
            System.out.println("Runde: " + count);
            System.out.println("Wie soll des Tier heißen?(Name muss einzigartig sein): ");
            name = scanAnimal.nextLine();
        } while (stall.isOnList(name));
        animal.setName(name);
        markt.buyAnimal(animal);
        while (true) {
            System.out.println(animal);
            int preis = animal.getWeight() * markt.getPricePerKg(animal);
            System.out.println("Der Preis beträgt " + preis + "€");
            System.out.println("Du hast " + lager.getMoney() + "€ am Konto");
            System.out.println("");
            if (checkLimits(0, lager.getMoney(), String.valueOf(preis))) {
                System.out.println("Möchtest du das Tier kaufen?(j/n)");
                input = scanAnimal.next().charAt(0);
                if (input == 'j') {
                    lager.decreaseMoney(preis);
                    stall.addAnimal(animal);
                    markt.decreaseAnimal(animal);
                    break;
                } else if (input == 'n') {
                    break;
                } else {
                    System.out.println("Ungültige Eingabe");
                }
            } else {
                System.out.println("Du hast nicht genügend Geld");
                System.out.println("");
                break;
            }
        }
        buyMarktUI();
    }

    public void helpUI() {
        while (true) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Kaufen Sie Futter und Tiere im Markt und verkaufen Sie ebenfalls dort Produkte und Fleisch. Jedes Produkt und Tier hat einen spezifischen Preis.");
            System.out.println("Geschlachtete Kühe geben mehr Fleisch, als geschlachtete Hühner, kosten aber ebenfalls mehr Nahrung/kg.");
            System.out.println("Streng genommen sind alle Kühe/Hühner weiblich, wir haben hier aber auch männliche Varianten, die entsprechend keine Produkte aber dafür aber mehr Fleisch/Nahrung herstellen können.");
            System.out.println("Alle 4 Runden(1 Runde entspricht eine Bewegung in einen anderen Menüpunkt, Ausnahme 'zurück') wird ein Produkt erzeugt, welches im Stall geerntet werden kann. ");
            System.out.println("");
            System.out.println("Jedes Tier hat ein speziesspezifisches Maximalgewicht. Sollten man es über diesen Wert füttern, wird das Futter einfach nicht angenommen und wieder ins Lager zurückgegeben.");
            System.out.println("Mit jeder Runder steigt der Hunger der Tiere an.");
            System.out.println("Übeschreitet er die Schwelle von 100, sinkt das Gewicht der Tiere um 1. Unterschreitet, das Gewicht den speziespefischen Minimalwert, stirbt das Tier und es gibt kein Fleisch");
            System.out.println("Der Spieler wird aber vorher über dieses Ereignis gewarnt, sofern er sich im Stall befindet. Sorgen Sie also dafür, dass die Tiere gut gefüttert sind, während sie woanders sind.");
            System.out.println("");
            System.out.println("Die jeweiligen Grenzwerte lauten(ausnahmsweise nicht über Variabeln ausgegeben): ");
            System.out.println("Spezies: Kuh || Minimalgewicht: 5kg || Maximalgewicht: 700kg || Futtereffizienz weiblich: 50% || Futtereffizienz männlich: 60% || Fleischeffizienz: 90% ");
            System.out.println("Spezies: Schaf || Minimalgewicht: 3kg || Maximalgewicht: 200kg || Futtereffizient: 70% || Fleischeffizient: 70% ");
            System.out.println("Spezies: Huhn || Minimalgewicht: 1kg || Maximalgewicht: 6kg || Futtereffizienz weiblich 90% || Futtereffizienz männlich 95% || Fleischeffizienz: 50% ");
            System.out.println("");
            System.out.println("Zurück mit Eingabe Taste");
            String input = scan.nextLine();
            if (input.equals("")) {
                break;
            }
        }
        startUI();
    }

    public boolean checkLimits(int lowerLimit, int upperLimit, String value) {

        if (isNumber(value)) {
            int valueAsInt = Integer.valueOf(value);
            if (valueAsInt > upperLimit) {
                System.out.println("Der Wert ist zu groß");
                System.out.println("");
                return false;
            } else if (valueAsInt < lowerLimit) {
                System.out.println("Der Wert ist zu klein");
                System.out.println("");
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean isNumber(String input) {

        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            System.out.println("Eingabe " + input + " ist keine ganze Zahl");
            return false;
        }
    }

    public void passTimePresent() {
        count++;
        if (count % 4 == 0) {
            stall.growProducts();
        }
        stall.increaseHungerAll();

        if (!stall.checkVitals().isEmpty()) {
            ArrayList<String> critical = stall.checkVitals();
            for (String name : critical) {
                char inputFeed;
                while (true) {
                    System.out.println("Achtung, " + stall.getAnimalClassName(name) + " " + name + " ist am Verhungern. Möchten Sie es füttern?(j/n)");
                    Scanner scanFeed = new Scanner(System.in);
                    inputFeed = scanFeed.next().charAt(0);
                    if (inputFeed == 'j') {
                        feedAnimalUI(name);
                        break;
                    } else if (inputFeed == 'n') {
                        break;
                    } else {
                        System.out.println("Ungültige Eingabe");
                    }
                }
            }
        }
    }

    public void passTimeAbsent() {
        count++;
        if (count % 4 == 0) {
            stall.growProducts();
        }
        stall.increaseHungerAll();
    }
}

