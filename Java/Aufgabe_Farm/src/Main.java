import animals.*;
import com.sun.security.jgss.GSSUtil;
import gebaeude.Lager;
import gebaeude.Markt;
import gebaeude.Stall;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Animal> tiere = new ArrayList<>();
        Lager lager = new Lager(0, 0, 0, 0, 0, 100);
        Markt markt = new Markt(1000, 3, 5, 10);

        Cow kuh1 = new Cow("Maria", 'w', 10, 100);
        Sheep schaf1 = new Sheep("Bert", 'm', 2, 4);
        Chicken huhn1 = new Chicken("Sandra", 'w', 2, 1);

        tiere.add(kuh1);
        tiere.add(schaf1);
        tiere.add(huhn1);

        Stall stall = new Stall(tiere);
        Ui ui = new Ui(stall, lager, markt);
        ui.startUI();
    }
}