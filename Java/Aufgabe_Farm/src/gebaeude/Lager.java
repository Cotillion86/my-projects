package gebaeude;

public class Lager {
    private int milk;
    private int eggs;
    private int wool;
    private double meat;
    private int food;
    private int money;

    public Lager(int milk, int eggs, int wool, double meat, int food, int money) {
        this.milk = milk;
        this.eggs = eggs;
        this.wool = wool;
        this.meat = meat;
        this.food = food;
        this.money = money;
    }

    public String toString() {
        return "Milch: " + getMilk() + " Liter || Eier: " + getEggs() + " Stück || Wolle: " + getWool() + "kg || Fleisch: " + getMeat() + "kg || Futter: " + getFood() + "kg || Geld: " + getMoney() + "€";
    }

    /*-----------------------------------------------------------INCREASER/DECREASER -------------------------------------------------------------------------*/

    public void increaseMoney(int amount) {
        this.money += amount;
    }

    public void decreaseMoney(int amount) {
        this.money -= amount;
    }
    public void increaseMilk() {
        this.milk++;
    }
    public void increaseMilk(int amount) {
        this.milk += amount;
    }
    public void increaseEggs() {
        this.eggs++;
    }
    public void increaseEggs(int amount) {
        this.eggs += amount;
    }
    public void increaseWool() {
        this.wool++;
    }

    public void increaseWool(int amount) {
        this.wool += amount;
    }
       public void increaseMeat() {
        this.meat++;
    }
    public void increaseMeat(double amount) {
        this.meat += amount;
    }

    public void decreaseMeat(double amount) {
        this.meat-= amount;
    }
         public void decreaseMeat() {
        this.meat--;
    }
    public void increaseFood() {
        this.food++;
    }
    public void increaseFood(int amount) {
        this.food += amount;
    }
           public void decreaseFood() {
        this.food--;
    }



    /*----------------------------------------GETTER/SETTER--------------------------------------------*/

    public int getMoney() {
        return money;
    }

    public void setMoney(int money) {
        this.money = money;
    }

    public int getMilk() {
        return milk;
    }

    public void setMilk(int milk) {
        this.milk = milk;
    }

    public int getEggs() {
        return eggs;
    }

    public void setEggs(int eier) {
        this.eggs = eier;
    }

    public int getWool() {
        return wool;
    }

    public void setWool(int wool) {
        this.wool = wool;
    }

    public double getMeat() {
        return meat;
    }

    public void setMeat(int meat) {
        this.meat = meat;
    }

    public int getFood() {
        return food;
    }

    public void setFood(int food) {
        this.food = food;
    }
}
