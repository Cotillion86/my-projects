package gebaeude;

import animals.Animal;
import animals.Chicken;
import animals.Cow;
import animals.Sheep;
import java.util.*;

public class Stall {
    private ArrayList<Animal> tiere;

    public Stall(ArrayList<Animal> tiere) {
        this.tiere = tiere;
    }

    public int size() {
        return tiere.size();
    }

    public void listAnimals() {
        for (int i = 0; i < tiere.size(); i++) {
            System.out.println((i + 1) + ": " + tiere.get(i));
        }
    }

    public Animal getAnimalObject(String name) {
        for (Animal animal : tiere) {
            if (animal.getName().equals(name)) {
                return animal;
            }
        }
        return null;
    }

    public void kill(String tier) {
        System.out.println("");
        System.out.println(tier + " wurde getötet!");
        System.out.println("");
        tiere.removeIf(animal -> (animal.getName() == tier));
    }

    public ArrayList checkVitals() {
        ArrayList<String> dead = new ArrayList<>();
        ArrayList<String> critical = new ArrayList<>();
        for (Animal animal : tiere) {
            if (!animal.isStarved().isBlank()) {
                dead.add(animal.isStarved());         // überprüfe ob Tier am leben ist. Falls nein speicher Name in Liste dead. Lösung über Liste, da es möglich ist, dass mehrere Tiere/Runde sterben
            }
            if (!animal.alertStatusAnimal().isBlank()) {
                critical.add(animal.alertStatusAnimal());   // überprüfe den Status des Tieres. Falls es bedroht ist, speicher in Liste critical. Lösung über Liste, da es möglich ist, dass mehrere Tiere/Runde kritisch werden.
            }
        }
        for(String name: dead) {
            kill(name);                                    //Namen in Liste werden an die kill Method weitergegeben.
        }
        return critical;
    }

    public void growProducts() {
        for (Animal animal: tiere) {
            animal.increaseProduct();
        }
    }

    public String getAnimalClassName(String name) {
        if (getAnimalObject(name) instanceof  Cow) {
            return "Kuh";
        } else if (getAnimalObject(name) instanceof  Chicken) {
            return "Huhn";
        } else if (getAnimalObject(name) instanceof  Sheep) {
            return "Schaf";
        }
        return null;
    }

    public void increaseHungerAll() {
        for (Animal animal : tiere) {
            animal.increaseHunger();
            if (animal.getHunger() > 100) {
                animal.decreaseWeight();
                animal.setHunger(animal.getHunger() - 100);
            }
        }
    }

    public void addAnimal(Animal animal) {
        tiere.add(animal);

    }

    public boolean isOnList(String tier) {
        for (Animal animal : tiere) {
            if (animal.getName().equals(tier)) {
                return true;
            }
        }
        return false;

    }

    public int takeMeat(Animal animalObject) {
        return animalObject.getWeight();
    }

    public ArrayList<Animal> getTiere() {
        return tiere;
    }

    public void setTiere(ArrayList<Animal> tiere) {
        this.tiere = tiere;
    }

}
