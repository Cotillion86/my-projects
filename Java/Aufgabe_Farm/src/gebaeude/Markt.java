package gebaeude;

import animals.*;

import java.util.Random;


public class Markt {
    private final int priceEgg = 2;
    private final int priceWool = 4;
    private final int priceMilk = 3;
    private final int priceFood = 1;
    private final int priceMeat = 5;
    private final int pricePerKgCow = 20;
    private final int pricePerKgChicken = 10;
    private final int pricePerKgSheep = 15;
    private int amountFood;
    private int amountCows;
    private int amountSheeps;
    private int amountChickens;
    public Markt(int amountFood, int amountCows, int amountSheeps, int amountChickens) {
        Random rand = new Random();
        this.amountFood = amountFood;
        this.amountCows = amountCows;
        this.amountSheeps = amountSheeps;
        this.amountChickens = amountChickens;}

    public Animal buyAnimal(Animal animal) {
        int age;
        int weight;
        char sex;

        Random rand = new Random();
        age = rand.nextInt(animal.getMaximumAge() - animal.getMinimumAge()) + animal.getMinimumAge();
        animal.setAge(age);
        weight = rand.nextInt(animal.getMaximumWeight() - animal.getMinimumWeight()) + animal.getMinimumWeight();
        animal.setWeight(weight);

        int rollSex = rand.nextInt(1);
        if (rollSex == 0) {
            sex = 'm';
        } else {
            sex = 'w';
        }

        animal.setSex(sex);
        return animal;
    }

    public int getPricePerKg(Animal animal) {
        if (animal instanceof Cow) {
            return pricePerKgCow;
        } else if (animal instanceof Chicken) {
            return pricePerKgChicken;
        } else if (animal instanceof Sheep) {
            return pricePerKgSheep;
        }
        return -1;
    }

    /*------------------------------------------------------DECREASER/INCREASER------------------------------------------------------*/

    public void decreaseAnimal(Animal animal) {
        if (animal instanceof  Cow) {
            decreaseCows();
        } else if (animal instanceof Chicken) {
            decreaseChickens();
        } else if (animal instanceof Sheep) {
            decreaseSheeps();
        }
    }

    public void decreaseFood(int amount) {
        this.amountFood -= amount;
    }
    public void decreaseSheeps() {
        this.amountSheeps --;
    }
    public void decreaseCows() {
        this.amountCows --;
    }public void decreaseChickens() {
        this.amountChickens --;
    }



    /*-------------------------------------------------GETTER/SETTER------------------------------------------------------------------*/

    public int getPriceEgg() {
        return priceEgg;
    }

    public int getPriceWool() {
        return priceWool;
    }

    public int getPriceMilk() {
        return priceMilk;
    }

    public int getPriceFood() {
        return priceFood;
    }

    public int getPriceMeat() {
        return priceMeat;
    }

    public int getPricePerKgCow() {
        return pricePerKgCow;
    }

    public int getPricePerKgChicken() {
        return pricePerKgChicken;
    }

    public int getPricePerKgSheep() {
        return pricePerKgSheep;
    }

    public int getAmountFood() {
        return amountFood;
    }

    public void setAmountFood(int amountFood) {
        this.amountFood = amountFood;
    }

    public int getAmountCows() {
        return amountCows;
    }

    public void setAmountCows(int amountCows) {
        this.amountCows = amountCows;
    }

    public int getAmountSheeps() {
        return amountSheeps;
    }

    public void setAmountSheeps(int amountSheeps) {
        this.amountSheeps = amountSheeps;
    }

    public int getAmountChickens() {
        return amountChickens;
    }

    public void setAmountChickens(int amountChickens) {
        this.amountChickens = amountChickens;
    }
}


