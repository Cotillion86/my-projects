package fahrzeuge;

public  class LKW extends Fahrzeug {

    public LKW(int id, String marke, int baujahr, double grundpreis) {
        super(id, marke, baujahr, grundpreis);
    }

    @Override
    public double getRabatt() {
        double rabatt;
        rabatt =  (2023 - baujahr)*6.0;
        if (rabatt > 15) {
            return 15.0;
        } else {
            return rabatt;
        }
    }

    @Override
    public void print() {
        System.out.println("ID: " + id);
        System.out.println("Marke: " + marke);
        System.out.println("Baujahr: " + baujahr);
        System.out.println("Grundpreis: " + grundpreis);
        System.out.println("Rabatt " + getRabatt());
        System.out.println("Preis: " + getPreis() + " Euro");
    }

    public double getPreis() {
        return grundpreis - (grundpreis / 100 *getRabatt());
    }


}
