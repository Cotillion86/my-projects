package fahrzeuge;

public class PKW extends Fahrzeug {

    private int serviceJahr;

    public PKW(int id, String marke, int baujahr, int serviceJahr, double grundpreis) {
        super(id, marke, baujahr, grundpreis);
        this.serviceJahr = serviceJahr;
    }

    @Override
    public double getRabatt() {
        double rabatt;
        rabatt = ((2023 - baujahr) * 3) + ((2023 - serviceJahr) * 5);
        if (rabatt > 10) {
            return 10.0;
        } else {
            return rabatt;
        }
    }

    @Override
    public void print() {
        System.out.println("ID: " + id);
        System.out.println("Marke: " + marke);
        System.out.println("Baujahr: " + baujahr);
        System.out.println("Servicejahr: " + serviceJahr);
        System.out.println("Grundpreis: " + grundpreis);
        System.out.println("Rabatt " + getRabatt());
        System.out.println("Preis: " + getPreis() + " Euro");
    }

    public double getPreis() {
        return grundpreis - (grundpreis / 100 * getRabatt());
    }

    /*---------------------------------------GETTER/SETTER-------------------------------------*/

    public int getServiceJahr() {
        return serviceJahr;
    }
    public void setServiceJahr(int serviceJahr) {
        if (serviceJahr < baujahr) {
            System.out.println("Die Eingabe " + serviceJahr + " kann nicht korrekt sein, da das Baujahr " + baujahr + " des Fahrzeugs darüber liegt.");
        } else {
            this.serviceJahr = serviceJahr;
        }
    }
}
