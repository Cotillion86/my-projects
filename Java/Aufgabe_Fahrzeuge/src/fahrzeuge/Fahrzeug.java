package fahrzeuge;

public abstract class Fahrzeug {
    protected int id;
    protected String marke;
    protected int baujahr;
    protected double grundpreis;

    public Fahrzeug(int id, String marke, int baujahr, double grundpreis) {
        this.id = id;
        this.marke = marke;
        this.baujahr = baujahr;
        this.grundpreis = grundpreis;
    }

    public abstract double getRabatt();
    public abstract void print();
    public double getPreis() {
        return -1.0;
    }
/*--------------------------------------GETTER/SETTER----------------------------------------*/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarke() {
        return marke;
    }

    public void setMarke(String marke) {
        this.marke = marke;
    }

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public double getGrundpreis() {
        return grundpreis;
    }

    public void setGrundpreis(double grundpreis) {
        this.grundpreis = grundpreis;
    }
}
