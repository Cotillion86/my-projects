import fahrzeuge.*;

import java.util.ArrayList;
import java.util.Collection;

public class Autohaus {

    private int abstellPlaetze;
    private boolean zuVieleFahrzeuge;
    private ArrayList<Fahrzeug> fahrzeugList;

    public Autohaus(){
        this.fahrzeugList = new ArrayList<>();
    }

    public void setFahrzeugList(String fahrzeugTyp, int id, String marke, int baujahr, int serviceJahr, double grundpreis) {
        if (fahrzeugTyp.equals("PKW")) {
        this.fahrzeugList.add(new PKW(id, marke, baujahr, serviceJahr, grundpreis));

        } else if (fahrzeugTyp.equals("LKW")) {
            this.fahrzeugList.add(new LKW(id, marke, baujahr, grundpreis));

        } else {
            System.out.println("Ungültige Eingabe");
        }
    }

    public boolean zuVieleFahrzeuge(int mitarbeiter) {
        return (anzahlFahrzeuge() > (mitarbeiter * 3 ));
    }

    public boolean zuVieleFahrzeuge() {
        return (anzahlFahrzeuge() > abstellPlaetze);
    }


        public ArrayList<Fahrzeug> getFahrzeuge() {
        return this.fahrzeugList;
    }

    public int anzahlFahrzeuge() {
        return this.fahrzeugList.size();
    }


    public int getAbstellPlaetze() {
        return abstellPlaetze;
    }

    public void setAbstellPlaetze(int abstellPlaetze) {
        this.abstellPlaetze = abstellPlaetze;
    }


}
