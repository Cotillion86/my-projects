import fahrzeuge.*;

public class Main {
    public static void main(String[] args) {

        Autohaus autohaus = new Autohaus();
        autohaus.setAbstellPlaetze(5);
        autohaus.setFahrzeugList("PKW", 12345, "Volvo", 2010, 2018, 2000);
        autohaus.setFahrzeugList("LKW", 12345, "Mercedes", 2015, -1, 3000);

        System.out.println(autohaus.anzahlFahrzeuge());
        System.out.println("Folgende Marken sind zu verkaufen: ");
        autohaus.getFahrzeuge().forEach(element -> {
            System.out.println(element.getMarke());
        });

        System.out.println("Gibt es genügend Abstellplätze: " + autohaus.zuVieleFahrzeuge());
        System.out.println("Gibt es genügend Mitarbeiter(10): " + autohaus.zuVieleFahrzeuge(10));

        for(Fahrzeug fahrzeug : autohaus.getFahrzeuge()) {
            System.out.println(fahrzeug.getPreis());
        }

        // ODER SCHIRCH

        for (int i = 0; i < autohaus.anzahlFahrzeuge(); i++) {
            System.out.println(autohaus.getFahrzeuge().get(i).getPreis());
        }

        autohaus.getFahrzeuge().forEach(element -> {
            if (element instanceof LKW) {
                System.out.println("Das Fahrzeug ist ein LKW");
            } else if (element instanceof PKW) {
                System.out.println("Das Fahrzeug ist ein PKW");
            }
        });

    }
}