package com.example.aufgabe_controller.controller;

import com.example.aufgabe_controller.entity.Article;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

import static com.example.aufgabe_controller.AufgabeControllerApplication.articleSet;

@RestController()
@RequestMapping("api/article")


public class ArticleController {

    @PostMapping()
    public String addNewArticle(@RequestBody Article article) {
        articleSet.add(article);

        return "Artikel erfolgreich angelegt";
    }

    @GetMapping()
    public ResponseEntity<Set> getArticle() {
        return new ResponseEntity<>(articleSet, HttpStatus.ACCEPTED);
    }

    @GetMapping("{articleId}")
    public ResponseEntity<Article> getArticle(@PathVariable int articleId) {
        if (getArticleById(articleId) != null) {
            return new ResponseEntity<>(getArticleById(articleId), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("{articleId}")
    public ResponseEntity<Article> setArticlePrice(@PathVariable int articleId, @RequestBody double newPrice) {
        if (getArticleById(articleId) != null) {
            Article foundArticle = getArticleById(articleId);
            foundArticle.setArticlePrice(newPrice);
            return new ResponseEntity<>(foundArticle, HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }



    public Article getArticleById(int id) {
        for (Article article : articleSet) {
            if (article.getArticleId() == id) {
                return article;
            } else {
                return null;
            }
        }
        return null;
    }
}


