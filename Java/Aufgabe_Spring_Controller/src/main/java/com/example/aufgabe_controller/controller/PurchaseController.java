package com.example.aufgabe_controller.controller;

import com.example.aufgabe_controller.entity.Article;
import com.example.aufgabe_controller.entity.Purchase;
import com.example.aufgabe_controller.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static com.example.aufgabe_controller.AufgabeControllerApplication.*;

@RestController()
@RequestMapping("api/purchase")

public class PurchaseController {

    @PostMapping
    public ResponseEntity<String> addPurchase(@RequestBody Purchase purchase) {
        try {
            isPurchaseInSet(purchase);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("Bestellung aufgegeben", HttpStatus.ACCEPTED);
    }

    @GetMapping
    public Set<Purchase> getAllPurchases() {
        return purchaseSet;
    }




    public void isPurchaseInSet(Purchase purchase) throws Exception {
        boolean hasUser = false;
        boolean hasArticle = false;
        for (User user : userSet) {
            if (user.getUserId() == purchase.getUserId()) {
                hasUser = true;
            }
        }
        for (Article article : articleSet) {
            if (article.getArticleId() == purchase.getArticleId()) {
                hasArticle = true;
            }
        }
        if (!hasUser && !hasArticle) {
            throw new Exception("User als auch Artikel nicht vorhanden");

        } else if (!hasUser) {
            throw new Exception("User nicht vorhanden");

        } else if (!hasArticle) {
            throw new Exception("Artikel nicht vorhanden");

        } else {
            purchaseSet.add(new Purchase(purchase.getUserId(), purchase.getArticleId()));
        }
    }
}
