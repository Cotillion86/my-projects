package com.example.aufgabe_controller;

import com.example.aufgabe_controller.entity.Article;
import com.example.aufgabe_controller.entity.Purchase;
import com.example.aufgabe_controller.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.HashSet;
import java.util.Set;

@SpringBootApplication
public class AufgabeControllerApplication {
		public static Set<User> userSet = new HashSet<>();
		public static Set<Article> articleSet = new HashSet<>();
		public static Set<Purchase> purchaseSet = new HashSet<>();

	public static void main(String[] args) {
		userSet.add(new User(1, "Bert", "PW123", 30));
		articleSet.add(new Article(1, "Flasche", 50.0));
		purchaseSet.add(new Purchase(1, 15));
		SpringApplication.run(AufgabeControllerApplication.class, args);


	}

}
