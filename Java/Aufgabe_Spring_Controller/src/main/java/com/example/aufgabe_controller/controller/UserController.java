package com.example.aufgabe_controller.controller;

import com.example.aufgabe_controller.entity.Article;
import com.example.aufgabe_controller.entity.Purchase;
import com.example.aufgabe_controller.entity.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

import static com.example.aufgabe_controller.AufgabeControllerApplication.*;


@RestController
@RequestMapping("api/user")
public class UserController {

    @PostMapping()
    public ResponseEntity<String> addUserToSet(@RequestBody User user) {
        try {
            isUserInSet(user);
        } catch (Exception e) {
            return new ResponseEntity<>("User bereits vorhanden", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("User erfolgreich angelegt", HttpStatus.CREATED);
    }

    @GetMapping()
    public Set<User> getAllUser() {
        return userSet;
    }

    @DeleteMapping("{userId}")
    public ResponseEntity<String> deleteUser(@PathVariable int userId) {
        try {
            checkThenDeleteUser(userId);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);

        }

        return new ResponseEntity<>("User gelöscht", HttpStatus.ACCEPTED);
    }


    public void isUserInSet(User user) throws Exception {
        for (User savedUser : userSet) {
            if (savedUser.getUserId() == user.getUserId()) {
                throw new Exception();
            }
        }
        userSet.add(user);
    }

    public void checkThenDeleteUser(int userId) throws Exception {
       for (Purchase purchase : purchaseSet) {
           if (purchase.getUserId() == userId) {
               throw new Exception("User hat noch eine Bestellung offen");
           }
       }

       for(User user : userSet) {
           if (user.getUserId()==userId) {
               userSet.remove(user);
               return;
           }
       }

       throw new Exception("User ist nicht in der Datenbank vorhanden");
    }
}

