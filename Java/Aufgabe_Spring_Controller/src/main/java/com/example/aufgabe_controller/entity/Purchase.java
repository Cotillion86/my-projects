package com.example.aufgabe_controller.entity;

public class Purchase {

    private int userId;
    private int articleId;

    public Purchase(int userId, int articleId) {
        this.userId = userId;
        this.articleId = articleId;
    }

    public Purchase() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }
}


