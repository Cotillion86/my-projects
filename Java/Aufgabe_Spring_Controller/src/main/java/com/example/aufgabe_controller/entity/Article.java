package com.example.aufgabe_controller.entity;

public class Article {

    private int articleId;
    private String articleName;
    private double articlePrice;

    public Article(int articleId, String articleName, double articlePrice) {
        this.articleId = articleId;
        this.articleName = articleName;
        this.articlePrice = articlePrice;
    }

    public Article() {
    }

    public int getArticleId() {
        return articleId;
    }

    public void setArticleId(int articleId) {
        this.articleId = articleId;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public double getArticlePrice() {
        return articlePrice;
    }

    public void setArticlePrice(double articlePrice) {
        this.articlePrice = articlePrice;
    }
}
