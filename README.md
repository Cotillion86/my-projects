# ⚠️ Deprecated Repository

This repository is **no longer maintained** and contains outdated verisons of projects.

Please visit the **up-to-date version** of this project here:  
➡️ [My Projects - Cotillion](https://gitlab.com/my-projects-cotillion)
