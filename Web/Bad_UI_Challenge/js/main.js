// Random number generator
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

const continueButton = document.getElementById('continueButton');

/*----------------------------------------------------LOGINDATEN-------------------------------------*/

if (window.location.pathname.includes("Bad_UI_Challenge/login.html")) {

    // E-Mail
    const generateMailButton = document.getElementById('generateMailButton');
    const numberOfEmailLettersInput = document.getElementById('numEmailLetters');
    const emailInputDiv = document.getElementById('emailInput');
    let emailAddress = "";
    let letterCheckBoxes = [];
    let isValidEmail = false;

    numberOfEmailLettersInput.addEventListener('keypress', event => {
        event.preventDefault();
    });

    generateMailButton.addEventListener('click', event => {
        emailInputDiv.innerHTML = "";
        let alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ß', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '_', '-', '@', '.'];
        let numberOfEmailLetters = numberOfEmailLettersInput.value;

        for (let i = 0; i < numberOfEmailLetters; i++) {
            alphabet.sort(function () {
                return Math.random() - .5
            });
            $('<select></select>').attr('id', 'letter' + [i]).appendTo('#emailInput');
            let currentID = "letter" + [i];

            const selectElement = document.getElementById(currentID);
            for (let j = 0; j < alphabet.length; j++) {
                const optionElement = document.createElement('option');
                optionElement.setAttribute('value', alphabet[j]);
                optionElement.textContent = alphabet[j];

                selectElement.appendChild(optionElement);
            }

            emailInputDiv.appendChild(selectElement);

        }

        // Prevent searching with keyboard. Unforunatly does not work :(
        letterCheckBoxes = document.querySelectorAll('#emailInput > select');
        letterCheckBoxes.forEach(element => {
            element.addEventListener('keydown', event => {
                event.preventDefault();
            });
        });
    });

    // Password
    let lengthBool, letterBool, numUpperBool, numProductBool, specialCharacterBool, isConfirmed = false;

    const passwordInput = document.getElementById('passwordInput');
    const passwordAlert = document.getElementById('passwordAlert');
    let password = "";

    passwordInput.addEventListener('focus', event => {
        passwordAlert.classList.remove('d-none');
    });

    passwordInput.addEventListener('blur', event => {
        passwordAlert.classList.add('d-none');
    });


    // PW listener and req 1
    passwordInput.addEventListener('input', event => {

        password = passwordInput.value;
        specialCharacterBool = false;

        if (password.length >= 12) {
            lengthBool = true;
        } else {
            lengthBool = false;
        }
        if (lengthBool) {
            document.querySelector('#passwordAlert > ul > li').classList.remove('text-success');
            document.querySelector('#passwordAlert > ul > li').classList.add('text-danger');
            document.querySelector('#passwordAlert > div').classList.remove('invisible');

        } else {
            document.querySelector('#passwordAlert > ul > li').classList.remove('text-danger');
            document.querySelector('#passwordAlert > ul > li').classList.add('text-success');
            document.querySelector('#passwordAlert > div').classList.add('invisible');
        }

        // Req 2
        let numUpper = (password.match(/[A-Z]/g) || []).length;
        let numLower = (password.match(/[a-z]/g) || []).length;

        if ((password.length / 2) === (numUpper + numLower)) {
            letterBool = true;
        } else {
            letterBool = false;
        }

        if (letterBool) {
            document.querySelector('#passwordAlert2 li:nth-of-type(1)')?.classList.remove('text-success');
            document.querySelector('#passwordAlert2 li:nth-of-type(1)')?.classList.add('text-danger');
        } else {
            document.querySelector('#passwordAlert2 li:nth-of-type(1)')?.classList.remove('text-danger');
            document.querySelector('#passwordAlert2 li:nth-of-type(1)')?.classList.add('text-success');
        }

        // Req 3
        if ((numUpper >= password.length / 4) && (numUpper <= password.length / 3)) {
            numUpperBool = true;
        } else {
            numUpperBool = false;
        }

        if (numUpperBool) {
            document.querySelector('#passwordAlert2 li:nth-of-type(2)')?.classList.remove('text-success');
            document.querySelector('#passwordAlert2 li:nth-of-type(2)')?.classList.add('text-danger');
        } else {
            document.querySelector('#passwordAlert2 li:nth-of-type(2)')?.classList.remove('text-danger');
            document.querySelector('#passwordAlert2 li:nth-of-type(2)')?.classList.add('text-success');
        }

        // Req 4
        let numbersInPassword = password.match(/\d+/g);
        if (numbersInPassword !== null) {
            let highestNumberInPassword = Math.max(...numbersInPassword);

            if (highestNumberInPassword > (numUpper * numLower)) {
                numProductBool = true;
            } else {
                numProductBool = false;
            }
        }

        if (numProductBool) {
            document.querySelector('#passwordAlert2 li:nth-of-type(3)')?.classList.remove('text-success');
            document.querySelector('#passwordAlert2 li:nth-of-type(3)')?.classList.add('text-danger');
        } else {
            document.querySelector('#passwordAlert2 li:nth-of-type(3)')?.classList.remove('text-danger');
            document.querySelector('#passwordAlert2 li:nth-of-type(3)')?.classList.add('text-success');
        }

        // Req5
        let specialCaseSymbols = (/[^a-z ^A-Z ^0-9]/);
        let firstSpecialCase = password.match(specialCaseSymbols);
        for (let i = firstSpecialCase?.index; i < password.length; i += 3) {
            if (specialCaseSymbols.test(password[i])) {
                specialCharacterBool = true;
            } else {
                specialCharacterBool = false;
                break;
            }
        }

        if (specialCharacterBool) {
            document.querySelector('#passwordAlert2 li:nth-of-type(4)')?.classList.remove('text-success');
            document.querySelector('#passwordAlert2 li:nth-of-type(4)')?.classList.add('text-danger');
        } else {
            document.querySelector('#passwordAlert2 li:nth-of-type(4)')?.classList.remove('text-danger');
            document.querySelector('#passwordAlert2 li:nth-of-type(4)')?.classList.add('text-success');
        }

    });


    // Adding click listener to "Weiter" Button and functionality
    continueButton.addEventListener('click', event => {
        if (password === document.querySelector('#passwordConfirm > div > input').value) {
            isConfirmed = true;
        }

        emailAddress = "";
        letterCheckBoxes.forEach(element => {
            emailAddress += element.value;
        });
        let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;

        isValidEmail = emailRegex.test(emailAddress);

        if (!isValidEmail) {
            alert('Das ist keine gültige E-Mail Adresse. Versuchs erneut!');
            location.reload(true);
        } else if (!(lengthBool && letterBool && numUpperBool && specialCharacterBool && numProductBool)) {
            alert('Dein Passwort ist scheiße. Versuchs erneut!');
            location.reload(true);
        } else if (lengthBool && letterBool && numUpperBool && specialCharacterBool && numProductBool && isConfirmed) {
            localStorage.setItem('email', emailAddress);
            localStorage.setItem('password', password);
            window.location.href = "name.html";
        } else if (lengthBool && letterBool && numUpperBool && specialCharacterBool && numProductBool && !isConfirmed) {
            alert('Ups, ganz vergessen. Aus Sicherheitsgründen musst du natürlich dein Passwort bestätigen');
            document.getElementById('passwordConfirm').classList.remove('d-none');
        }
    });
}


/* -------------------------------------------- NAMEN --------------------------------------------------*/

if (window.location.pathname.includes("Bad_UI_Challenge/name.html")) {

    // Declaration and initialization of global variables
    const maxHeight = window.innerHeight;
    const maxWidth = window.innerWidth;
    const firstNameInput = document.getElementById('firstNameInput');
    const lastNameInput = document.getElementById('lastNameInput');
    const letterInput = document.getElementById('letterInput');
    const firstNameCheckBox = document.getElementById('checkBoxFirstName');
    const lastNameCheckBox = document.getElementById('checkBoxLastName');
    const letterSlot = document.getElementById('letterSlot');
    const shiftButton = document.getElementById('shiftButton');
    const deleteButton = document.getElementById('deleteButton2');
    const outerBody = document.getElementById('outerBody');
    let checkBoxes = document.querySelectorAll('#checkBoxesNames > div > label > input');


    checkBoxes.forEach(element => {
        element.addEventListener('click', event => {
            if (firstNameCheckBox.checked) {
                outerBody.style.backgroundColor = "#eee";
            } else if (lastNameCheckBox.checked) {
                outerBody.style.backgroundColor = "blueviolet";
            }
        });
    });

    let dragged = null;
    let isLocked = false

    // Disable Keyboard
    document.onkeydown = function (e) {
        return false;
    }

    // Random creation of letter divs
    let letters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'ß'];
    letters.sort(function () {
        return Math.random() - .5
    });

    // Function Upper and Lowercase
    function toUpper() {
        letters = letters.map(element => {
            return element.toUpperCase();
        });
    }

    function toLower() {
        letters = letters.map(element => {
            return element.toLowerCase();
        });
    }

    // Function render letters
    function render() {
        letterSlot.innerHTML = "";
        for (let i = 0; i < letters.length; i++) {
            $('<div draggable="true">' + letters[i] + '</div>').appendTo('#letterSlot');
        }

        const draggedLetter = document.querySelectorAll('#letterSlot > div');
        draggedLetter.forEach(element => {
            element.addEventListener("dragstart", event => {
                dragged = event.target.innerText;
            });
        });
    }

    render();

    // Shift Button Functionality
    shiftButton.addEventListener('click', event => {
        if (isLocked) {
            isLocked = false;
            toLower();
            render();
        } else if (!isLocked) {
            isLocked = true;
            toUpper();
            render();
        }
    });

    // Delete Button Functionality
    deleteButton.addEventListener('click', event => {
        location.reload(true);
    });

    // Creating target div in random position
    let xPos = getRandomInt(0, maxWidth - 100);
    let yPos = getRandomInt(0, maxHeight - 100);
    letterInput.style.left = xPos + "px";
    letterInput.style.top = yPos + "px";

    // Adding drop and dragover  listener to target div with functionality
    letterInput.addEventListener('drop', event => {
        event.preventDefault();
        if (firstNameCheckBox.checked) {

            firstNameInput.value += dragged;
        } else {

            lastNameInput.value += dragged;
        }
    });

    letterInput.addEventListener('dragover', event => {
        event.preventDefault();
    });

    // Making target div move
    const leftBound = 0;
    const rightBound = maxWidth - 50;
    const topBound = 0;
    const bottomBound = maxHeight - 50;
    let xSpeed = 13;
    let ySpeed = 13;

    function moveLetterInput() {
        xPos += xSpeed;
        yPos += ySpeed;
        if (xPos < leftBound) {
            xPos = leftBound;
            xSpeed = -xSpeed;
        } else if (xPos > rightBound) {
            xPos = rightBound;
            xSpeed = -xSpeed;
        }
        if (yPos < topBound) {
            yPos = topBound;
            ySpeed = -ySpeed;
        } else if (yPos > bottomBound) {
            yPos = bottomBound;
            ySpeed = -ySpeed;
        }
        letterInput.style.left = xPos + "px";
        letterInput.style.top = yPos + "px";

    }

    setInterval(moveLetterInput, 20);

    // Adding click listener to "Weiter" Button and functionality
    continueButton.addEventListener('click', event => {
        const regexName = /^[A-Z][a-z]$/;
        if ((regexName.test(firstNameInput.value) && regexName.test(lastNameInput.value))) {
            localStorage.setItem('firstName', firstNameInput.value);
            localStorage.setItem('lastName', lastNameInput.value);
            window.location.href = "number.html";
        } else {
            alert('Gültige Namen haben mindestens 2 Buchstaben und fangen mit einem Großbuchstaben an. Das weiß ein Volksschüler!!!!')
            location.reload();

        }
    });

}
/*----------------------------------------------------TELEFONNUMMER-------------------------------------*/

if (window.location.pathname.includes("Bad_UI_Challenge/number.html")) {

    // Declaration of global variables
    let keys = document.querySelectorAll('.key');
    const textarea = document.querySelector('#code');
    const inputBox = document.getElementById('box');
    let isValidPhoneNumber = false;


    // Distance function
    function getClosestNumberIndex() {
        let distance;
        let indexClosest;
        let distanceX;
        let distanceY;
        let distanceClosest = 10000;
        let inputBoxLeft = (inputBox.offsetLeft + (inputBox.offsetWidth / 2));
        let inputBoxTop = (inputBox.offsetTop + (inputBox.offsetHeight / 2));

        keys.forEach((key, index) => {
            let left = (key.offsetLeft + (key.offsetWidth / 2));
            let top = (key.offsetTop + (key.offsetHeight / 2));
            distanceX = inputBoxLeft - left;
            distanceY = inputBoxTop - top;
            distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);

            if (distanceClosest >= distance) {
                distanceClosest = distance;
                indexClosest = key.innerHTML;
            }
        })

        if (distanceClosest < 30) {

            return indexClosest;
        }
        alert("Du hast nichts ausgewählt")

        return null;
    }

    // Adding listener and functionality

    window.addEventListener('keydown', event => {
        event.preventDefault();
        let posLeft = parseInt(inputBox.style.left);
        let posTop = parseInt(inputBox.style.top);

        if (event.key == 'ArrowRight') {
            inputBox.style.left = (posLeft + 3) + 'px';
        }
        if (event.key == 'ArrowLeft') {
            inputBox.style.left = (posLeft - 3) + 'px';
        }
        if (event.key == 'ArrowUp') {
            inputBox.style.top = (posTop - 3) + 'px';
        }
        if (event.key == 'ArrowDown') {
            inputBox.style.top = (posTop + 3) + 'px';
        }
        if (event.key == 'Enter') {

            let closestNumberIndex = getClosestNumberIndex();
            if (closestNumberIndex !== null) {
                if (confirm("Du hast " + closestNumberIndex + " ausgewählt. Ist das korrekt?")) {
                    textarea.value += closestNumberIndex;
                } else {
                    textarea.value = "";
                }
            }
            inputBox.style.top = (posTop + getRandomInt(100, 150)) + 'px';
            inputBox.style.left = (posLeft + getRandomInt(100, 200)) + 'px';
        }

    });

    window.addEventListener('click', event => {
        if (event.target.id == "submit") {
            let phoneNumber = textarea.value;
            let phoneNumberRegex = /^0\d{10,}$/;

            isValidPhoneNumber = phoneNumberRegex.test(phoneNumber);

            if (!isValidPhoneNumber) {
                alert("Das ist keine richtige Nummer. So billig kommst du hier nicht davon!");
            } else {

                if (!confirm("Du hast " + phoneNumber + " als Nummer ausgewählt. Möchtest du die Nummer löschen oder speichern?")) {
                    localStorage.setItem('phoneNumber', phoneNumber);
                    window.location.href = "finale.html"
                } else {
                    location.reload(true);
                }
            }

        } else if (event.target.id == "delete") {
            textarea.value = "";
        }
    });
}

/*----------------------------------------------------FINALE-------------------------------------*/

if (window.location.pathname.includes("Bad_UI_Challenge/finale.html")) {

    const cookie = document.getElementById('cookie');

    // Final text
    document.getElementById('outPutDiv').innerHTML =
        `         Gratuliere <b>${localStorage.getItem('firstName')} ${localStorage.getItem('lastName')}</b>. 
         Du hast dich erfolgreich mit der Telefonnummer <b>${localStorage.getItem('phoneNumber')}</b> angemeldet.
         
         Deine streng geheimen Zugangsdaten lauten:
          
         Nutzername: <span id="blink">${localStorage.getItem('email')}</span>
         Passwort: <span id="blink">${localStorage.getItem('password')}</span>

         Als Belohnung gibt es einen leckeren <b>Cookie</b>. 
         Einfach draufklicken, dann wird er downgeloadet!`

    // Cookie Handler
    window.addEventListener('mousemove', event => {
        let width = window.innerWidth;
        let height = window.innerHeight;
        let x = event.clientX;
        let y = event.clientY;
        let distanceX = parseInt(cookie.style.left) + cookie.offsetWidth / 2 - x;
        let distanceY = parseInt(cookie.style.top) + cookie.offsetHeight / 2 - y;
        let distance = Math.sqrt(distanceX * distanceX + distanceY * distanceY);
        if (distance < 100) {
            cookie.style.left = Math.random() * (width - 300) + "px";
            cookie.style.top = Math.random() * (height - 300) + "px";
        }
    });
}
