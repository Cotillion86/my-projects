import {defineStore} from 'pinia'
import axios from "axios";


export const useUserStore = defineStore('user', {
    state: () => ({
        users: []
    }),
    actions: {
        async getUser() {
            const getResponse = await axios.get("https://ziqmuiotvf.user-management.asw.rest/api/users");
            this.users = [...getResponse.data];
        },

        async createUser(user) {
            const postResponse = await axios.post("https://ziqmuiotvf.user-management.asw.rest/api/users", user);
            this.users.push(postResponse.data);
        },

        async deleteUser(userId) {
            await axios.delete("https://ziqmuiotvf.user-management.asw.rest/api/users/" + userId);
            let userIndex = this.users.findIndex(user => user.userId === userId);
            this.users.splice(userIndex, 1);
        },

        async editUser(updatedPerson) {
            const {userId,...editedPerson} = updatedPerson;
            const putResponse = await axios.put("https://ziqmuiotvf.user-management.asw.rest/api/users/" + userId, editedPerson);
            let userIndex = this.users.findIndex(user => user.userId === userId);
            this.users.splice(userIndex, 1, putResponse.data)

        }
    }

})
