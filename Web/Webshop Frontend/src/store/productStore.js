import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'

export const useProductStore = defineStore('product', {
    state: () => ({
        products: []
    }),
    actions: {
        async createProduct(product) {
            const {data} = await axios.post(API_URL + 'products', product)
            this.products.push(data)
        },

        async getProducts() {
            const {data} = await axios.get(API_URL +  'products')
            this.products = data
        },

        async deleteProducts(productId) {
            await axios.delete(API_URL + 'products/' + productId)
            const index = this.products.findIndex(product => product.productId === productId);
            this.products.splice(index, 1)
        },

        async editProduct(editedProduct) {
            const {data} = await axios.put(API_URL + 'products/' + editedProduct.productId, editedProduct)
            const index = this.products.findIndex(product => product.productId === editedProduct.productId);
            this.products.splice(index, 1, data)
        },

        async getProductById(productId) {

        }
    }
})
