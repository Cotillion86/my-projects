import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'

export const useBasketStore = defineStore('basket', {
    state: () => ({
        basket: {}
    }),
    actions: {
        async addProductToBasket(addedProduct) {
            const {data} = await axios.post(API_URL + 'baskets/item', addedProduct)
            this.basket = data

        },

        async getBasket() {
            const {data} = await axios.get(API_URL + 'baskets')
            this.basket = data

        },

        async deleteBasket() {
            await axios.delete(API_URL + 'baskets')
            this.basket = {}
        },

        async deleteProductFromBasketById(productId) {
            const {data} = await axios.delete(API_URL+ 'baskets/item/' + productId)
            this.basket = data

        },

        async editBasketItem(basketProduct) {
            const {data} = await axios.put(API_URL + 'baskets/item/' + basketProduct.productId, basketProduct)
            this.basket = data

        },

        async putBasket(editBasketObject) {
            const {data} = await axios.put(API_URL + 'baskets', editBasketObject)
            this.basket = data
        },

        async postBasket() {
            await axios.post(API_URL + 'baskets/order')
            await this.deleteBasket()
            this.basket = {}

        }
    }
})
