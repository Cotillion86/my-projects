import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/styles'
import { createVuetify } from 'vuetify'
import { VDataTable } from 'vuetify/labs/VDataTable'
import {colors} from "@/Color scheme/Colors";


export default createVuetify({
    components: {
        VDataTable,
    },
    theme: {
        themes: {
            light: {
                colors: {
                    primary: colors.primaryBlue,
                    secondary: colors.secondaryBlue,
                    userPrimary: colors.primaryGreen,
                    userSecondary: colors.secondaryGreen
                },
            },
            dark: {
                colors: {
                    'on-surface': '#fff'
                }
            }

        },
    },
})
