import {createRouter, createWebHistory} from 'vue-router'
import AuthenticationLayout from '@/layouts/AuthenticationLayout.vue'
import ApplicationLayout from '@/layouts/ApplicationLayout.vue'
import LoginView from '@/views/auth/LoginView.vue'
import RegistrationView from '@/views/auth/RegistrationView.vue'
import HomeView from '@/views/HomeView.vue'
import {useAuthStore} from '@/store/authStore'
import CreateCompanyView from "@/views/CreateCompanyView.vue"
import {useProjectsStore} from "@/store/projectStore";
import {useTradeStore} from "@/store/tradeStore";
import {useOfferStore} from "@/store/offerStore";
import MyProjectsView from "@/views/user_views/MyProjectsView.vue"
import UserProfileView from "@/views/UserProfileView.vue"
import ProjectsByTradeView from "@/views/company_views/ProjectsByTradeView.vue"
import OffersByCompanyView from "@/views/company_views/OffersByCompanyView.vue"
import ReportView from "@/views/admin_views/ReportView.vue";
import AdminProfileView from "@/views/admin_views/AdminProfileView.vue";
import ApprovalView from "@/views/admin_views/ApprovalView.vue";
import {useReportStore} from "@/store/reportStore";
import {useRatingStore} from "@/store/RatingStore";
import {useStatisticsStore} from "@/store/statisticsStore";
import {useUserStore} from "@/store/userStore";
import CompanyProfileView from "@/views/CompanyProfileView.vue";
import MapViewCompany from '@/views/company_views/MapView.vue'
import MapViewUser from '@/views/user_views/MapView.vue'
import CreateAddressView from '@/views/CreateAddressView.vue'
import CompanyByTradeOverviewView from "@/views/CompanyByTradeOverviewView.vue";
import {useAddressStore} from "@/store/addressStore";

const routes = [
    {
        path: '/',
        component: ApplicationLayout,
        children: [
            {
                path: '',
                component: HomeView,
                beforeEnter: async () => {
                    if (!useStatisticsStore().resourcesInizialized){
                        await useRatingStore().getSampleTestimonials()
                        await useTradeStore().getTrades()
                        await useStatisticsStore().getStatistics()
                        await useStatisticsStore().getCompaniesForHomeView()
                        useStatisticsStore().resourcesInizialized=true
                    }
                }
            },

            {
                path: 'admin/profile',
                component: AdminProfileView,
            },

            {
                path: 'admin/reports',
                component: ReportView,
                beforeEnter: async () => {
                    await useReportStore().getReports()
                    await useUserStore().getLockedUser()
                    await useUserStore().getAllUserWithCompanyAndAddress()
                },
            },
            {
                path: 'admin/approvals',
                component: ApprovalView
            },

            {
                path: 'company/map',
                component: MapViewCompany,
                beforeEnter: async () => {
                    await useAddressStore().getAddress()
                    await useProjectsStore().getProjectsByTrade()
                    await useOfferStore().getOffersForCompany()
                }
            },
            {
                path: 'company/:companyId',
                component: CompanyProfileView,
                beforeEnter: async (to) => {
                    await useStatisticsStore().getCompanyProfile(to.params.companyId)
                }
            },

            {
                path: 'company/open-projects',
                component: ProjectsByTradeView,
                // beforeEnter: async () => {
                //     await useProjectsStore().getProjectsByTrade()
                //     await useCompanyStore().getCompanyByUserId(useAuthStore().user.userId)
                // }
            },
            {
                path: 'company/my-offers',
                component: OffersByCompanyView,
                beforeEnter: async () => {
                    await useOfferStore().getOffersForCompany()
                }
            },
            {
                path: 'user/my-projects',
                component: MyProjectsView,
                beforeEnter: async () => {
                    await useProjectsStore().getUserProjectsWithOffer()
                    await useTradeStore().getTrades()
                }
            },
            {
                path: 'user/:userId',
                component: UserProfileView,
                beforeEnter: async () => {
                    await useAuthStore().getUser()
                }
            },
            {
                path: 'user/map',
                component: MapViewUser,
                beforeEnter: async () => {
                    await useProjectsStore().getUserProjectsWithOffer()
                    await useAddressStore().getAddress()

                }
            },
            {
                path: '/companies/:trade',
                component: CompanyByTradeOverviewView,
                beforeEnter: async (to) => {
                    await useStatisticsStore().getCompaniesByTrade(to.params.trade)
                }
            }
        ]
    },
    {
        path: '/login',
        component: AuthenticationLayout,
        children: [
            {
                path: '',
                component: LoginView
            },
            {
                path: '/register',
                component: RegistrationView
            },
            {
                path: '/company',
                component: CreateCompanyView,
                beforeEnter: async () => {
                    await useTradeStore().getTrades()
                }

            },
            {
                path: '/address',
                component: CreateAddressView
            }
        ]
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach(() => useAuthStore().initialize())

export default router
