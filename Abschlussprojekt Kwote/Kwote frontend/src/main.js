import App from './App.vue'
import vuetify from '@/plugins/vuetify'
import router from '@/router'
import {loadFonts} from '@/plugins/webfontloader'
import {createPinia} from 'pinia'
import axios from 'axios'
import {createApp} from "vue";

axios.interceptors.request.use(request => {
    const jwt = localStorage.getItem('jwt')
    if (!request.headers.hasAuthorization() && jwt) {
        request.headers.setAuthorization("Bearer " + jwt)
    }
    return request
})

loadFonts()
createApp(App)
    .use(vuetify)
    .use(createPinia())
    .use(router)
    .mount('#app')


