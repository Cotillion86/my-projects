import {ref} from "vue";

export const colors= {
    primaryBlue: '#00326d',
    altPrimaryBlue: '#074d94',
    secondaryBlue: '#23b4dd',
    altSecondaryBlue: '#48d1fd',

    primaryGreen: '#0d6256',
    altPrimaryGreen: '#1fa59a',
    secondaryGreen: '#24bfa3',
    altSecondaryGreen: '#43d2da'}
