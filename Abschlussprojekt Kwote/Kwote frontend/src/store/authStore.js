import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'
import {useCompanyStore} from "@/store/companyStore";
import {useOfferStore} from "@/store/offerStore";
import {useProjectsStore} from "@/store/projectStore";
import {useReportStore} from "@/store/reportStore";
import {useMessageStore} from "@/store/messageStore";
import {useAddressStore} from "@/store/addressStore";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import {useRouter} from "vue-router";
import {useImageStore} from "@/store/imageStore";
import {ref} from "vue";
import {useTradeStore} from "@/store/tradeStore";

export const useAuthStore = defineStore('auth', {

    state: () => ({
        isInitialized: false, // dient dazu, um festzuhalten, ob initial nach dem Seitenreload die Benutzerdaten bereits geladen wurden
        user: null,
        router:useRouter()
    }),
    getters: {
        // Kann genutzt werden, um allgemein abzuprüfen, ob der Seitenbesucher eingeloggt ist
        isLoggedIn() {
            return !!this.user
        },
        // Überprüft, ob der eingeloggte Seitenbesucher ein Admin ist
        isAdmin() {
            return this.user?.role === 'ADMIN'
        },
        // Überprüft, ob der eingeloggte Seitenbesucher ein Kunde ist
        isUser() {
            return this.user?.role === 'USER'
        },
        isCompany() {
            return this.user?.role === 'COMPANY'
        }
    },
    actions: {
        /**
         * Diese Funktion dient dazu, nach einem Seitenreload die Benutzerdaten einmal zu laden.
         * Wenn diese Funktion mehrfach aufgerufen wird, soll sie dennoch nur ein einziges Mal einen Request abschicken
         */
        async initialize() {
            if(!this.isInitialized) {
                /**
                 * "isInitialized" bekommt als Wert die Promise an sich, die hier aber noch nicht awaited wird.
                 * Damit wird bei einem erneuten Aufruf der Funktion true und verhindert, dass "loadUser" unabsichtlich erneut ausgeführt wird.
                 * Mit "catch" verhindert man, dass diese Promise einen Error wirft
                 */
                this.isInitialized = this.loadUser().catch(() => undefined)
            }
            /**
             * Hier wird erst die Promise awaited. Wenn die Funktion aufgerufen wird, wenn der Wert von "isInitialized" bereits true ist,
             * entspricht der Ausdruck "await true", was nichts macht.
             */
            await this.isInitialized
            this.isInitialized = true
        },
        async loadUser() {
            // Es soll kein Request abgeschickt wird, wenn kein JWT gespeichert ist.
            if(!localStorage.getItem('jwt')) {
                return
            }
            await this.getUser()
            await this.loadResources()
        },
        async login(credentials) {
            // wenn ein alter token im speicher sit wird er hier gelöscht da sonst das backend einen expired fehler werfen kann
            if(!!localStorage.getItem('jwt')) {
                    localStorage.removeItem('jwt')
            }
            const {data} = await axios.post(API_URL + 'auth/login', credentials)
            this.applyAuthentication(data)
            await this.loadResources()
        },
        async register(registration, profileImage) {
            if(!!localStorage.getItem('jwt')) {
                localStorage.removeItem('jwt')
            }
            const {data} = await axios.post(API_URL + 'auth/register', registration)
            if(!!profileImage){
                await axios.post(API_URL + "images/profile/"+data.user.userId, profileImage)
            }
            this.applyAuthentication(data)
            await this.loadResources()
        },
        logout() {
            this.router.push('/')
            this.user = null
            useAddressStore().$reset()
            useCompanyStore().$reset()
            useOfferStore().$reset()
            useReportStore().$reset()
            useMessageStore().disconnect()
            localStorage.removeItem('jwt')
            // imageStore
            // messageStore
            // projectStore
            // ratingStore
            // statisticStore
            // tradeStore
        },
        applyAuthentication({user, jwt}) {
            this.user = user

            // WENN MANN Bearer heir einfügt funktionieren die put requests und so aber beim page refresh qird man rausgehaut
            // localStorage.setItem('jwt', "Bearer " + jwt) // Hier wird der JWT dauerhaft unter dem Namen "jwt" (erster Parameter) gespeichert.
            localStorage.setItem('jwt', jwt) // Hier wird der JWT dauerhaft unter dem Namen "jwt" (erster Parameter) gespeichert.
        },
        async getUser(){
            const {data} = await axios.get(API_URL + 'auth/')
            this.applyAuthentication(data)
        },
        async loadResources(){
            await useMessageStore().getMyMessages()
            await useMessageStore().connect()
            await useAddressStore().getAddress()
            if(this.isCompany)  {
                await useCompanyStore().getCompanyByUserId(this.user.userId)
                await useOfferStore().getOffersForCompany()
                await useProjectsStore().getProjectsByTrade()
                await useReportStore().getMyReports()

            } else if(this.isUser) {
                await useReportStore().getMyReports()
                await useProjectsStore().getUserProjectsWithOffer()
            }
        }
    }
})
