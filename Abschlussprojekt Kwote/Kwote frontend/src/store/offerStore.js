import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'
import {useProjectsStore} from "@/store/projectStore";

export const useOfferStore = defineStore('offer', {
    state: () => ({
        offers: [],
        offersForUser: [],
        offersForCompany: {}
    }),
    actions: {
        async postOffer(offer) {
            const {data} = await axios.post(API_URL + 'offer/', offer)
            this.offers.push(data)
        },

        async getOffers() {
            const {data} = await axios.get(API_URL + 'offer/')
            this.offers = data
        },

        async getOffersForUser(userId) {
            const {data} = await axios.get(API_URL + 'offer/' + userId )
            this.offers = data

        },
        async getOffersForCompany() {
            const {data} = await axios.get(API_URL + 'offer/company')
            this.offersForCompany = data
        },

        async updateOffer(updatedOffer) {
            const {data} = await axios.put(API_URL + 'offer/' + updatedOffer.offerId, updatedOffer)
            let index = this.offers.findIndex(offer => offer.offerId === data.offerId);
            if(index !== -1) {
                this.offers[index] = data
            }
        },

        async deleteOffer(deletedOffer) {
            await axios.delete(API_URL + 'offer/' + deletedOffer.offerId)
            let index = this.offers.findIndex(offer => offer.offerId === deletedOffer.offerId)
            if (index !== -1) {
                this.offers.splice(index, 1)
            }
        }
    }
})
