import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'

export const useReportStore = defineStore('report', {
    state: () => ({
        reports: []
    }),
    actions: {
        async reportOffer(reportOffer) {
            await axios.post(API_URL + 'report/', reportOffer)
            await this.getMyReports()

        },

        async getReports() {
            const {data} = await axios.get(API_URL + 'report/')
            this.reports = data
        },

        async updateReport(updatedReport) {
            const {data} = await axios.put(API_URL + 'report/', updatedReport)
            let index = this.reports.findIndex(report => report.reportId === data.reportId);
            if(index !== -1) {
                this.reports[index] = data
            }
        },

        async getMyReports() {
            const{data} = await axios.get(API_URL + 'report/user/')
            this.reports = data;
        },

        async deleteReport(reportId) {
            await axios.delete(API_URL + 'report/' + reportId)
            let index = this.reports.findIndex(report => report.reportId === reportId);
            if(index !== -1) {
                this.reports.splice(index, 1)
            }
        }
    }
})
