import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'
import {ref, toRaw} from "vue";
import {useStatisticsStore} from "@/store/statisticsStore";
import {useAuthStore} from "@/store/authStore";


export const useImageStore = defineStore('imageStore', () => {
    const statisticsStore = useStatisticsStore()
    const authStore = useAuthStore()

    const projectImages = ref([])
    const profileImages = ref([])
    const imageMap = new Map()
    const config = {
        headers: {
            'Content-Type': 'multipart/form-data',
        },
    }

    function getImage(url) {
        if (!imageMap.has(url)) {
            imageMap.set(url, ref('https://picsum.photos/200/300'))
            axios.get(url, {responseType: 'blob'}).then(({data: imageblob}) => {
                const image = imageMap.get(url)
                image.value = URL.createObjectURL(imageblob)
            })
        }
        return imageMap.get(url)
    }

    async function getProjects() {
        const {data} = await axios.get(API_URL + "images/" + imageId)
    }

    async function postImage(formData) {
        // const config = {
        //     headers: {
        //         'Content-Type': 'multipart/form-data',
        //     },
        // };

        try {
            const response = await axios.post(API_URL + "images/upload", formData, config);
            image.value = response.data;
        } catch (error) {
            console.error('Error uploading image:', error);
        }
    }

    async function postProfileImage(imageFormData) {
        try {
            const {data} =  await axios.post(API_URL + "images/profile/" +authStore.user.userId, imageFormData)
            // authStore.user.profileImageUrl=data
        } catch (error) {
            console.error('Error uploading image:', error);
        }

    }

    async function postPortfolioImage(formData, companyId) {
        // const config = {
        //     headers: {
        //         'Content-Type': 'multipart/form-data',
        //     },
        // };

        try {
            const {data} = await axios.post(API_URL + "images/portfolio/" + companyId, formData, config);
            statisticsStore.companyProfile.portfolioImageUrls.push(data)
        } catch (error) {
            console.error('Error uploading image:', error);
        }
    }
    async function deletePortfolioImage(imageId, index) {

            const {data} = await axios.delete(API_URL + "images/portfolio/" + imageId);
            statisticsStore.companyProfile.portfolioImageUrls.splice(index,1)
    }

    async function postCompanyImage(formData) {
        try {
            const {data} = await axios.post(API_URL + "images/company" , formData, config);
        } catch (error) {
            console.error('Error uploading image:', error);
        }
    }

    return {getImage, postImage, postPortfolioImage, postCompanyImage, postProfileImage, deletePortfolioImage}
})
