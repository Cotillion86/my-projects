import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'
import {useAuthStore} from "@/store/authStore";

export const useCompanyStore = defineStore('company', {
    state: () => ({
        company: null,
        companies: [],

    }),

    actions: {
        async createCompany(companyData, imageFormData) {
            console.log(imageFormData)
            const {data} = await axios.post(API_URL + 'companies/' , companyData)
            this.company = data

            const config = {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
            };
            try {
                const {data} = await axios.post(API_URL + "images/company", imageFormData, config);

            } catch (error) {
                console.error('Error uploading image:', error);
            }

        },

        async getCompanyByUserId(userId) {
            const {data} = await axios.get(API_URL + 'companies/user/' + userId)
            this.company = data

        },

        async getAllCompanies() {
            const {data} = await axios.get(API_URL + 'companies/all')
            this.companies = data
        }
    }
})

