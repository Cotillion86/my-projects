import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'
import {useAuthStore} from "@/store/authStore";

export const useUserStore = defineStore('user', {
    state: () => ({
        lockedUsers: [],
        user: null,
        users: [],
        allUsersWithCompanyAndAddress: [],
        authstore: useAuthStore()
    }),
    actions: {
        async getUserById(userId) {
            const {data} = await axios.get(API_URL + 'users/' + userId)
            this.user = data

        },

        async updateUser(user) {
            const {data} = await axios.put(API_URL + 'users/' + user.userId, user)
            this.user = data
        },

        async getLockedUser() {
            const {data} = await axios.get(API_URL + 'users/')
            this.lockedUsers = data.userDtoList
        },

        async getAllUser() {
            const {data} = await axios.get(API_URL +'users/all')
            this.users = data
        },

        async getAllUserWithCompanyAndAddress() {
            const {data} = await axios.get(API_URL + 'users/all-with-companies')
            this.allUsersWithCompanyAndAddress = data
        },
        async editOwnUserData() {
            const {firstName, lastName } = this.authstore.user;
            const editedUser= {firstName, lastName }
            const {data} = await axios.put(API_URL + 'users/editownaccount', editedUser)
        }
    }
})
