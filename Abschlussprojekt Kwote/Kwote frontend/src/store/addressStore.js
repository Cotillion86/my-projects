import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'

export const useAddressStore = defineStore('address', {
    state: () => ({
        isInitialized: false,
        address: null,
        addressList: []
    }),
    actions: {
        async createAddress(address) {
            const {data} = await axios.post(API_URL + 'address/' , address)
            if(data.primaryAddress) {
                this.address = data
            } else {
                this.addressList.push(data)
            }
        },
        async putAddress(address) {
            const {data} = await axios.put(API_URL + 'address/' , address)
            this.address = data
        },
        async getAddress() {
            const {data} = await axios.get(API_URL + 'address/')
            if (data.primaryAddress) {
                this.address = data
            } else {
                this.addressList.push(data)
            }
        }
    }
})
