import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'
import {ref, toRaw} from "vue";



export const useRatingStore = defineStore('ratings',()=> {

    const sampleTestimonials = ref([])

    async function getSampleTestimonials(){
        const {data}=await axios.get(API_URL+"rating/sample")
        sampleTestimonials.value=data.testimonialDtos
    }

    return {getSampleTestimonials, sampleTestimonials}
})
