import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'
import {computed, ref, toRaw} from "vue";
import {useAuthStore} from "@/store/authStore";
import {useOfferStore} from "@/store/offerStore";
import {useImageStore} from "@/store/imageStore";



export const useProjectsStore = defineStore('projects', () => {
    const authstore = useAuthStore()
    const offerstore = useOfferStore()
    const projects = ref([])

    const projectsWithoutOffers = computed(()=> {
       return projects.value.filter(project=>!(offerstore.offersForCompany.offeredProjects.includes(project.projectId)))
    })

    async function getProjects() {
        const {data} = await axios.get(API_URL + "project/")
        projects.value = data;
    }

    async function getProjectsByTrade() {
        const {data} = await axios.get(API_URL + "project/company/")
        projects.value = data;
    }

    async function postProject(project, imageArray, beforeAfter) {
        const {data} = await axios.post(API_URL + "project/", project)
        projects.value.push(data)

        for(let formData of imageArray){
            console.log(formData)
           const {request} = await axios.post(API_URL + "images/project/"+data.projectId+beforeAfter, formData)
        }
    }

    async function getUserProjectsWithOffer() {
              const {data} =  await axios.get(API_URL + 'project/user/')
        projects.value = data
    }

    async function updateProject(project) {
        const {data} = await axios.put(API_URL + 'project/', project)
        let index = projects.value.findIndex(project => project.projectId === data.projectId)
        if(index !== -1) {
            projects.value[index] = data
        }
    }

    async function deleteProject(deletedProject) {
        await axios.delete(API_URL + 'project/' + deletedProject.projectId)
        let index = projects.value.findIndex(project => project.projectId === deletedProject.projectId)
        if (index !== -1) {
            projects.value.splice(index, 1)
        }

    }

    async function rateProject(ratedProject, images, projectId){
        await axios.post(API_URL + 'rating/' , ratedProject)
        for(let formData of images){
            const {request} = await axios.post(API_URL + "images/project/"+ projectId +"/1", formData)
        }
    }



return {projects, getProjects, postProject, getProjectsByTrade, getUserProjectsWithOffer, updateProject, deleteProject, projectsWithoutOffers, rateProject}
})
