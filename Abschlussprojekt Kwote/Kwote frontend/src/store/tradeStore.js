import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'
import {ref, toRaw} from "vue";
import {tr} from "vuetify/locale";


export const useTradeStore = defineStore('trade',()=> {

          const trades = ref([])

    async function getTrades(){
        const {data} = await axios.get(API_URL+"trade/")
        trades.value = data.tradeList
    }
  async function postTrade(trade){
       await axios.post(API_URL+"trade/", trade)
    }

return {trades, getTrades, postTrade}
})
