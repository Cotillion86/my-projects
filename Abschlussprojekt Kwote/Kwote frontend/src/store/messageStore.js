import {defineStore} from 'pinia'
import axios from 'axios'
import {API_URL} from '@/api'
import {computed, ref, toRaw} from "vue";
import SockJS from "sockjs-client";
import Stomp from "stompjs";
import {useAuthStore} from "@/store/authStore";


export const useMessageStore = defineStore('messageStore', () => {
    const authStore = useAuthStore()
    let stompClient;
    let subscription;

    const chats = ref([])
    const activeChat = ref()

    async function getMyMessages() {
        const {data} = await axios.get(API_URL + 'messages/')
        chats.value = data.chats
    }

    async function getLastMessageByChat(chatId) {
        const {data} = await axios.get(API_URL + 'messages/' + chatId)
        return data
    }

    async function createChatroom(offerIdObject) {
        const {data} = await axios.post(API_URL + 'messages/', offerIdObject)
        getMyMessages()
        const chat = chats.value.find(chat => chat.chatIdString == data)
        activeChat.value = chat
    }

    async function disconnect() {
        console.log('disconnected')
        subscription.unsubscribe()
        stompClient.disconnect()
    }

    async function connect() {
        const sockJS = new SockJS("http://localhost:8080/ws");
        stompClient = Stomp.over(sockJS);
        stompClient.connect("Bearer " + localStorage.getItem("jwt"), '', onConnected, console.error);

        function onConnected() {
            // 2. parameter is callbackfunction die ausgeführt wird wenn TCP connection eine Änderung pusht
            if (!!authStore.user) {
                subscription = stompClient.subscribe('/user/' + authStore.user.userId + '/private',async (message) => {
                    const newMessage = JSON.parse(message.body)
                    const chat = chats.value.find(chat => chat.chatId === newMessage.chatId)
                    if (!!chat) {
                        chat.messages.push(newMessage)
                        if (activeChat.value==null||false) {
                            activeChat.value = chat
                        }
                    } else {
                        await getMyMessages()
                        const chat = chats.value.find(chat => chat.chatId === newMessage.chatId)
                        if (activeChat.value==null||false) {
                            activeChat.value = chat
                        }
                    }
                })
            } else {
                console.log('no User')
            }
        }
    }

    async function sendMessage(messageObject) {
        const message = {}
        const chat =
        message.senderId = authStore.user?.userId
        message.recipientId = messageObject.recipientId
        message.content = messageObject.myMessage
        //3. parameter ist object das an Pfad übergeben wird
        stompClient.send("/app/privateMessage", {}, JSON.stringify(message));
        const lastMessage = await getLastMessageByChat(messageObject.chatId)
        chats.value.find(chat => chat.chatId === messageObject.chatId).messages.push(lastMessage)
    }

    return {chats, getMyMessages, createChatroom, activeChat, connect, sendMessage, disconnect}
})
