import { defineStore } from 'pinia'
import axios from 'axios'
import { API_URL } from '@/api'
import {ref, toRaw} from "vue";



export const useStatisticsStore = defineStore('statistics',()=> {

    const statistics = ref({})
    const companiesForHomeView = ref()
    const companyProfile =ref()
    const companiesByTrade= ref()
    const resourcesInizialized= ref(false)

    async function getStatistics(){
        const {data}=await axios.get(API_URL+"statistics/")
        statistics.value=data
    }
    async function getCompanyProfile(companyId){
        const {data}=await axios.get(API_URL+"statistics/companypage/"+companyId)
        companyProfile.value=data
    }
    async function getCompaniesByTrade(trade){
        const {data}=await axios.get(API_URL+"statistics/companiesByTrade/"+trade)
        companiesByTrade.value=data.companies
    }
    async function getCompaniesForHomeView(){
        const {data}=await axios.get(API_URL+"statistics/sampleCompanies/")
        companiesForHomeView.value=data.companyForHomeViewDtos
    }


    return {statistics, getStatistics, getCompaniesForHomeView, companiesForHomeView,
        companyProfile, companiesByTrade, getCompanyProfile, getCompaniesByTrade, resourcesInizialized}
})
