package backend.Kwote.controllers;

import backend.Kwote.repositories.ProfileImageCrudRepository;
import backend.Kwote.services.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/images/")
public class ImageController {
    @Autowired
    ImageService imageService;
    @Autowired
    ProfileImageCrudRepository profileImageCrudRepository;

    @PostMapping("profile/{userId}")
    public ResponseEntity<?> uploadProfileImage(@RequestParam("file") MultipartFile file, @PathVariable Integer userId) throws IOException {
        String uploadImageResponse = imageService.uploadProfileImage(file, userId);
        return ResponseEntity.ok(uploadImageResponse);
    }

    @GetMapping("profile/{imageId}")
    public ResponseEntity<?> downloadProfileImage(@PathVariable Integer imageId) {
        byte[] image = imageService.downloadProfileImage(imageId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }
//    0 für before und 1 für after
    @PostMapping("project/{projectId}/{beforeAfter}")
    public ResponseEntity<?> uploadProjectImage(@RequestParam("file") MultipartFile file, @PathVariable Integer projectId, @PathVariable Integer beforeAfter) throws IOException {
        String uploadImageResponse = imageService.uploadProjectImage(file,projectId, beforeAfter);
        return ResponseEntity.ok(uploadImageResponse);
    }

//    für mehrere images funktoiniert noch nicht
//    @PostMapping("project/{projectId}")
//    public ResponseEntity<?> uploadProjectImages(@RequestParam("file") List<MultipartFile> files, @PathVariable Integer projectId) throws IOException {
//        String uploadImageResponse = imageService.uploadProjectImages(files,projectId);
//        return ResponseEntity.ok(uploadImageResponse);
//    }

    @GetMapping("project/{imageId}")
    public ResponseEntity<?> downloadProjectImage(@PathVariable Integer imageId) {
        byte[] image = imageService.downloadProjectImage(imageId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }

    @PostMapping("company")
    public ResponseEntity<?> uploadCompanyProfileImage(@RequestParam("file") MultipartFile file, @RequestHeader (name="Authorization") String token) throws IOException {
        String uploadImageResponse = imageService.uploadCompanyProfileImage(file, token);
        return ResponseEntity.ok(uploadImageResponse);
    }

    @GetMapping("company/{imageId}")
    public ResponseEntity<?> downloadCompanyProfileImage(@PathVariable Integer imageId) {
        byte[] image = imageService.downloadCompanyProfileImage(imageId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }

    @PostMapping("portfolio/{companyId}")
    public ResponseEntity<?> uploadCompanyPortfolioImage(@RequestParam("file") MultipartFile file, @PathVariable Integer companyId) throws IOException {
        String uploadImageResponse = imageService.uploadCompanyPortfolioImage(file, companyId);
        return ResponseEntity.ok(uploadImageResponse);
    }
    @DeleteMapping ("portfolio/{imageId}")
    public ResponseEntity<?> deletePortfolioImage(@PathVariable Integer imageId, @RequestHeader (name="Authorization") String token) throws IOException {
        String uploadImageResponse = imageService.deleteCompanyPortfolioImage(imageId, token);
        return ResponseEntity.ok(uploadImageResponse);
    }

    @GetMapping("portfolio/{imageId}")
    public ResponseEntity<?> downloadCompanyPortfolioImage(@PathVariable Integer imageId) {
        byte[] image = imageService.downloadCompanyPortfolioImage(imageId);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(image);
    }
}
