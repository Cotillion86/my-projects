package backend.Kwote.controllers;

import backend.Kwote.dtos.ProjectDto;
import backend.Kwote.dtos.CreateProjectDto;
import backend.Kwote.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/project/")
@EnableMethodSecurity
public class ProjectController {
    @Autowired
    ProjectService projectService;

    @PostMapping
    ResponseEntity <ProjectDto> postProject (@RequestHeader (name="Authorization") String token, @RequestBody CreateProjectDto createProjectDto){
        return ResponseEntity.ok(projectService.postProject(token, createProjectDto));
    }

    @GetMapping
    ResponseEntity<List> getAllProjects() {
        return ResponseEntity.ok((projectService.getProjects()));
    }

    @GetMapping("user/")
     ResponseEntity<List> getProjectsWithOffer(@RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok((projectService.getProjectsWithOffer(token)));
    }

     @GetMapping("company/")
     ResponseEntity<List> getProjectsByTrade(@RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(projectService.getProjectsByTrade(token));
    }

    @PutMapping
    ResponseEntity<ProjectDto> updateProject(@RequestBody ProjectDto projectDto) {
        return ResponseEntity.ok(projectService.updateProject(projectDto));
    }

    @DeleteMapping("{projectId}")
    void deleteProject(@PathVariable int projectId) {
        projectService.deleteProject(projectId);
    }


}
