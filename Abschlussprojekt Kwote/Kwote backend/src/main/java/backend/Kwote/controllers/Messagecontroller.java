package backend.Kwote.controllers;

import backend.Kwote.dtos.MessageOutgoingDto;
import backend.Kwote.dtos.MessagesCreateChatroomDto;
import backend.Kwote.dtos.MessagesGetAllDto;
import backend.Kwote.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/messages/")
public class Messagecontroller {
    @Autowired
    MessageService messageService;

    @GetMapping
    public MessagesGetAllDto getAllMessagesByUser (@RequestHeader(name="Authorization") String token){
        return messageService.getAllMessagesByUserDto(token);
    }
    @GetMapping("{chatId}")
    public ResponseEntity getMyLastMessage (@RequestHeader(name="Authorization") String token, @PathVariable Integer chatId){
        return messageService.getMyLastMessage(token, chatId);
    }

    @PostMapping
    public ResponseEntity createChatRoom (@RequestHeader(name="Authorization") String token, @RequestBody MessagesCreateChatroomDto messagesCreateChatroomDto){
        return messageService.createChatroom(token, messagesCreateChatroomDto);
    }
}
