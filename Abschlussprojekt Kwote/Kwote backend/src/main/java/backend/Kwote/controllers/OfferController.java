package backend.Kwote.controllers;

import backend.Kwote.dtos.CreateOfferDto;
import backend.Kwote.dtos.OfferDto;
import backend.Kwote.services.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/offer/")

public class OfferController {

    @Autowired
    OfferService offerService;

    @PostMapping
    public ResponseEntity<OfferDto> postOffer(@RequestBody CreateOfferDto createOfferDto) {
        return ResponseEntity.ok(offerService.postOffer(createOfferDto));
    }

    @GetMapping
    public ResponseEntity<List> getOffers() {
        return ResponseEntity.ok(offerService.getOffers());
    }

//    Gibt ein Array aus das alle offers auf projekte des user beinhaltet dzt. nicht in verwendung
    @GetMapping("{userId}")
    public ResponseEntity<List> getOffersForUser(@PathVariable int userId) {
        return ResponseEntity.ok(offerService.getOffersForUser(userId));
    }

//    Gibt alle Offers und zugehörige Projekte einer Firma aus die dem User im Token zugeordnet ist
//    Array mit allen projekt Ids
//    Array mit Objekten die offer und projekt objekte enthalten
    @GetMapping("company")
    public ResponseEntity<?> getOffersByCompany(@RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(offerService.getOffersByCompany(token));
    }

    @PutMapping("{offerId}")
    public ResponseEntity<OfferDto> updateOffer(@PathVariable int offerId, @RequestBody OfferDto offerDto) {
        return ResponseEntity.ok(offerService.updateOffer(offerId, offerDto));
    }

    @DeleteMapping("{offerId}")
    public void deleteOffer(@PathVariable int offerId) {
        offerService.deleteOffer(offerId);
    }
}
