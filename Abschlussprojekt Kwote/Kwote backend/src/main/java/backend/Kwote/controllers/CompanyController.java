package backend.Kwote.controllers;

import backend.Kwote.dtos.CompanyDto;
import backend.Kwote.dtos.CreateCompanyDto;
import backend.Kwote.dtos.TradeDto;
import backend.Kwote.dtos.TradePostDto;
import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.services.CompanyService;
import backend.Kwote.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/companies/")
@EnableMethodSecurity public class CompanyController {

    @Autowired
    CompanyService companyService;

    @Autowired
    StatisticsService statisticsService;

    @PostMapping()
    public ResponseEntity<?> postCompany(@RequestBody CreateCompanyDto createCompanyDto) {
        return ResponseEntity.ok(companyService.createCompany(createCompanyDto));

    }

    @GetMapping()
    public ResponseEntity<List> getCompanyByTrade(@RequestBody TradePostDto trade) {
        return ResponseEntity.ok(companyService.getCompanyByTrade(trade));
    }

    @GetMapping("user/{userId}")
    public ResponseEntity<CompanyDto> getCompanyByUserId(@PathVariable int userId) {
        return ResponseEntity.ok(companyService.getCompanyByUserId(userId));
    }

    @GetMapping("all")
    public ResponseEntity<List<CompanyDto>> getAllCompanies() {
        return ResponseEntity.ok(companyService.getAllCompanies());
    }

}
