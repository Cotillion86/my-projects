package backend.Kwote.controllers;

import backend.Kwote.services.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/statistics/")
public class StatisticsController {
    @Autowired
    StatisticsService statisticsService;

    @GetMapping
    ResponseEntity <?> getStatistics (){
       return ResponseEntity.ok( statisticsService.getStatistics());
    }

    @GetMapping("sampleCompanies/")
    ResponseEntity <?> getSampleCompaniesForHomeView(){
        return ResponseEntity.ok(statisticsService.getSampleCompaniesForHomeView());
    }

    @GetMapping("companiesByTrade/{trade}")
    ResponseEntity <?> getAllCompaniesByTradeForOverview(@PathVariable String trade){
        return ResponseEntity.ok(statisticsService.getAllCompaniesByTradeForOverview(trade));
    }
    @GetMapping("companypage/{companyId}")
    public ResponseEntity <?> getCompanyProfilePage (@PathVariable Integer companyId){
        return ResponseEntity.ok(statisticsService.getCompanyProfilePage(companyId));
    }
}
