package backend.Kwote.controllers;

import backend.Kwote.dtos.RatingDto;
import backend.Kwote.services.RatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/rating/")
@EnableMethodSecurity
public class RatingController {
    @Autowired
    RatingService ratingService;

    @PostMapping
    ResponseEntity<?> postRating(@RequestBody RatingDto ratingDto) {
        return ResponseEntity.ok(ratingService.postRating(ratingDto));
    }
    @GetMapping("sample")
    ResponseEntity<?> getSampleRatings(){
        return ResponseEntity.ok(ratingService.getSampleTestimonials());
    }
}
