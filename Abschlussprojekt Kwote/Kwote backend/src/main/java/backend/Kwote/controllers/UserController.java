package backend.Kwote.controllers;

import backend.Kwote.dtos.*;
import backend.Kwote.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/users/")
@EnableMethodSecurity
public class UserController {
    @Autowired
    UserService userService;

    @DeleteMapping("{userId}")
    List<UserDto>  registerUser (@PathVariable int userId){
        userService.deleteUser(userId);
        return getAllUsers();
    }

    @GetMapping("{userId}")
    UserDto getUser (@PathVariable int userId){
        return userService.getUser(userId);
    }

    @GetMapping("/all")
    List<UserDto> getAllUsers (){
        return userService.getAllUsers();
    }

    @PutMapping("{userId}")
    ResponseEntity <?>  editUser (@RequestBody EditUserDto editUser, @PathVariable int userId){
       return  ResponseEntity.ok(userService.editUser(userId, editUser));
    }

    @PutMapping("editownaccount")
    ResponseEntity <?>  editUser (@RequestBody UserEditSelfDto editUser, @RequestHeader(name="Authorization") String token){
        return  ResponseEntity.ok(userService.editUserOwn(token, editUser));
    }


    @GetMapping
    ResponseEntity<LockedUserListDto> getLockedUsers() {
        return ResponseEntity.ok(userService.getLockedUsers());
    }

    @GetMapping("/all-with-companies")
    ResponseEntity<List<UserWithCompanyAndAddressDto>> getAllUserWithCompanyAndAddress(){
        return ResponseEntity.ok(userService.getAllUserWithCompanyAndAddress());
    }



}
