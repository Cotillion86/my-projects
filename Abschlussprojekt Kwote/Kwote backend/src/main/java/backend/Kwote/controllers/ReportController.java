package backend.Kwote.controllers;

import backend.Kwote.dtos.CreateReportDto;
import backend.Kwote.dtos.ReportDto;
import backend.Kwote.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/report/")
public class ReportController {

    @Autowired
    ReportService reportService;

    @PostMapping
    ResponseEntity<?> reportUser(@RequestHeader(name="Authorization") String token, @RequestBody CreateReportDto createReportDto) {
        reportService.reportUser(createReportDto, token);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
    @GetMapping
    ResponseEntity<List<ReportDto>> getAllReports() {
        return ResponseEntity.ok(reportService.getAllReports());
    }

    @PutMapping
    ResponseEntity<ReportDto> updateReport(@RequestBody ReportDto reportDto) {
        return ResponseEntity.ok(reportService.updateReport(reportDto));
    }

    @GetMapping("user/")
    ResponseEntity<List> getUserReports(@RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(reportService.getUserReports(token));
    }

    @DeleteMapping("{reportId}")
    void deleteReport(@PathVariable Integer reportId) {
        reportService.deleteReport(reportId);
    }
}



