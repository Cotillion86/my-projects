package backend.Kwote.controllers;

import backend.Kwote.dtos.MessageIncomingDto;
import backend.Kwote.dtos.MessageOutgoingDto;
import backend.Kwote.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class ChatController {

    @Autowired
    SimpMessagingTemplate simpMessagingTemplate;
    @Autowired
    MessageService messageService;

//    return ist nur notwendig für die anfrage macht mit den socket antworten nichts
    @MessageMapping("adminMessage") // /app/message
    @SendTo("adminchat/public")
    public MessageOutgoingDto recieveAdminMessage(@Payload MessageIncomingDto messageIncomingDto){
        return MessageOutgoingDto.builder().build();
    }

    @MessageMapping("privateMessage")
    public MessageOutgoingDto recievePrivateMessage(@Payload MessageIncomingDto messageIncomingDto){
        MessageOutgoingDto messageOutgoingDto = messageService.saveMessage(messageIncomingDto);
        simpMessagingTemplate.convertAndSendToUser(messageIncomingDto.getRecipientId().toString(), "/private", messageOutgoingDto); // /user/{{name}}/private
        return messageOutgoingDto;
    }
}
