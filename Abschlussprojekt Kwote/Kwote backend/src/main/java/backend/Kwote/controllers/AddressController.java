package backend.Kwote.controllers;

import backend.Kwote.dtos.CreateAddressDto;
import backend.Kwote.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/address/")
@EnableMethodSecurity
public class AddressController {

    @Autowired
    AddressService addressService;

    @PostMapping()
        public ResponseEntity<?> postAddress(@RequestBody CreateAddressDto createAddressDto) {
        return ResponseEntity.ok(addressService.createAddress(createAddressDto));

        }
  @PutMapping()
        public ResponseEntity<?> putAddress(@RequestBody CreateAddressDto createAddressDto, @RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(addressService.putAddress(createAddressDto, token));
        }

    @GetMapping()
    public ResponseEntity<?> getAddress(@RequestHeader (name="Authorization") String token) {
        return ResponseEntity.ok(addressService.getAddress(token));
    }

    @DeleteMapping("{addressId}")
    public void deleteAddress(@PathVariable Integer addressId) {
        addressService.deleteAddress(addressId);
    }



}
