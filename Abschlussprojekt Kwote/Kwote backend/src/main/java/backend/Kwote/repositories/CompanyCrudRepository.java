package backend.Kwote.repositories;

import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.TradeEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CompanyCrudRepository extends CrudRepository<CompanyEntity, Integer> {

    List<CompanyEntity> findByTradeEntityList(TradeEntity tradeEntity);
    Optional<CompanyEntity> findByUserEntity(UserEntity userEntity);

    @Query("SELECT COUNT(c) FROM CompanyEntity c")
    Integer countAllCompanies();

    List<CompanyEntity> findAll();
}
