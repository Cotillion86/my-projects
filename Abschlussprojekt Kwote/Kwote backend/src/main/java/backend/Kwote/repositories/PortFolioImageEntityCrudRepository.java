package backend.Kwote.repositories;

import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.PortFolioImageEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PortFolioImageEntityCrudRepository extends CrudRepository <PortFolioImageEntity, Integer> {

    List<PortFolioImageEntity> findAllByCompanyEntity (CompanyEntity companyEntity);
}
