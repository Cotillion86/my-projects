package backend.Kwote.repositories;

import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.RatingEntity;
import backend.Kwote.entities.TradeEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface TradeCrudRepository extends CrudRepository<TradeEntity, Integer> {
    Optional <TradeEntity> findByTrade(String Trade);
    List<TradeEntity> findByProjectEntityList(ProjectEntity projectEntity);

    List<TradeEntity> findAllByOrderByCreatedAtAsc(Pageable pageable);
}
