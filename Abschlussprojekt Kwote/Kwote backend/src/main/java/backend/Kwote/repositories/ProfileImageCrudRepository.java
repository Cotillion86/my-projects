package backend.Kwote.repositories;

import backend.Kwote.entities.ProfileImageEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ProfileImageCrudRepository extends CrudRepository <ProfileImageEntity, Integer> {
    Optional<ProfileImageEntity> findProfileImageEntityByUserEntity(UserEntity userEntity);
}
