package backend.Kwote.repositories;

import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.ReportEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportCrudRepository extends CrudRepository<ReportEntity, Integer> {

    List<ReportEntity> findAllByReportedByUser(UserEntity userEntity);

    Optional<ReportEntity> findByReportedByUserAndReportedProjectEntity(UserEntity userEntity, ProjectEntity projectEntity);
}
