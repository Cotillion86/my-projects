package backend.Kwote.repositories;

import backend.Kwote.entities.ChatRoomEntity;
import backend.Kwote.entities.MessageEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface MessageCrudRepository extends CrudRepository <MessageEntity, Integer> {
    List <MessageEntity> findAllByChatRoomEntity (ChatRoomEntity chatRoomEntity);
    Optional<MessageEntity> findTopBySenderAndChatRoomEntityOrderByCreatedAtDesc(UserEntity sender, ChatRoomEntity chatRoomEntity);
}
