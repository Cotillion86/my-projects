package backend.Kwote.repositories;

import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.CompanyImageEntity;
import backend.Kwote.entities.ProfileImageEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface CompanyProfileImageCrudRepository extends CrudRepository <CompanyImageEntity, Integer> {
    Optional<CompanyImageEntity> findProfileImageEntityByUserEntity(UserEntity userEntity);
}
