package backend.Kwote.repositories;

import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.TradeEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProjectCrudRepository extends CrudRepository<ProjectEntity, Integer> {

    List<ProjectEntity> findAllByUserEntity(UserEntity userEntity);
    List<ProjectEntity> findProjectEntitiesByTradeEntityList (TradeEntity tradeEntity);


    @Query("SELECT COUNT(p) FROM ProjectEntity p")
    Integer countAllProjects();

}
