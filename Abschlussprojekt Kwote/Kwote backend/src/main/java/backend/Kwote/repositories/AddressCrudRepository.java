package backend.Kwote.repositories;

import backend.Kwote.entities.AddressEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AddressCrudRepository extends CrudRepository<AddressEntity, Integer> {
    Optional<AddressEntity> findAddressEntitiesByUserEntityAndPrimaryAddressIsTrue(UserEntity userEntity);
}
