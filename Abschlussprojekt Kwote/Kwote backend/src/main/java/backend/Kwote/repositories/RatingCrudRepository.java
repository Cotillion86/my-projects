package backend.Kwote.repositories;

import backend.Kwote.entities.RatingEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface RatingCrudRepository extends CrudRepository<RatingEntity, Integer> {
    List<RatingEntity> findAllByOrderByCreatedAtDesc(Pageable pageable);
    List<RatingEntity> findAllByCompanyIdOrderByCreatedAtDesc(Integer companyId);

    @Query("SELECT AVG(r.rating) FROM RatingEntity r WHERE r.companyId = ?1")
    Double findAverageRatingByCompanyId(Integer companyId);

    @Query("SELECT COUNT(r) FROM RatingEntity r WHERE r.companyId = ?1")
    Integer countRatingsByCompanyId(Integer companyId);

    @Query("SELECT COUNT(r) FROM RatingEntity r")
    Integer countAllRatings();

    @Query("SELECT r FROM RatingEntity r WHERE r.companyId = ?1 ORDER BY r.createdAt DESC")
    List<RatingEntity> findLatestRatingByCompanyId(Integer companyId, Pageable pageable);
}
