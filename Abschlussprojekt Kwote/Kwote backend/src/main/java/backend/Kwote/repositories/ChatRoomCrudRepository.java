package backend.Kwote.repositories;

import backend.Kwote.entities.ChatRoomEntity;
import backend.Kwote.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ChatRoomCrudRepository extends CrudRepository <ChatRoomEntity, Integer> {
    Optional <ChatRoomEntity> findByChatIdByUsers (String chatIdByUsers);
    List<ChatRoomEntity> findAllByInitiatorOrRecipient(UserEntity initiator, UserEntity recipient);

}
