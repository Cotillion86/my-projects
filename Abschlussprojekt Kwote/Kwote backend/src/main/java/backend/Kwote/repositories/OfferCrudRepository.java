package backend.Kwote.repositories;

import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.OfferEntity;
import backend.Kwote.entities.ProjectEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OfferCrudRepository extends CrudRepository<OfferEntity, Integer> {

    List<OfferEntity> findAllByProjectEntity(ProjectEntity projectEntity);
    List<OfferEntity> findAllByCompanyEntity(CompanyEntity companyEntity);
    Optional<OfferEntity> findByCompanyEntityAndProjectEntity(CompanyEntity companyEntity, ProjectEntity projectEntity);

}
