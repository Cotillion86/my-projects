package backend.Kwote.repositories;

import backend.Kwote.entities.ProfileImageEntity;
import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.ProjectImageEntity;
import org.springframework.data.repository.CrudRepository;

public interface ProjectImageCrudRepository extends CrudRepository <ProjectImageEntity, Integer> {
    Iterable <ProjectImageEntity> findAllByProjectEntity (ProjectEntity projectEntity);
}
