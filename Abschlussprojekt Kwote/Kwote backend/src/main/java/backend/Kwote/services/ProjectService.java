package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.*;
import backend.Kwote.repositories.*;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ProjectService {
    @Autowired
    ProjectCrudRepository projectCrudRepository;
    @Autowired
    TradeCrudRepository tradeCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    OfferCrudRepository offerCrudRepository;
    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    JwtService jwtService;
    @Autowired
    CompanyService companyService;
    @Autowired
    ProjectImageCrudRepository projectImageCrudRepository;
    @Autowired
    TradeService tradeService;
    @Autowired
    UserService userService;
    @Autowired
    AddressCrudRepository addressCrudRepository;

    @Autowired
    AddressService addressService;

    @Autowired
    ReportCrudRepository reportCrudRepository;

    public ProjectDto postProject(String token, CreateProjectDto createProjectDto) {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        AddressEntity addressEntity = addressCrudRepository.findById(createProjectDto.getAddressId()).get();

        ProjectEntity projectEntity = new ProjectEntity()
                .projectName(createProjectDto.getProjectName())
                .projectText(createProjectDto.getProjectText())
                .addressEntity(addressEntity)
                .searchRadius(createProjectDto.getSearchRadius())
                .userEntity(userEntity);

        tradeService.addTradesToEntity(createProjectDto.getTrades(), projectEntity);
        userCrudRepository.save(userEntity);

        addressEntity.projectEntityList().add(projectEntity);
        addressCrudRepository.save(addressEntity);


        return projectDtoFromProjectEntity(projectEntity);


    }

    public List<ProjectDto> getProjects() {

        return StreamSupport.stream(projectCrudRepository.findAll().spliterator(), false)
                .map(projectEntity -> projectDtoFromProjectEntity(projectEntity))
                .collect((Collectors.toList()));

    }

    public List<ProjectWithOfferDto> getProjectsWithOffer(String token) {
        List<ProjectWithOfferDto> projectWithOffers = new ArrayList<>();
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);

        List<ProjectEntity> projectEntityList = projectCrudRepository.findAllByUserEntity(userEntity);

        for (ProjectEntity projectEntity : projectEntityList) {
            if (!projectEntity.deleted()) {
                ProjectWithOfferDto projectWithOfferDto = convertProjectEntityToProjectFromOfferDto(projectEntity);

                List<OfferEntity> offerEntityList = offerCrudRepository.findAllByProjectEntity(projectEntity);

                if (!offerEntityList.isEmpty()) {
                    projectWithOfferDto.setOffers(convertOfferEntityListToDtoList(offerEntityList));
                }

                projectWithOffers.add(projectWithOfferDto);
            }
        }

        return projectWithOffers;
    }


    //Überarbeitet damit im ProjectDto Projekte mit überlappenden Trades dennoch nur 1x mit den entsprechenden Trades vorkommen.
    public List<ProjectDto> getProjectsByTrade(String token) {

        List<ProjectDto> projects = new ArrayList<>();
        HashSet<ProjectEntity> projectEntityHashSet = new HashSet<>();
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        Optional<CompanyEntity> companyEntityOptional = companyCrudRepository.findByUserEntity(userEntity);
        if (companyEntityOptional.isPresent()) {
            List<TradeEntity> companyTradelist = StreamSupport.stream(companyEntityOptional.get().tradeEntityList().spliterator(), false)
                    .collect(Collectors.toList());
            
            //Zuerst werden alle Projekte mit den gewünschten Trades in ein Set geladen, um Duplikate zu vermeiden
            for (TradeEntity tradeEntity : companyTradelist) {
                List<ProjectEntity> projectEntitiesList = projectCrudRepository.findProjectEntitiesByTradeEntityList(tradeEntity);
                for (ProjectEntity projectEntity : projectEntitiesList) {

                    if (!projectEntity.offerAccepted()
                            && !projectEntity.deleted()
                            && reportCrudRepository.findByReportedByUserAndReportedProjectEntity(userEntity, projectEntity).isEmpty()
                            && offerCrudRepository.findByCompanyEntityAndProjectEntity(companyEntityOptional.get(), projectEntity).isEmpty()) {

                        projectEntityHashSet.add(projectEntity);
                    }
                }
            }

            //Dann werden jene Trades von den ProjectEntities herausgeschnitten, die die Company nicht anbietet und schließlich zur Liste projects hinzugefügt.
            for (ProjectEntity projectEntity : projectEntityHashSet) {
                projects.add(projectDtoFromProjectEntity(cutTradesFromEntity(projectEntity, companyTradelist)));
            }

            return projects;
        }
        else{
            return null;
        }
    }

    public ProjectDto updateProject(ProjectDto projectDto) {

        ProjectEntity projectEntity = projectCrudRepository.findById(projectDto.getProjectId()).get();
        projectEntity.deleted(projectDto.getDeleted());
        projectEntity.reported(projectDto.getReported());
        projectCrudRepository.save(projectEntity);


        //überprüft ob deleted true ist und setzt dann alle offer auf rejected. Falls es keine OfferEntity mit ProjectEntity gibt wird PE gelöscht
        if (projectDto.getDeleted()) {
            List<OfferEntity> offerEntityList = offerCrudRepository.findAllByProjectEntity(projectCrudRepository.findById(projectDto.getProjectId()).get());
            if (!offerEntityList.isEmpty()) {
                StreamSupport.stream(offerEntityList.spliterator(), false)
                        .map(offerEntity -> offerEntity.rejected(true))
                        .forEach(offerCrudRepository::save);
            } else {

                deleteProject(projectDto.getProjectId());
                if (!projectDto.getAddressDto().isPrimaryAddress()) {
                    addressService.deleteAddress(projectDto.getAddressDto().getAddressId());
                }
                return null;
            }

        }

        projectEntity = updateProjectEntityFromProjectDto(projectCrudRepository.findById(projectDto.getProjectId()).get(), projectDto);
        projectCrudRepository.save(projectEntity);
        return projectDtoFromProjectEntity(projectEntity);
    }

    //Sicheres Löschen von Project, alle Tradeverbindungen werden entfernt.
    public void deleteProject(int projectId) {

        ProjectEntity projectEntity = projectCrudRepository.findById(projectId).get();

        if (projectEntity.deleted() && !projectEntity.reported()) {
            tradeCrudRepository.findByProjectEntityList(projectEntity)
                    .forEach(tradeEntity -> {
                        tradeEntity.projectEntityList().remove(projectEntity);
                        tradeCrudRepository.save(tradeEntity);
                    });
            projectEntity.tradeEntityList().clear();
            projectCrudRepository.save(projectEntity);
            projectCrudRepository.deleteById(projectEntity.projectId());
        }
    }


    public ProjectDto projectDtoFromProjectEntity(ProjectEntity projectEntity) {

        List<ImageUrlDto> imageUrlDtos =
                StreamSupport.stream(projectImageCrudRepository.findAllByProjectEntity(projectEntity).spliterator(), false)
                        .map(entity -> ImageUrlDto.builder()
                                .imageUrl("http://localhost:8080/api/images/project/" + entity.imageId())
                                .imageId(entity.imageId())
                                .beforeAfter(entity.beforeAfter())
                                .build())
                        .collect((Collectors.toList()));

        return ProjectDto.builder()
                .projectId(projectEntity.projectId())
                .projectName(projectEntity.projectName())
                .projectText(projectEntity.projectText())
                .trades(StreamSupport.stream(projectEntity.tradeEntityList().spliterator(), false)
                        .map(entity -> entity.trade())
                        .collect(Collectors.toList()))
                .userId(projectEntity.userEntity().userId())
                .addressDto(projectEntity.addressEntity()!=null?
                        addressService.convertAddressEntityToAddressDto(projectEntity.addressEntity())
                                :null)
                .finished(projectEntity.finished())
                .offerAccepted(projectEntity.offerAccepted())
                .reported(projectEntity.reported())
                .imageUrlDtoList(imageUrlDtos)
                .searchRadius(projectEntity.searchRadius())
                .deleted(projectEntity.deleted())
                .build();
    }

    public ProjectWithOfferDto convertProjectEntityToProjectFromOfferDto(ProjectEntity projectEntity) {

        return ProjectWithOfferDto.builder()
                .projectId(projectEntity.projectId())
                .projectName(projectEntity.projectName())
                .projectText(projectEntity.projectText())
                .trades(StreamSupport.stream(projectEntity.tradeEntityList().spliterator(), false)
                        .map(entity -> entity.trade())
                        .collect(Collectors.toList()))
                .finished(projectEntity.finished())
                .searchRadius(projectEntity.searchRadius())
                .offerAccepted(projectEntity.offerAccepted())
                .deleted(projectEntity.deleted())
                .reported(projectEntity.reported())
                .rated(projectEntity.rated())
                .addressDto(projectEntity.addressEntity()!=null?
                        addressService.convertAddressEntityToAddressDto(projectEntity.addressEntity())
                        : null
                        )
                .userDto(userService.convertUserEntityToUserDto(projectEntity.userEntity()))
                .imageUrlDtoList(StreamSupport.stream(projectImageCrudRepository.findAllByProjectEntity(projectEntity).spliterator(), false)
                        .map(entity -> ImageUrlDto.builder()
                                .beforeAfter(entity.beforeAfter())
                                .imageUrl("http://localhost:8080/api/images/project/" + entity.imageId())
                                .imageId(entity.imageId())
                                .build())
                        .collect(Collectors.toList()))
                .build();
    }

    private OfferDto convertOfferEntityToOfferDto(OfferEntity offerEntity) {

        return OfferDto.builder()
                .offerId(offerEntity.offerId())
                .price(offerEntity.price())
                .offerText(offerEntity.offerText())
                .projectId(offerEntity.projectEntity().projectId())
                .companyDto(companyService.convertCompanyEntityToCompanyDto(offerEntity.companyEntity()))
                .confirmed(offerEntity.confirmed())
                .rejected(offerEntity.rejected())
                .accepted(offerEntity.accepted())
                .build();
    }


    public List<OfferDto> convertOfferEntityListToDtoList(Iterable<OfferEntity> offerEntityIterable) {
        List<OfferDto> offers = new ArrayList<>();
        for (OfferEntity offerEntity : offerEntityIterable) {
            if (!offerEntity.rejected()) {
                offers.add(convertOfferEntityToOfferDto(offerEntity));
            }
        }
        return offers;
    }

    // Methode updateProjectEntityFromProjectDto aktualisiert Imageliste und OfferEntity nicht. Denke von deren Seite aus durchführen
    private ProjectEntity updateProjectEntityFromProjectDto(ProjectEntity projectEntity, ProjectDto projectDto) {

        updateTradesOfProjectEntity(projectDto.getTrades(), projectEntity);


        return projectEntity
                .projectName(projectDto.getProjectName())
                .projectText(projectDto.getProjectText())
                .finished(projectDto.getFinished())
                .reported(projectDto.getReported())
                .searchRadius(projectDto.getSearchRadius())
                .offerAccepted(projectDto.getOfferAccepted())
                .deleted(projectDto.getDeleted());
    }


    List<TradeEntity> addTradesToProject(List<String> tradeList, ProjectEntity projectEntity) {
        List<TradeEntity> tradeEntityList = new ArrayList<>();
        for (String tradeItem : tradeList) {

            TradeEntity tradeEntity = tradeCrudRepository.findByTrade(tradeItem).get();
            tradeEntityList.add(tradeEntity);
            tradeEntity.projectEntityList().add(projectEntity);
            tradeCrudRepository.save(tradeEntity);
        }
        return tradeEntityList;
    }

    //Methode schneidet aus ProjectEntity die Trades heraus, die nicht in der mitgegebenen companyTradeList vorhanden sind. Gebraucht um Dtos für die Company zu bauen.
    ProjectEntity cutTradesFromEntity(ProjectEntity projectEntity, List<TradeEntity> companyTradeList) {
        Iterator<TradeEntity> iterator = projectEntity.tradeEntityList().iterator();
        while (iterator.hasNext()) {
            TradeEntity tradeEntity = iterator.next();
            if (!companyTradeList.contains(tradeEntity)) {
                iterator.remove();
            }
        }
        return projectEntity;
    }

    //Methode führt sicheres Aktualisieren der Trades von Projects durch. Geht beide Listen durch, prüft ob neue Einträge hinzugekommen, oder alte entfernt wurden.
    void updateTradesOfProjectEntity(List<String> newTradesNameList, ProjectEntity projectEntity) {
        List<String> oldTradeNameList = (tradeCrudRepository.findByProjectEntityList(projectEntity)).stream()
                .map(tradeEntity -> tradeEntity.trade())
                .collect(Collectors.toList());
        for (String tradeName : oldTradeNameList) {
            if (!newTradesNameList.contains(tradeName)) {
                tradeCrudRepository.findByTrade(tradeName).get().projectEntityList().remove(projectEntity);
            }
        }

        projectEntity.tradeEntityList().clear();
        for (String newTrade : newTradesNameList) {
            if (!oldTradeNameList.contains(newTrade)) {
                if (tradeCrudRepository.findByTrade(newTrade).isEmpty()) {
                    tradeCrudRepository.save(new TradeEntity().trade(newTrade));
                }
                TradeEntity tradeEntity = tradeCrudRepository.findByTrade(newTrade).get();
                projectEntity.tradeEntityList().add(tradeEntity);
                tradeEntity.projectEntityList().add(projectEntity);
                tradeCrudRepository.save(tradeEntity);
                projectCrudRepository.save(projectEntity);
            } else {
                projectEntity.tradeEntityList().add(tradeCrudRepository.findByTrade(newTrade).get());
                projectCrudRepository.save(projectEntity);
            }
        }

    }


    //Kalkuliert die Distanz zwischen 2 Geopunkten und gibt Ergebnis in km zurück
    public double calculateDistance(double lat1, double lon1, double lat2, double lon2) {
        final double earthRadius = 6371;

        double dLat = toRadians(lat2 - lat1);
        double dLon = toRadians(lon2 - lon1);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(toRadians(lat1)) * Math.cos(toRadians(lat2)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double calculatedDistance = earthRadius * c;

        return calculatedDistance;

    }

    public double toRadians(double degrees) {
        return degrees * (Math.PI / 180);
    }

    public boolean isDistanceTooGreat(double distance, double searchRadius) {
        if (distance <= searchRadius) {
            return true;
        } else {
            return false;
        }
    }
}


