package backend.Kwote.services;


import backend.Kwote.dtos.*;
import backend.Kwote.entities.AddressEntity;
import backend.Kwote.entities.ProfileImageEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.AddressCrudRepository;
import backend.Kwote.repositories.ProfileImageCrudRepository;
import backend.Kwote.repositories.UserCrudRepository;
import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;


@Service
public class AuthService {
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    JwtService jwtService;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    ProfileImageCrudRepository profileImageCrudRepository;
    @Autowired
    AddressCrudRepository addressCrudRepository;

    public AuthResponseDto getAuthentication(String token){
        return convertUserEntityToAuthResponseDto(jwtService.getUserEntityByTokenSubject(token));
    }

    public AuthResponseDto loginUser(LoginDto loginDto){
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDto.getEmail(),
                        loginDto.getPassword()
                )
        );
        var user = userCrudRepository.findByEmail(loginDto.getEmail()).orElseThrow();
       return convertUserEntityToAuthResponseDto(user);
    }

    public AuthResponseDto createUser(RegisterDto newUser) {
        UserEntity newUserEntity = userEntityFromRegisterDto(newUser, new UserEntity());
        userCrudRepository.save(newUserEntity);
        return convertUserEntityToAuthResponseDto(newUserEntity);
    }

  private UserEntity userEntityFromRegisterDto(RegisterDto newUser, UserEntity userEntity) {
        return userEntity
                .firstName(newUser.getFirstName())
                .lastName(newUser.getLastName())
                .email(newUser.getEmail())
                .password(passwordEncoder.encode(newUser.getPassword()))
                .role(newUser.getRole());
    }

    public AuthResponseDto convertUserEntityToAuthResponseDto(UserEntity userEntity) {
        Map claims = new HashMap<String, Object>();
        claims.put("role", userEntity.role());
        Optional<AddressEntity> addressEntityOptional = addressCrudRepository.findAddressEntitiesByUserEntityAndPrimaryAddressIsTrue(userEntity);
        Optional<ProfileImageEntity> profileImageEntity = profileImageCrudRepository.findProfileImageEntityByUserEntity(userEntity);
        return AuthResponseDto.builder()
                .jwt(jwtService.generateToken(claims, userEntity))
                .user(UserDto.builder()
                        .userId(userEntity.userId())
                        .firstName(userEntity.firstName())
                        .lastName(userEntity.lastName())
                        .email(userEntity.email())
                        .role(userEntity.role())
                        .address(addressEntityOptional.isPresent()
                                ?
                                AddressDto
                                        .builder()
                                        .streetName(addressEntityOptional.get().streetName())
                                        .streetNumber(addressEntityOptional.get().streetNumber())
                                        .city(addressEntityOptional.get().city())
                                        .zip(addressEntityOptional.get().zip())
                                        .country(addressEntityOptional.get().country())
                                        .build()
                                :
                                null
                        )
                        .profileImageUrl(profileImageEntity.isPresent()?
                                "http://localhost:8080/api/images/profile/"+profileImageEntity.get().imageId()
                                :null)
                        .build())
                .build();
    }


}
