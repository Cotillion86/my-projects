package backend.Kwote.services;

import backend.Kwote.entities.*;
import backend.Kwote.enums.BeforeAfter;
import backend.Kwote.repositories.*;
import backend.Kwote.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Service
public class ImageService {
    @Autowired
    ProfileImageCrudRepository profileImageCrudRepository;
    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    ProjectImageCrudRepository projectImageCrudRepository;
    @Autowired
    CompanyProfileImageCrudRepository companyProfileImageCrudRepository;
    @Autowired
    PortFolioImageEntityCrudRepository portFolioImageEntityCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    ProjectCrudRepository projectCrudRepository;

    @Autowired
    JwtService jwtService;

    public String uploadCompanyPortfolioImage(MultipartFile file, Integer companyId) throws IOException {
        PortFolioImageEntity Imagedata = portFolioImageEntityCrudRepository.save(
                new PortFolioImageEntity()
                        .imageName(file.getOriginalFilename())
                        .type(file.getContentType())
                        .imageData(ImageUtils.compressImage(file.getBytes()))
                        .companyEntity(companyCrudRepository.findById(companyId).get()));
        if (Imagedata != null) {
            return "http://localhost:8080/api/images/portfolio/" + Imagedata.imageId();
        } else return null;
    }

    public byte[] downloadCompanyPortfolioImage(Integer imageId) {
        Optional<PortFolioImageEntity> image = portFolioImageEntityCrudRepository.findById(imageId);
        byte[] imageData = ImageUtils.decompressImage(image.get().imageData());
        return imageData;
    }

    public String uploadCompanyProfileImage(MultipartFile file, String token) throws IOException {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        Optional <CompanyImageEntity> companyImageEntityOptional = companyProfileImageCrudRepository.findProfileImageEntityByUserEntity(userEntity);
        if (companyImageEntityOptional.isPresent()){
            CompanyImageEntity companyImageEntity = companyProfileImageCrudRepository.save(
            companyImageEntityOptional.get().imageData(ImageUtils.compressImage(file.getBytes()))
            );
             return "http://localhost:8080/api/images/company/" + companyImageEntity.imageId();

        }else{
            CompanyImageEntity companyImageEntity = companyProfileImageCrudRepository.save(
                    new CompanyImageEntity()
                            .imageName(file.getOriginalFilename())
                            .type(file.getContentType())
                            .imageData(ImageUtils.compressImage(file.getBytes()))
                            .userEntity(userEntity)
            );
            return "http://localhost:8080/api/images/company/" + companyImageEntity.imageId();
        }


    }

    public byte[] downloadCompanyProfileImage(Integer imageId) {
        Optional<CompanyImageEntity> image = companyProfileImageCrudRepository.findById(imageId);
        byte[] imageData = ImageUtils.decompressImage(image.get().imageData());
        return imageData;
    }

    public String uploadProfileImage(MultipartFile file, Integer userId) throws IOException {
        Optional<ProfileImageEntity> profileImageEntityOptional = profileImageCrudRepository.findProfileImageEntityByUserEntity(userCrudRepository.findById(userId).get());
        if (profileImageEntityOptional.isPresent()) {
            ProfileImageEntity profileImage = profileImageCrudRepository.save(
                    profileImageEntityOptional.get().imageData(ImageUtils.compressImage(file.getBytes()))
            );
            return "http://localhost:8080/api/images/portfolio/" + profileImage.imageId();
        } else {
            ProfileImageEntity profileImage = profileImageCrudRepository.save(
                    new ProfileImageEntity()
                            .imageName(file.getOriginalFilename())
                            .type(file.getContentType())
                            .imageData(ImageUtils.compressImage(file.getBytes()))
                            .userEntity(userCrudRepository.findById(userId).get()));
            return "http://localhost:8080/api/images/portfolio/" + profileImage.imageId();
        }
    }

    public byte[] downloadProfileImage(Integer imageId) {
        Optional<ProfileImageEntity> image = profileImageCrudRepository
                .findById(imageId);
        byte[] imageData = ImageUtils.decompressImage(image.get().imageData());
        return imageData;
    }

    public String uploadProjectImage(MultipartFile file, Integer projectId, Integer beforeAfter) throws IOException {
        ProjectImageEntity Imagedata = projectImageCrudRepository.save(
                new ProjectImageEntity()
                        .imageName(file.getOriginalFilename())
                        .type(file.getContentType())
                        .beforeAfter(beforeAfter == 0 ? BeforeAfter.BEFORE : BeforeAfter.AFTER)
                        .imageData(ImageUtils.compressImage(file.getBytes()))
                        .projectEntity(projectCrudRepository.findById(projectId).get()));

        return "File Uploaded succesfully : " + file.getOriginalFilename();

    }

//Variante für mehrere Images funktioniert noch nicht
//    public String uploadProjectImages(List<MultipartFile> files, Integer projectId) throws IOException {
//        ProjectEntity projectEntity = projectCrudRepository.findById(projectId).get();
//        for (MultipartFile file :files) {
//            projectImageCrudRepository.save(
//                    new ProjectImageEntity()
//                            .imageName(file.getOriginalFilename())
//                            .type(file.getContentType())
//                            .imageData(ImageUtils.compressImage(file.getBytes()))
//                            .projectEntity(projectEntity));
//        }
//        return "Files Uploaded succesfully !";
//    }

    public byte[] downloadProjectImage(Integer fileId) {
        Optional<ProjectImageEntity> image = projectImageCrudRepository.findById(fileId);
        byte[] imageData = ImageUtils.decompressImage(image.get().imageData());
        return imageData;
    }

    public String deleteCompanyPortfolioImage(Integer imageId, String token) {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        Optional<CompanyEntity> companyEntity = companyCrudRepository.findByUserEntity(userEntity);
        Optional<PortFolioImageEntity> portFolioImageEntity = portFolioImageEntityCrudRepository.findById(imageId);
        if (companyEntity.isPresent() && portFolioImageEntity.isPresent()
                &&companyEntity.get()==portFolioImageEntity.get().companyEntity()) {
            portFolioImageEntityCrudRepository.deleteById(imageId);
            return "ok";
        } else {
            return "no company found";
        }
    }
}
