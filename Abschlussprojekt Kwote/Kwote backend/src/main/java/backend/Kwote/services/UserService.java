package backend.Kwote.services;


import backend.Kwote.dtos.*;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;



@Service
public class UserService {
    @Autowired
    UserCrudRepository userCrudRepository;

    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    CompanyService companyService;
    @Autowired
    AddressService addressService;
    @Autowired
    JwtService jwtService;


    public UserDto editUser(int userId, EditUserDto editUser) {
        UserEntity userEntity = userCrudRepository.findById(userId).get();
        UserEntity editedUserEntity = editUserEntity(editUser, userEntity);
        userCrudRepository.save(editedUserEntity);
        return convertUserEntityToUserDto(editedUserEntity);
    }

    public UserDto editUserOwn(String token, UserEditSelfDto userEditSelfDto) {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        UserEntity editedUserEntity = editUserOwnEntity(userEditSelfDto, userEntity);
        userCrudRepository.save(editedUserEntity);
        return convertUserEntityToUserDto(editedUserEntity);
    }

    public void deleteUser(int userId) {
        userCrudRepository.deleteById(userId);
    }

    public List<UserDto> getAllUsers() {
        return convertUserEntityListToDtoList(userCrudRepository.findAll());
    }

    public UserDto getUser(int userId) {
        return convertUserEntityToUserDto(userCrudRepository.findById(userId).get());
    }

    private UserEntity editUserEntity(EditUserDto editUser, UserEntity userEntity) {
        return userEntity
                .firstName(editUser.getFirstName())
                .lastName(editUser.getLastName())
                .email(editUser.getEmail())
//                .password(passwordEncoder.encode(editUser.getPassword()))
                .locked(editUser.isLocked())
                .role(editUser.getRole());

    }
    private UserEntity editUserOwnEntity(UserEditSelfDto editUser, UserEntity userEntity) {
        return userEntity
                .firstName(editUser.getFirstName())
                .lastName(editUser.getLastName());
    }

    public LockedUserListDto getLockedUsers() {
        return  LockedUserListDto.builder()
                .userDtoList(convertUserEntityListToDtoList(userCrudRepository.findAllByLockedIsTrue()))
                .build();
    }

    public List<UserDto> convertUserEntityListToDtoList(Iterable<UserEntity> userEntityIterable) {
        List<UserDto> userDtoList = new ArrayList<>();
        for (UserEntity userEntity : userEntityIterable) {
            userDtoList.add(convertUserEntityToUserDto(userEntity));
        }
        return userDtoList;
    }

    public UserDto convertUserEntityToUserDto(UserEntity userEntity) {
        return UserDto.builder()
                .userId(userEntity.userId())
                .firstName(userEntity.firstName())
                .lastName(userEntity.lastName())
                .email(userEntity.email())
                .locked(userEntity.locked())
                .role(userEntity.role())
                .build();
    }


    public List<UserWithCompanyAndAddressDto> getAllUserWithCompanyAndAddress() {
        List<UserWithCompanyAndAddressDto> userWithCompanyAndAddressDtoList = new ArrayList<>();
        List<UserDto> userDtoList = getAllUsers();
        for(UserDto user : userDtoList) {
                CompanyDto companyDto = companyService.getCompanyByUserId(user.getUserId());

            AddressDto addressDto = addressService.getAddressByUserId(user.getUserId());

            if(companyDto.getCompanyId() != 0 && addressDto.getAddressId() != 0) {
                userWithCompanyAndAddressDtoList.add(UserWithCompanyAndAddressDto.builder()
                        .userDto(user)
                        .addressDto(addressDto)
                        .companyDto(companyDto)
                        .build()
                );

            } else if (companyDto.getCompanyId() == 0 && addressDto.getAddressId() != 0) {
                userWithCompanyAndAddressDtoList.add(UserWithCompanyAndAddressDto.builder()
                        .userDto(user)
                        .addressDto(addressDto)
                        .build()
                );

            } else if (companyDto.getCompanyId() != 0 && addressDto.getAddressId() == 0) {
                userWithCompanyAndAddressDtoList.add(UserWithCompanyAndAddressDto.builder()
                        .userDto(user)
                        .companyDto(companyDto)
                        .build()
                );
            } else {
                userWithCompanyAndAddressDtoList.add(UserWithCompanyAndAddressDto.builder()
                        .userDto(user)
                        .build()
                );
            }
        }

        return userWithCompanyAndAddressDtoList;
    }
}
