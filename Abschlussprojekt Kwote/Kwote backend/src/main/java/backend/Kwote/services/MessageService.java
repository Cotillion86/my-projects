package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.ChatRoomEntity;
import backend.Kwote.entities.MessageEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.enums.Role;
import backend.Kwote.repositories.ChatRoomCrudRepository;
import backend.Kwote.repositories.MessageCrudRepository;
import backend.Kwote.repositories.OfferCrudRepository;
import backend.Kwote.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {
    @Autowired
    MessageCrudRepository messageCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    JwtService jwtService;
    @Autowired
    ChatRoomCrudRepository chatRoomCrudRepository;
    @Autowired
    OfferCrudRepository  offerCrudRepository;

    public ResponseEntity createChatroom (String token, MessagesCreateChatroomDto messagesCreateChatroomDto){
        UserEntity user = jwtService.getUserEntityByTokenSubject(token);

            UserEntity userTwo = (user.role()== Role.USER) ?
                    offerCrudRepository.findById(messagesCreateChatroomDto.getOfferId()).get().companyEntity().userEntity()
                    :
                    offerCrudRepository.findById(messagesCreateChatroomDto.getOfferId()).get().projectEntity().userEntity();

        Integer[] userIds = {user.userId(), userTwo.userId()};
        String chatIdByUsers = Math.min(userIds[0], userIds[1]) + "_" + Math.max(userIds[0], userIds[1]);
        Optional<ChatRoomEntity> chatRoomEntityOptional = chatRoomCrudRepository.findByChatIdByUsers(chatIdByUsers);
        if (chatRoomEntityOptional.isPresent()){
            return ResponseEntity.ok(chatIdByUsers);
        }else{
            ChatRoomEntity chatRoomEntity= chatRoomCrudRepository.save(
                    new ChatRoomEntity()
                            .chatIdByUsers(chatIdByUsers)
                            .initiator(user)
                            .recipient(userTwo)
            );
            return ResponseEntity.ok(chatIdByUsers);
        }
    }

    public ResponseEntity getMyLastMessage (String token, Integer chatId){
        UserEntity user = jwtService.getUserEntityByTokenSubject(token);
        ChatRoomEntity chatRoomEntity = chatRoomCrudRepository.findById(chatId).get();
        Optional <MessageEntity> messageEntityOptional=messageCrudRepository.findTopBySenderAndChatRoomEntityOrderByCreatedAtDesc(user, chatRoomEntity);
        if (messageEntityOptional.isPresent()){
            return ResponseEntity.ok(convertMessageEntityToMessageOutgoingDto(messageEntityOptional.get()));
        }else {
            return ResponseEntity.badRequest().body("No Message Found!");
        }
    }


    public MessagesGetAllDto getAllMessagesByUserDto(String token) {
        UserEntity user = jwtService.getUserEntityByTokenSubject(token);
        List<ChatRoomEntity> chatRoomEntities = chatRoomCrudRepository.findAllByInitiatorOrRecipient(user, user);
        List<MessagesByUserDto> messagesByUserDtos = new ArrayList<>();
        for (ChatRoomEntity chatRoomEntity : chatRoomEntities) {
            UserEntity otherUser = chatRoomEntity.initiator()==user ? chatRoomEntity.recipient() : chatRoomEntity.initiator();
            List<MessageEntity> messageEntities = messageCrudRepository.findAllByChatRoomEntity(chatRoomEntity);
            List<MessageOutgoingDto> messageOutgoingDtos = new ArrayList<>();
            for (MessageEntity messageEntity : messageEntities) {
                messageOutgoingDtos.add(convertMessageEntityToMessageOutgoingDto(messageEntity));
            }
            messagesByUserDtos.add(
                    MessagesByUserDto.builder()
                            .chatId(chatRoomEntity.chatId())
                            .chatIdString(chatRoomEntity.chatIdByUsers())
                            .withName(otherUser.firstName()+" "+otherUser.lastName())
                            .withId(otherUser.userId())
                            .show(false)
                            .messages(messageOutgoingDtos)
                            .build()
            );
        }
        return MessagesGetAllDto.builder().chats(messagesByUserDtos).build();
    }

    public MessageOutgoingDto saveMessage(MessageIncomingDto messageIncomingDto) {
        UserEntity sender = userCrudRepository.findById(messageIncomingDto.getSenderId()).get();
        UserEntity recipient = userCrudRepository.findById(messageIncomingDto.getRecipientId()).get();
        Integer[] userIds = {messageIncomingDto.getSenderId(), messageIncomingDto.getRecipientId()};
        String chatIdByUsers = Math.min(userIds[0], userIds[1]) + "_" + Math.max(userIds[0], userIds[1]);
        Optional<ChatRoomEntity> chatRoomEntityOptional = chatRoomCrudRepository.findByChatIdByUsers(chatIdByUsers);
        if (chatRoomEntityOptional.isPresent()) {
            MessageEntity messageEntity = messageCrudRepository.save(
                    new MessageEntity()
                            .sender(sender)
                            .chatRoomEntity(chatRoomEntityOptional.get())
                            .content(messageIncomingDto.getContent())
            );
            return convertMessageEntityToMessageOutgoingDto(messageEntity);
        } else {
            ChatRoomEntity chatRoomEntity = chatRoomCrudRepository.save(
                    new ChatRoomEntity()
                            .chatIdByUsers(chatIdByUsers)
                            .initiator(sender)
                            .recipient(recipient)
            );
            MessageEntity messageEntity = messageCrudRepository.save(
                    new MessageEntity()
                            .sender(sender)
                            .chatRoomEntity(chatRoomEntity)
                            .content(messageIncomingDto.getContent())
            );
            return convertMessageEntityToMessageOutgoingDto(messageEntity);
        }
    }

    public MessageOutgoingDto convertMessageEntityToMessageOutgoingDto(MessageEntity messageEntity) {
        return MessageOutgoingDto.builder()
                .chatId(messageEntity.chatRoomEntity().chatId())
                .messageId(messageEntity.messageId())
                .senderId(messageEntity.sender().userId())
                .senderName(messageEntity.sender().firstName())
                .content(messageEntity.content())
                .timestamp(messageEntity.createdAt())
                .build();
    }
}
