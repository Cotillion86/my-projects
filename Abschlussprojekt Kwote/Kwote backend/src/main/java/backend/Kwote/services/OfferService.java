package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.OfferEntity;
import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.CompanyCrudRepository;
import backend.Kwote.repositories.OfferCrudRepository;
import backend.Kwote.repositories.ProjectCrudRepository;
import backend.Kwote.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OfferService {

    @Autowired
    OfferCrudRepository offerCrudRepository;
    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    ProjectCrudRepository projectCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    CompanyService companyService;
    @Autowired
    JwtService jwtService;
    @Autowired
    ProjectService projectService;

    public OfferDto postOffer(CreateOfferDto createOfferDto) {
        ProjectEntity projectEntity = projectCrudRepository.findById(createOfferDto.getProjectId()).get();
        CompanyEntity companyEntity = companyCrudRepository.findById(createOfferDto.getCompanyId()).get();

        OfferEntity offerEntity = createOfferEntityFromDto(companyEntity, projectEntity, createOfferDto);
        offerCrudRepository.save(offerEntity);

        companyEntity.offerEntityList().add(offerEntity);
        companyCrudRepository.save(companyEntity);

        projectEntity.offerEntityList().add(offerEntity);
        projectCrudRepository.save(projectEntity);

        return convertOfferEntityToOfferDto(offerEntity);

    }

    public List<OfferDto> getOffers() {

        return convertOfferEntityListToDtoList(offerCrudRepository.findAll());
    }

    public List<OfferDto> getOffersForUser(int userId) {
        ArrayList<OfferDto> offers = new ArrayList<>();
        List<ProjectEntity> projectEntityList = projectCrudRepository.findAllByUserEntity(userCrudRepository.findById(userId).get());
        for (ProjectEntity projectEntity : projectEntityList) {
            List<OfferEntity> offerEntityList = offerCrudRepository.findAllByProjectEntity(projectEntity);
            if (!offerEntityList.isEmpty()) {
                offers.addAll(convertOfferEntityListToDtoList(offerEntityList));
            }
        }
        return offers;
    }

    public AllOffersAndProjectListDto getOffersByCompany(String token) {
        ArrayList<OfferByCompanyDto> offersAndProjects = new ArrayList<>();
        ArrayList<Integer> projectIds = new ArrayList<>();
        UserEntity user = jwtService.getUserEntityByTokenSubject(token);
        Optional<CompanyEntity> company = companyCrudRepository.findByUserEntity(user);
        if(company.isPresent()){
            List<OfferEntity> offerEntityList = offerCrudRepository.findAllByCompanyEntity(company.get());

            for (OfferEntity offerEntity : offerEntityList) {
                if (!offerEntity.deleted()) {
                    offersAndProjects.add(
                            OfferByCompanyDto.builder()
                                    .offer(convertOfferEntityToOfferDto(offerEntity))
                                    .project(projectService.projectDtoFromProjectEntity(projectService.cutTradesFromEntity(offerEntity.projectEntity(), company.get().tradeEntityList())))
                                    .build());
                    projectIds.add(offerEntity.projectEntity().projectId());
                }
            }
            return AllOffersAndProjectListDto.builder()
                    .offeredProjects(projectIds)
                    .offers(offersAndProjects)
                    .build();
        }
        else {
            return null;
        }
    }

    public OfferDto updateOffer(int offerId, OfferDto offerDto) {
        OfferEntity updatedOfferEntity = updateOfferEntity(offerCrudRepository.findById(offerId).get(), offerDto);
        offerCrudRepository.save(updatedOfferEntity);

        projectCrudRepository.save(
                projectCrudRepository.findById(updatedOfferEntity.projectEntity().projectId()).get()
                        .offerAccepted(updatedOfferEntity.confirmed())
        );
        return (convertOfferEntityToOfferDto(updatedOfferEntity));
    }

    //Setzt OfferEntity auf deleted und löscht sie wenn nicht reported. Falls reported true, dann verhindert boolean deleted ein Auftauchen im Frontend außer für Admins.
    // Falls reported false wird OfferEntity gelöscht und falls ProjectEntity keine Offer mehr hat und auf delete gesetzt ist wird ebenfalls gelöscht
    public void deleteOffer(int offerId) {
        OfferEntity offerEntity = offerCrudRepository.findById(offerId).get();
        offerEntity.deleted(true);
        if (offerEntity.reported()) {
            offerCrudRepository.save(offerEntity);
            return;
        }
        ProjectEntity projectEntity = offerCrudRepository.findById(offerId).get().projectEntity();
        offerCrudRepository.deleteById(offerId);

        if(projectEntity != null) {

            projectService.updateProject(projectService.projectDtoFromProjectEntity(projectEntity));
        }
    }

    public OfferEntity createOfferEntityFromDto(CompanyEntity companyEntity, ProjectEntity projectEntity, CreateOfferDto createOfferDto) {

        return new OfferEntity()
                .offerText(createOfferDto.getOfferText())
                .price(createOfferDto.getPrice())
                .projectEntity(projectEntity)
                .companyEntity(companyEntity);
    }

    public OfferDto convertOfferEntityToOfferDto(OfferEntity offerEntity) {

        return OfferDto.builder()
                .offerId(offerEntity.offerId())
                .price(offerEntity.price())
                .offerText(offerEntity.offerText())
                .projectId(offerEntity.projectEntity().projectId())
                .companyDto(companyService.convertCompanyEntityToCompanyDto(offerEntity.companyEntity()))
                .accepted(offerEntity.accepted())
                .confirmed(offerEntity.confirmed())
                .rejected(offerEntity.rejected())
                .deleted(offerEntity.deleted())
                .build();
    }


    private List<OfferDto> convertOfferEntityListToDtoList(Iterable<OfferEntity> offerEntityIterable) {
        List<OfferDto> offers = new ArrayList<>();
        for (OfferEntity offerEntity : offerEntityIterable) {
            offers.add(convertOfferEntityToOfferDto(offerEntity));
        }
        return offers;
    }

    private OfferEntity updateOfferEntity(OfferEntity offerEntity, OfferDto offerDto) {
        return offerEntity
                .offerText(offerDto.getOfferText())
                .price(offerDto.getPrice())
                .projectEntity(projectCrudRepository.findById(offerDto.getProjectId()).get())
                .companyEntity(companyCrudRepository.findById(offerDto.getCompanyDto().getCompanyId()).get())
                .accepted(offerDto.isAccepted())
                .confirmed(offerDto.isConfirmed())
                .deleted(offerDto.isDeleted())
                .reported(offerDto.isReported())
                .rejected(offerDto.isRejected());


    }


}
