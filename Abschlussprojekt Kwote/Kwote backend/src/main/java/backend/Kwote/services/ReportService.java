package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.OfferEntity;
import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.ReportEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.OfferCrudRepository;
import backend.Kwote.repositories.ProjectCrudRepository;
import backend.Kwote.repositories.ReportCrudRepository;
import backend.Kwote.repositories.UserCrudRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class ReportService {

    @Autowired
    ReportCrudRepository reportCrudRepository;

    @Autowired
    UserCrudRepository userCrudRepository;

    @Autowired
    JwtService jwtService;
    @Autowired
    OfferCrudRepository offerCrudRepository;
    @Autowired
    UserService userService;
    @Autowired
    OfferService offerService;
    @Autowired
    ProjectService projectService;
    @Autowired
    ProjectCrudRepository projectCrudRepository;


    public void reportUser(CreateReportDto createReportDto, String token) {
        ReportEntity reportEntity = new ReportEntity();
        UserEntity reportedByUser = jwtService.getUserEntityByTokenSubject(token);
        reportEntity.reportedByUser(reportedByUser);
        reportEntity.reportText(createReportDto.getReportText());

        if (createReportDto.getReportedUserId() != null) {
            UserEntity reportedUserEntity = userCrudRepository.findById(createReportDto.getReportedUserId()).get();
            reportEntity.reportedUser(reportedUserEntity);

            reportCrudRepository.save(reportEntity);

            reportedUserEntity.reportedUserEntityList().add(reportEntity);
            reportedByUser.reportedByUserEntityList().add(reportEntity);

            userCrudRepository.save(reportedByUser);
            userCrudRepository.save(reportedUserEntity);

        } else if (createReportDto.getReportedOfferId() != null) {
            OfferEntity reportedOfferEntity = offerCrudRepository.findById(createReportDto.getReportedOfferId()).get();
            reportedOfferEntity.reported(true);
            reportEntity.reportedOfferEntity(reportedOfferEntity);

            reportCrudRepository.save(reportEntity);

            reportedOfferEntity.reportEntityList().add(reportEntity);
            reportedByUser.reportedUserEntityList().add(reportEntity);

            offerCrudRepository.save(reportedOfferEntity);
            userCrudRepository.save(reportedByUser);

        } else if (createReportDto.getReportedProjectId() != null) {
            ProjectEntity reportedProjectEntity = projectCrudRepository.findById(createReportDto.getReportedProjectId()).get();
            reportedProjectEntity.reported(true);
            reportEntity.reportedProjectEntity(reportedProjectEntity);

            reportCrudRepository.save(reportEntity);

            reportedProjectEntity.reportEntityList().add(reportEntity);
            reportedByUser.reportedUserEntityList().add(reportEntity);

            projectCrudRepository.save(reportedProjectEntity);
            userCrudRepository.save(reportedByUser);
        }

    }

    public List<ReportDto> getAllReports() {
        List<ReportDto> reportDtoList = StreamSupport.stream(reportCrudRepository.findAll().spliterator(), false)
                .map(reportEntity -> convertReportEntityToReportDto(reportEntity))
                .collect(Collectors.toList());
        return reportDtoList;
    }

    public ReportDto convertReportEntityToReportDto(ReportEntity reportEntity) {

        ReportDto reportDto = ReportDto.builder()
                .reportId(reportEntity.reportId())
                .reportedByUser(userService.convertUserEntityToUserDto(reportEntity.reportedByUser()))
                .reportText(reportEntity.reportText())
                .createdAt(reportEntity.createdAt().toString())
                .finished(reportEntity.finished())
                .build();

        if (reportEntity.reportedUser() != null) {
            reportDto.setReportedUser(userService.convertUserEntityToUserDto(reportEntity.reportedUser()));
            reportDto.setReportedOffer(OfferDto.builder()
                    .offerId(0)
                    .build());
            reportDto.setReportedProject(ProjectWithOfferDto.builder()
                    .projectId(0)
                    .build());

        } else if (reportEntity.reportedOfferEntity() != null) {
            reportDto.setReportedOffer(offerService.convertOfferEntityToOfferDto(reportEntity.reportedOfferEntity()));
            reportDto.setReportedUser(UserDto.builder()
                    .userId(0)
                    .build());
            reportDto.setReportedProject(ProjectWithOfferDto.builder()
                    .projectId(0)
                    .build());


        } else if (reportEntity.reportedProjectEntity() != null) {
            ProjectEntity projectEntity = projectCrudRepository.findById(reportEntity.reportedProjectEntity().projectId()).get();
            reportDto.setReportedProject(projectService.convertProjectEntityToProjectFromOfferDto(projectEntity));
            reportDto.getReportedProject().setOffers(projectService.convertOfferEntityListToDtoList(projectEntity.offerEntityList()));
            reportDto.setReportedUser(UserDto.builder()
                    .userId(0)
                    .build());
            reportDto.setReportedOffer(OfferDto.builder()
                    .offerId(0)
                    .build());
        }

        return reportDto;
    }

    public ReportDto updateReport(ReportDto reportDto) {
        ReportEntity reportEntity = reportCrudRepository.findById(reportDto.getReportId()).get();
        reportEntity.finished(reportDto.getFinished());
        reportCrudRepository.save(reportEntity);
        return convertReportEntityToReportDto(reportEntity);
    }


    public List<ReportDto> getUserReports(String token) {
        UserEntity userEntity = userCrudRepository.findById(jwtService.getUserEntityByTokenSubject(token).userId()).get();
        List<ReportDto> reportDtoList = StreamSupport.stream(reportCrudRepository.findAllByReportedByUser(userEntity).spliterator(), false)
                .map(reportEntity -> convertReportEntityToReportDto(reportEntity))
                .collect(Collectors.toList());
        return reportDtoList;

    }

    public void deleteReport(Integer reportId) {
        reportCrudRepository.deleteById(reportId);

    }
}