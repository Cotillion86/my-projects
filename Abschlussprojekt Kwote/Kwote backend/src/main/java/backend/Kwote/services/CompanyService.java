package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.*;
import backend.Kwote.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
public class CompanyService {

    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    CompanyProfileImageCrudRepository companyProfileImageCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    TradeCrudRepository tradeCrudRepository;
    @Autowired
    TradeService tradeService;

    public CompanyDto createCompany(CreateCompanyDto createCompanyDto) {
        CompanyEntity companyEntity = createCompanyEntityFromCreateCompanyDto(userCrudRepository.findById(createCompanyDto.getUserId()).get(), createCompanyDto);

        tradeService.addTradesToEntity(createCompanyDto.getTrades(), companyEntity);

        return convertCompanyEntityToCompanyDto(companyEntity);
    }

    public CompanyDto getCompanyByUserId(int userId) {
        if (companyCrudRepository.findByUserEntity(userCrudRepository.findById(userId).get()).isPresent()) {
            return convertCompanyEntityToCompanyDto(companyCrudRepository.findByUserEntity(userCrudRepository.findById(userId).get()).get());
        }
        return CompanyDto.builder()
                .companyId(0)
                .build();
    }

    public List<CompanyDto> getCompanyByTrade(TradePostDto trade) {

        List<CompanyEntity> companyEntityList = companyCrudRepository.findByTradeEntityList(tradeCrudRepository.findByTrade(trade.getTrade()).get());
        List<CompanyDto> companyDtoList = new ArrayList<>();
        for (CompanyEntity companyEntity : companyEntityList) {
            companyDtoList.add(convertCompanyEntityToCompanyDto(companyEntity));

        }
        return companyDtoList;
    }

    private CompanyEntity createCompanyEntityFromCreateCompanyDto(UserEntity userEntity, CreateCompanyDto createCompanyDto) {
        return new CompanyEntity()
                .companyName(createCompanyDto.getCompanyName())
                .homepage((createCompanyDto.getHomepage()))
                .registerNumber(createCompanyDto.getRegisterNumber())
                .userEntity(userEntity);

    }

    CompanyDto convertCompanyEntityToCompanyDto(CompanyEntity companyEntity) {
        Optional<CompanyImageEntity> companyImageEntityOptional = companyProfileImageCrudRepository.findProfileImageEntityByUserEntity(companyEntity.userEntity());
        return CompanyDto.builder()
                .companyId(companyEntity.companyId())
                .companyName(companyEntity.companyName())
                .homepage(companyEntity.homepage())
                .trade(companyEntity.tradeEntityList().stream()
                        .map(TradeEntity::trade)
                        .collect((Collectors.toList())))
                .registerNumber(companyEntity.registerNumber())
                .companyUserId(companyEntity.userEntity().userId())
                .companyProfileUrl(companyImageEntityOptional.isPresent()?
                        "http://localhost:8080/api/images/company/"+companyImageEntityOptional.get().imageId()
                        :
                        null )
                .build();
    }

    public List<CompanyDto> getAllCompanies() {
        List<CompanyEntity> companyEntityList = companyCrudRepository.findAll();
        return (companyEntityList).stream()
                .map(companyEntity -> convertCompanyEntityToCompanyDto(companyEntity))
                .toList();
    }
}


