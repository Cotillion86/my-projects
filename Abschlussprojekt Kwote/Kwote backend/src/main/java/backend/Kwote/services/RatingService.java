package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.CompanyImageEntity;
import backend.Kwote.entities.OfferEntity;
import backend.Kwote.entities.RatingEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.CompanyProfileImageCrudRepository;
import backend.Kwote.repositories.OfferCrudRepository;
import backend.Kwote.repositories.ProjectCrudRepository;
import backend.Kwote.repositories.RatingCrudRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RatingService {
    @Autowired
    RatingCrudRepository ratingCrudRepository;
    @Autowired
    OfferCrudRepository offerCrudRepository;
    @Autowired
    ProjectCrudRepository projectCrudRepository;
    @Autowired
    ProjectService projectService;
    @Autowired
    CompanyProfileImageCrudRepository companyProfileImageCrudRepository;
    @Autowired
    JwtService jwtService;

    public RatingResponseDto postRating(RatingDto ratingDto) {
        OfferEntity offerEntity = offerCrudRepository.findById(ratingDto.getOfferId()).get();
        RatingEntity ratingEntity = new RatingEntity()
                .rating(ratingDto.getRating())
                .ratingText(ratingDto.getRatingText())
                .offerEntity(offerEntity)
                .companyId(offerEntity.companyEntity().companyId());
        ratingCrudRepository.save(ratingEntity);
        projectCrudRepository.save(
                projectCrudRepository.findById(offerEntity.projectEntity().projectId()).get().rated(true)
        );
        return ratingEntityToRatingResponseDto(ratingEntity);
    }

//    baut eine Liste mit max 15 der neuesten einträge des Ratingsrepository aus TestimonialDtos mit leicht veränderten CompanyDtos
    public TestimonialSampleResponseDto getSampleTestimonials() {
        int pageSize = 15;
        int pageNumber = 0;
        Sort sort = Sort.by("createdAt").descending();
        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        List<RatingEntity> ratingEntities = ratingCrudRepository.findAllByOrderByCreatedAtDesc(pageable);

        List <TestimonialDto> testimonialDtoList = new ArrayList<>();
        for (RatingEntity ratingEntity : ratingEntities) {
           Optional<CompanyImageEntity> companyImageEntityOptional = companyProfileImageCrudRepository.findProfileImageEntityByUserEntity(ratingEntity.offerEntity().companyEntity().userEntity());
           testimonialDtoList.add(
                    TestimonialDto.builder()
                            .ratingId(ratingEntity.ratingId())
                            .rating(ratingEntity.rating())
                            .ratingText(ratingEntity.ratingText())
                            .testimonialCompanyDto(
                                    TestimonialCompanyDto.builder()
                                            .companyId(ratingEntity.offerEntity().companyEntity().companyId())
                                            .companyName(ratingEntity.offerEntity().companyEntity().companyName())
                                            .homepage(ratingEntity.offerEntity().companyEntity().homepage())
                                            .companyScore(ratingCrudRepository.findAverageRatingByCompanyId(ratingEntity.companyId()))
                                            .companyProfileImageUrl(companyImageEntityOptional.isPresent()?
                                                    "http://localhost:8080/api/images/company/" + companyImageEntityOptional.get().imageId()
                                                    :
                                                    null)
                                            .companyScoreNumber(ratingCrudRepository.countRatingsByCompanyId(ratingEntity.companyId()))
                                            .build())
                            .projectDto(projectService.projectDtoFromProjectEntity(ratingEntity.offerEntity().projectEntity()))
                            .build()
            );
        }
        return TestimonialSampleResponseDto.builder()
                .testimonialDtos(testimonialDtoList)
                .build();
    }

    public RatingResponseDto ratingEntityToRatingResponseDto(RatingEntity ratingEntity) {
        return RatingResponseDto.builder()
                .ratingId(ratingEntity.ratingId())
                .rating(ratingEntity.rating())
                .ratingText(ratingEntity.ratingText())
                .projectId(ratingEntity.offerEntity().projectEntity().projectId())
                .companyId(ratingEntity.offerEntity().companyEntity().companyId())
                .byFirstName(ratingEntity.offerEntity().projectEntity().userEntity().firstName())
                .byLastName(ratingEntity.offerEntity().projectEntity().userEntity().lastName())
                .createdAt(ratingEntity.createdAt())
                .build();
    }


}
