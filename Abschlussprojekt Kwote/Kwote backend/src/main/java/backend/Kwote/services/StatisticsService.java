package backend.Kwote.services;

import backend.Kwote.dtos.*;
import backend.Kwote.entities.*;
import backend.Kwote.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class StatisticsService {
    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    RatingCrudRepository ratingCrudRepository;
    @Autowired
    RatingService ratingService;
    @Autowired
    ProjectCrudRepository projectCrudRepository;
    @Autowired
    ProjectService projectService;
    @Autowired
    TradeCrudRepository tradeCrudRepository;
    @Autowired
    ProfileImageCrudRepository profileImageCrudRepository;
    @Autowired
    CompanyProfileImageCrudRepository companyProfileImageCrudRepository;
    @Autowired
    PortFolioImageEntityCrudRepository portFolioImageEntityCrudRepository;
    @Autowired
    CompanyService companyService;

    public CompanyProfilePageDto getCompanyProfilePage(Integer companyId) {
        Optional<CompanyEntity> companyEntityOptional = companyCrudRepository.findById(companyId);
        if (companyEntityOptional.isPresent()) {
            CompanyEntity companyEntity = companyEntityOptional.get();
            List<RatingEntity> ratingEntityList = ratingCrudRepository.findAllByCompanyIdOrderByCreatedAtDesc(companyId);
            Optional<CompanyImageEntity> companyImageEntityOptional = companyProfileImageCrudRepository.findProfileImageEntityByUserEntity(companyEntity.userEntity());
            List<RatingResponseDto> ratingResponseDtos = new ArrayList<>();
            for (RatingEntity ratingEntity : ratingEntityList) {
                ratingResponseDtos.add(
                        ratingService.ratingEntityToRatingResponseDto(
                                ratingEntity
                        )
                );
            }

            List <String> portFolioImageUrlList=new ArrayList<>();
            List <PortFolioImageEntity> portFolioImageEntities = portFolioImageEntityCrudRepository.findAllByCompanyEntity(companyEntity);
            for (PortFolioImageEntity portFolioImage : portFolioImageEntities){
                portFolioImageUrlList.add(
                        "http://localhost:8080/api/images/portfolio/"+portFolioImage.imageId()
                );
            }

            return CompanyProfilePageDto.builder()
                    .companyDto(companyService.convertCompanyEntityToCompanyDto(companyEntity))
                    .companyProfileUrl(companyImageEntityOptional.isPresent()?
                            "http://localhost:8080/api/images/company/"+companyImageEntityOptional.get().imageId()
                            :
                            null )
                    .companyRating(ratingCrudRepository.findAverageRatingByCompanyId(companyEntity.companyId()))
                    .numberOfRatings(ratingCrudRepository.countRatingsByCompanyId(companyEntity.companyId()))
                    .ratingResponseDtos(ratingResponseDtos)
                    .portfolioImageUrls(portFolioImageUrlList)
                    .build();
        } else {
            return null;
        }
    }


    public AllCompaniesByTradeForOverviewDto getAllCompaniesByTradeForOverview(String trade){
        TradeEntity tradeEntity = tradeCrudRepository.findByTrade(trade).get();
        List<CompanyForOverViewDto> companyForOverViewDtos= new ArrayList<>();
        List <CompanyEntity> companyEntityList = companyCrudRepository.findByTradeEntityList(tradeEntity);
        for (CompanyEntity companyEntity :companyEntityList){
            Pageable pageable = PageRequest.of(0, 1);
            List<RatingEntity> latestRating = ratingCrudRepository.findLatestRatingByCompanyId(companyEntity.companyId(), pageable);
            companyForOverViewDtos.add(
                    CompanyForOverViewDto.builder()
                            .companyDto(companyService.convertCompanyEntityToCompanyDto(companyEntity))
                            .companyRating(ratingCrudRepository.findAverageRatingByCompanyId(companyEntity.companyId()))
                            .numberOfRatings(ratingCrudRepository.countRatingsByCompanyId(companyEntity.companyId()))
                            .lastRatingForCompany(
                                    latestRating.size()>0?
                                    ratingService.ratingEntityToRatingResponseDto(
                                           latestRating.get(0)
                                    )
                                            :
                                            null
                            )
                            .projectOfLastRating(
                                    latestRating.size()>0?
                                            projectService.projectDtoFromProjectEntity(latestRating.get(0).offerEntity().projectEntity())
                                            :
                                            null
                            )
                            .build()
            );
        }
        return AllCompaniesByTradeForOverviewDto.builder().trade(tradeEntity.trade()).bannerUrl(tradeEntity.bannerImageUrl()).companies(companyForOverViewDtos).build();
    }
    public StatisticsCountDto getStatistics() {
        return StatisticsCountDto.builder()
                .companiesNumber(companyCrudRepository.countAllCompanies())
                .projectsNumber(projectCrudRepository.countAllProjects())
                .ratingsNumber(ratingCrudRepository.countAllRatings())
                .build();
    }

    public CompaniesForHomeViewDto getSampleCompaniesForHomeView() {

        int pageSize = 4;
        int pageNumber = 0;
        Sort sort = Sort.by("createdAt").ascending();
        Pageable pageable = PageRequest.of(pageNumber, pageSize, sort);

        List<TradeEntity> tradeEntities = tradeCrudRepository.findAllByOrderByCreatedAtAsc(pageable);
        Collections.shuffle(tradeEntities);

        List<CompanyForHomeViewDto> companyForHomeViewDtos = new ArrayList<>();


        for (TradeEntity tradeEntity : tradeEntities) {
            List<CompanyEntity> companyEntityList = companyCrudRepository.findByTradeEntityList(tradeEntity);
            Collections.shuffle(companyEntityList);

            List<String> profileImageUrls = new ArrayList<>();

            int numImages = Math.min(2, companyEntityList.size());
            for (int i = 0; i < numImages; i++) {
                CompanyEntity company = companyEntityList.get(i);
                Optional<CompanyImageEntity> companyImageEntityOptional = companyProfileImageCrudRepository.findProfileImageEntityByUserEntity(company.userEntity());

                profileImageUrls.add( companyImageEntityOptional.isPresent()?
                        "http://localhost:8080/api/images/company/" + companyImageEntityOptional.get().imageId()
                        :
                        null);
            }

            companyForHomeViewDtos.add(
                    CompanyForHomeViewDto.builder()
                            .numberOfCompanies(companyEntityList.size())
                            .sampleCompanyImgUrl(profileImageUrls)
                            .trade(tradeEntity.trade())
                            .bannerImageUrl(tradeEntity.bannerImageUrl())
                            .cheekyName(tradeEntity.cheekyName())
                            .iconName(tradeEntity.iconName())
                            .build()
            );
        }
        return CompaniesForHomeViewDto.builder()
                .companyForHomeViewDtos(companyForHomeViewDtos)
                .build();
        }
}
