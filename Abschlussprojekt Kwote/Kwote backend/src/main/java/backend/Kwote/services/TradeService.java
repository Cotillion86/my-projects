package backend.Kwote.services;

import backend.Kwote.dtos.TradeDto;
import backend.Kwote.dtos.TradeListDto;
import backend.Kwote.dtos.TradePostDto;
import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.ProjectEntity;
import backend.Kwote.entities.TradeEntity;
import backend.Kwote.repositories.CompanyCrudRepository;
import backend.Kwote.repositories.ProjectCrudRepository;
import backend.Kwote.repositories.TradeCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TradeService {
    @Autowired
    TradeCrudRepository tradeCrudRepository;
    @Autowired
    CompanyCrudRepository companyCrudRepository;
    @Autowired
    ProjectCrudRepository projectCrudRepository;

    public TradePostDto postTrade(TradePostDto tradePostDto) {
        TradeEntity tradeEntity = new TradeEntity()
                .trade(tradePostDto.getTrade());
        tradeCrudRepository.save(
                tradeEntity
        );
        return TradePostDto.builder()
                .trade(tradeEntity.trade())
                .build();
    }

    public TradeListDto getTrades() {
        Iterable<TradeEntity> tradeEntityIterable = tradeCrudRepository.findAll();

        TradeListDto tradeListDto = TradeListDto.builder()
                .tradeList(new ArrayList<>())
                .build();

        for (TradeEntity tradeEntity : tradeEntityIterable) {
            tradeListDto.getTradeList().add(tradeEntity.trade());
        }
        return tradeListDto;
    }

    TradeDto convertTradeEntityToTradeDto(TradeEntity tradeEntity) {
        return TradeDto.builder()
                .tradeId(tradeEntity.tradeId())
                .trade(tradeEntity.trade())
                .build();
    }

    //Fügt je nach Entity dem Project/der Company Trades aus einer Liste hinzu. Falls Trade noch nicht vorhanden, wird dieser erstellt.
    public void addTradesToEntity(List<String> tradeList, Object object) {
        for (String tradeItem : tradeList) {
            if (tradeCrudRepository.findByTrade(tradeItem).isEmpty()) {
                tradeCrudRepository.save(new TradeEntity().trade(tradeItem));
            }

            TradeEntity tradeEntity = tradeCrudRepository.findByTrade(tradeItem).get();

            if (object instanceof CompanyEntity) {
                ((CompanyEntity) object).tradeEntityList().add(tradeEntity);
                tradeEntity.companyEntityList().add((CompanyEntity) object);
                companyCrudRepository.save((CompanyEntity) object);
                tradeCrudRepository.save(tradeEntity);

            } else if (object instanceof ProjectEntity) {
                ((ProjectEntity) object).tradeEntityList().add(tradeEntity);
                tradeEntity.projectEntityList().add((ProjectEntity) object);
                projectCrudRepository.save((ProjectEntity) object);
                tradeCrudRepository.save(tradeEntity);
            }
        }
    }
}