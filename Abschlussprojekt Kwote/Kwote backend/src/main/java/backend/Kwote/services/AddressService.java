package backend.Kwote.services;

import backend.Kwote.dtos.AddressDto;
import backend.Kwote.dtos.CreateAddressDto;
import backend.Kwote.entities.AddressEntity;
import backend.Kwote.entities.CompanyEntity;
import backend.Kwote.entities.UserEntity;
import backend.Kwote.repositories.AddressCrudRepository;
import backend.Kwote.repositories.UserCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    AddressCrudRepository addressCrudRepository;
    @Autowired
    UserCrudRepository userCrudRepository;
    @Autowired
    JwtService jwtService;

    public AddressDto getAddress(String token) {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);

        Optional<AddressEntity> addressEntity = addressCrudRepository.findAddressEntitiesByUserEntityAndPrimaryAddressIsTrue(userEntity);
        if (addressEntity.isPresent()){
            return convertAddressEntityToAddressDto(addressEntity.get());
        }
        else return null;
    }
    public AddressDto createAddress(CreateAddressDto createAddressDto) {
        UserEntity userEntity = userCrudRepository.findById(createAddressDto.getUserId()).get();
        AddressEntity addressEntity = (addressCrudRepository.save(createAddressEntityFromDto(userEntity, createAddressDto)));
        userCrudRepository.save(userEntity);
        return convertAddressEntityToAddressDto(addressEntity);
    }

    public AddressDto putAddress(CreateAddressDto createAddressDto, String token) {
        UserEntity userEntity = jwtService.getUserEntityByTokenSubject(token);
        Optional<AddressEntity> addressEntityOptional = addressCrudRepository.findAddressEntitiesByUserEntityAndPrimaryAddressIsTrue(userEntity);

        if (addressEntityOptional.isPresent()) {
            AddressEntity addressEntity = addressEntityOptional.get();
            addressCrudRepository.save(
                    addressEntity
                            .streetName(createAddressDto.getStreetName())
                            .city(createAddressDto.getCity())
                            .zip(createAddressDto.getZip())
                            .country(createAddressDto.getCountry())
                            .streetNumber(createAddressDto.getStreetNumber())
                            .longitude(createAddressDto.getLongitude())
                            .latitude(createAddressDto.getLatitude())
                            .primaryAddress(createAddressDto.isPrimaryAddress())

            );

            return convertAddressEntityToAddressDto(addressEntity);

        } else {
            AddressEntity addressEntity = new AddressEntity()
                    .streetName(createAddressDto.getStreetName())
                    .city(createAddressDto.getCity())
                    .zip(createAddressDto.getZip())
                    .country(createAddressDto.getCountry())
                    .streetNumber(createAddressDto.getStreetNumber())
                    .latitude(createAddressDto.getLatitude())
                    .longitude(createAddressDto.getLongitude())
                    .primaryAddress(createAddressDto.isPrimaryAddress())
                    .userEntity(userEntity);
            addressCrudRepository.save(addressEntity);
            return convertAddressEntityToAddressDto(addressEntity);
        }
    }

    private AddressEntity createAddressEntityFromDto(UserEntity userEntity, CreateAddressDto createAddressDto) {
        AddressEntity addressEntity = new AddressEntity();
        return addressEntity
                .streetName(createAddressDto.getStreetName())
                .streetNumber(createAddressDto.getStreetNumber())
                .city(createAddressDto.getCity())
                .country(createAddressDto.getCountry())
                .zip(createAddressDto.getZip())
                .longitude(createAddressDto.getLongitude())
                .latitude(createAddressDto.getLatitude())
                .primaryAddress(createAddressDto.isPrimaryAddress())
                .userEntity(userEntity);
    }

     AddressDto convertAddressEntityToAddressDto(AddressEntity addressEntity) {
        return AddressDto.builder()
                .addressId(addressEntity.addressId())
                .streetName(addressEntity.streetName())
                .streetNumber(addressEntity.streetNumber())
                .zip(addressEntity.zip())
                .city(addressEntity.city())
                .country(addressEntity.country())
                .userId(addressEntity.userEntity().userId())
                .latitude(addressEntity.latitude())
                .longitude(addressEntity.longitude())
                .primaryAddress(addressEntity.primaryAddress())
                .build();
    }

    public AddressDto getAddressByUserId(Integer userId) {
        Optional<AddressEntity> addressEntityOptional = addressCrudRepository.findById(userId);
        if (addressEntityOptional.isEmpty()) {
            return AddressDto.builder()
                    .addressId(0)
                    .build();
        } else {
            AddressEntity addressEntity = addressEntityOptional.get();
            return convertAddressEntityToAddressDto(addressEntity);
        }
    }

    public void deleteAddress(Integer addressId) {
        addressCrudRepository.deleteById(addressId);
    }
}
