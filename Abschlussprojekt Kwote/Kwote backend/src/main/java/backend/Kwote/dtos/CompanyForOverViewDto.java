package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CompanyForOverViewDto {
    CompanyDto companyDto;
    Double companyRating;
    Integer numberOfRatings;
    RatingResponseDto lastRatingForCompany;
    ProjectDto projectOfLastRating;
}
