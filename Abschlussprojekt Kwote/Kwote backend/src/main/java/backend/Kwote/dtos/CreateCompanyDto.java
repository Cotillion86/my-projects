package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class CreateCompanyDto {
    private String companyName;
    private String homepage;
    private int registerNumber;
    private List<String> trades;
    private int userId;
}
