package backend.Kwote.dtos;

import backend.Kwote.enums.BeforeAfter;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ImageUrlDto {

    private int imageId;
    String imageUrl;
    BeforeAfter beforeAfter;

}
