package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Builder
@Getter
@Setter
public class RatingResponseDto {
    Integer ratingId;
    Integer rating;
    String ratingText;
    Integer companyId;
    Integer projectId;
    String byFirstName;
    String byLastName;
    Instant createdAt;
}
