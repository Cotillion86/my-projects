package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Builder
@Getter
@Setter

public class ProjectWithOfferDto {
    private Integer projectId;
    private String projectName;
    private AddressDto addressDto;
    private double searchRadius;
    private String projectText;
    private List<String> trades;
    private UserDto userDto;
    private List<OfferDto> offers;
    private List<ImageUrlDto> imageUrlDtoList;
    private Boolean offerAccepted = false;
    private Boolean rated;
    private Boolean finished;
    private Boolean reported;
    private Boolean deleted;
}
