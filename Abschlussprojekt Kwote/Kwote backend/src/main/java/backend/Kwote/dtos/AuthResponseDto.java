package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter

public class AuthResponseDto {
    private String jwt;
    private UserDto user;
}
