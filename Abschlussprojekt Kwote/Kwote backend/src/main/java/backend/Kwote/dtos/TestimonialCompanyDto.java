package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
public class TestimonialCompanyDto {
    private int companyId;
    private String companyName;
    private String homepage;
    private double companyScore;
    private Integer companyScoreNumber;
    private String companyProfileImageUrl;
}
