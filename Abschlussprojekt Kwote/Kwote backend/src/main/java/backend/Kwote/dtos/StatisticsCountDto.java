package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class StatisticsCountDto {
    Integer companiesNumber;
    Integer projectsNumber;
    Integer ratingsNumber;
}
