package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class RatingDto {
    Integer rating;
    String ratingText;
    Integer offerId;
}
