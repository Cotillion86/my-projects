package backend.Kwote.dtos;

import lombok.*;



@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TradePostDto {
    String trade;
}
