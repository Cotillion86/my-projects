package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TestimonialDto {
    private Integer ratingId;
    private Integer rating;
    private String ratingText;
    private TestimonialCompanyDto testimonialCompanyDto;
    private ProjectDto projectDto;
}
