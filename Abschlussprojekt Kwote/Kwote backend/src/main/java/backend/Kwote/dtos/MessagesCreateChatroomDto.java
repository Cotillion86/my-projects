package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class MessagesCreateChatroomDto {
    Integer offerId;
}
