package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ReportDto {

    private Integer reportId;

    private UserDto reportedByUser;
    private UserDto reportedUser;
    private OfferDto reportedOffer;
    private ProjectWithOfferDto reportedProject;
    private String reportText;
    private Boolean finished;
    private String createdAt;


}