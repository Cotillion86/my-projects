package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class ProjectDto {
    Integer projectId;
    String projectName;
    String projectText;
    List<String> trades;
    private double searchRadius;

    int userId;
    AddressDto addressDto;
    Boolean finished;
    Boolean reported;
    Boolean offerAccepted;
    Boolean deleted;
    List<ImageUrlDto> imageUrlDtoList;
}
