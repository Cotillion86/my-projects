package backend.Kwote.dtos;

import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class MessagesByUserDto {
    Integer chatId;
    String chatIdString;
    String withName;
    Integer withId;
    Boolean show;
    List <MessageOutgoingDto> messages;
}
