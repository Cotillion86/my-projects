package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class OfferByCompanyDto {
    private OfferDto offer;
    private ProjectDto project;
}
