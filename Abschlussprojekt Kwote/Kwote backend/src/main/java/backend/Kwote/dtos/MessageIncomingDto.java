package backend.Kwote.dtos;

import backend.Kwote.enums.MessageStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Builder
@Getter
@Setter
public class MessageIncomingDto {
    private Integer senderId;
    private Integer recipientId;
    private String content;

}
