package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter

public class CreateOfferDto {
    private double price;
    private String offerText;
    private int projectId;
    private int companyId;
}