package backend.Kwote.dtos;

import backend.Kwote.enums.MessageStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.criteria.CriteriaBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.Date;

@Builder
@Getter
@Setter
public class MessageOutgoingDto {
    private Integer chatId;
    private Integer messageId;
    private Integer senderId;
    private String senderName;
    private String content;
    private Instant timestamp;
}
