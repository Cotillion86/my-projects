package backend.Kwote.dtos;

import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class OfferDto {
    private int offerId;
    private double price;
    private String offerText;
    private int projectId;
    private boolean accepted;
    private boolean confirmed;
    private boolean rejected;
    private boolean deleted;
    private boolean reported;
    private CompanyDto companyDto;

}
