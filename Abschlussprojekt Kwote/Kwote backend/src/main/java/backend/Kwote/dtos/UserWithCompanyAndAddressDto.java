package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserWithCompanyAndAddressDto {

    private UserDto userDto;
    private CompanyDto companyDto;
    private AddressDto addressDto;
}
