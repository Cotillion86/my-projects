package backend.Kwote.dtos;

import backend.Kwote.entities.UserEntity;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
public class CompanyDto {
    private int companyId;
    private String companyName;
    private String homepage;
    private int registerNumber;
    private List<String> trade = new ArrayList<>();
    private int companyUserId;
    private String companyProfileUrl;
}
