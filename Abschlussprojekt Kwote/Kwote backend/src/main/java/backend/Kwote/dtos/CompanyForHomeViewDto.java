package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class CompanyForHomeViewDto {
    Integer numberOfCompanies;
    String trade;
    String bannerImageUrl;
    String iconName;
    String cheekyName;
    List <String> sampleCompanyImgUrl;
}
