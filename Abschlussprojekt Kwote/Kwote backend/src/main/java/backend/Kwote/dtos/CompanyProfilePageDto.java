package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class CompanyProfilePageDto {
    CompanyDto companyDto;
    String companyProfileUrl;
    Double companyRating;
    Integer numberOfRatings;
    List <RatingResponseDto> ratingResponseDtos;
    List <String> portfolioImageUrls;
}
