package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
public class TestimonialSampleResponseDto {
    private List<TestimonialDto> testimonialDtos;
}
