package backend.Kwote.dtos;

import lombok.*;

import java.util.List;

@Builder
@Getter
@Setter
public class CreateProjectDto {
   private String projectName;
   private String projectText;
   private double searchRadius;

   private Integer addressId;
   private List<String> trades;
}
