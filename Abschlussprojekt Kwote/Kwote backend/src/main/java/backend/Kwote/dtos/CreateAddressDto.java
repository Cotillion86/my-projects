package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CreateAddressDto {

    private String streetName;
    private String streetNumber;
    private int zip;
    private String city;
    private String country;

    private boolean primaryAddress;
    private double longitude;
    private double latitude;
    private int userId;
}
