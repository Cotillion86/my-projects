package backend.Kwote.dtos;

import backend.Kwote.enums.BeforeAfter;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
public class LockedUserListDto {

    private List<UserDto> userDtoList;
}
