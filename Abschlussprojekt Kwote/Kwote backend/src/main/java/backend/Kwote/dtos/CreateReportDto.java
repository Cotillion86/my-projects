package backend.Kwote.dtos;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CreateReportDto {

    private Integer reportedUserId;
    private Integer reportedOfferId;
    private Integer reportedProjectId;
    private String reportText;

}
