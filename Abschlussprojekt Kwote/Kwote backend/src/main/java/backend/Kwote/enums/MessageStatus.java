package backend.Kwote.enums;

public enum MessageStatus {
    RECEIVED, DELIVERED
}
