package backend.Kwote.entities;

import backend.Kwote.enums.BeforeAfter;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name="ProjectImages")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class ProjectImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer imageId;
    private String imageName;
    private String type;
    private BeforeAfter beforeAfter;
    @ManyToOne
    @JoinColumn (name = "project")
    private ProjectEntity projectEntity;

    @Lob
@Column(length = 100000)
    private byte[] imageData;
}
