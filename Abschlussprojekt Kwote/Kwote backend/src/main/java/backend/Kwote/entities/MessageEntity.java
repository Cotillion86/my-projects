package backend.Kwote.entities;

import backend.Kwote.enums.MessageStatus;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;

import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "Messages")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class MessageEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer messageId;
    @ManyToOne
    @JoinColumn(name = "sender")
    private UserEntity sender;
    @ManyToOne
    @JoinColumn(name = "chatRoomEntity")
    private ChatRoomEntity chatRoomEntity;
    private String content;
    @CreationTimestamp
    private Instant createdAt;
//        @Enumerated (EnumType.STRING)
//        private MessageStatus status;
}
