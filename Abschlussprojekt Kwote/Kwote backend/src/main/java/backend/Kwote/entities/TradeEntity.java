package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Trades")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class TradeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer tradeId;
    String trade;
    Boolean valid = false;
    String bannerImageUrl;
    String iconName;
    String cheekyName;

    @ManyToMany (mappedBy = "tradeEntityList", cascade = CascadeType.ALL)
    List <ProjectEntity> projectEntityList = new ArrayList<>();

    @ManyToMany(mappedBy = "tradeEntityList", cascade = CascadeType.ALL)
    List<CompanyEntity> companyEntityList = new ArrayList<>();
    @CreationTimestamp
    private Instant createdAt;
    @UpdateTimestamp
    private Instant updatedAt;
}
