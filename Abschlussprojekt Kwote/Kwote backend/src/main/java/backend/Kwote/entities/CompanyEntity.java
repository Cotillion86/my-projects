package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Companies")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter

public class CompanyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer companyId;

    private String companyName;
    private String homepage;
    @Column (unique = true)
    private int registerNumber;

    @ManyToMany
    @JoinTable(name="company_trade",
            joinColumns = @JoinColumn(name= "companyId"),inverseJoinColumns = @JoinColumn(name= "tradeId")
    )
    private List<TradeEntity> tradeEntityList = new ArrayList<>();

    private boolean enabled = true;
    @CreationTimestamp
    private Instant createdAt;
    @UpdateTimestamp
    private Instant updatedAt;

    @OneToOne
    @JoinColumn(name = "userId", unique = true)
    private UserEntity userEntity;

    @OneToMany(mappedBy = "companyEntity")
    private List<OfferEntity> offerEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "companyEntity")
    private List <PortFolioImageEntity> portfolioImageEntities= new ArrayList<>();
}
