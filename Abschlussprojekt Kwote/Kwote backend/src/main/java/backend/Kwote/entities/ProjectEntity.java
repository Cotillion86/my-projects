package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Projects")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class ProjectEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer projectId;
    String projectName;
    String projectText;
    private double searchRadius;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="project_trade",
            joinColumns = @JoinColumn(name= "projectId"),inverseJoinColumns = @JoinColumn(name= "tradeId")
    )
    private List<TradeEntity> tradeEntityList = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "userId")
    UserEntity userEntity;

    @OneToMany(mappedBy = "projectEntity", cascade = CascadeType.ALL)
    private List<OfferEntity> offerEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "projectEntity",  cascade = CascadeType.REMOVE)
    private List<ProjectImageEntity> projectImageEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "reportedProjectEntity", cascade = CascadeType.ALL)
    private List<ReportEntity> reportEntityList;

    @ManyToOne
    @JoinColumn(name="addressId")
    private AddressEntity addressEntity;

    Boolean finished = false;
    Boolean offerAccepted = false;
    Boolean rated = false;
    Boolean deleted = false;

    Boolean reported = false;
}
