package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;

import java.time.Instant;
import java.util.List;

@Entity
@Table(name="Chatroom")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter

public class ChatRoomEntity {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    Integer chatId;

    String chatIdByUsers;
    @ManyToOne
    @JoinColumn (name="initiator")
    private UserEntity initiator;
    @ManyToOne
    @JoinColumn (name="recipient")
    private UserEntity recipient;
    @OneToMany (mappedBy = "chatRoomEntity")
    List <MessageEntity> messageEntityList;
    @CreationTimestamp
    Instant createdAt;
}
