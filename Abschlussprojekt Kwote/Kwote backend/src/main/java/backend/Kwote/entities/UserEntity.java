package backend.Kwote.entities;


import backend.Kwote.enums.Role;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;


import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "Users")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer userId;
    @Column(unique = true)
    private String email;
    private boolean locked = false;
    private String firstName;
    private String lastName;
    private String password;
    private Boolean enabled = true;
    @Enumerated(EnumType.STRING)
    private Role role;
    @CreationTimestamp
    private Instant createdAt;
    @UpdateTimestamp
    private Instant updatedAt;
    private boolean reported = false;

    @OneToOne(mappedBy = "userEntity")
    private CompanyEntity companyEntity;
    @OneToOne(mappedBy = "userEntity")
    private ProfileImageEntity profileImageEntity;

    @OneToMany(mappedBy = "reportedUser", cascade = CascadeType.ALL)
    private List<ReportEntity>  reportedUserEntityList;

    @OneToMany(mappedBy = "reportedByUser", cascade = CascadeType.ALL)
    private List<ReportEntity> reportedByUserEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "userEntity")
    private List<AddressEntity> addressEntityList = new ArrayList<>();

    @OneToMany(mappedBy = "userEntity")
    private List <ProjectEntity> projectEntityList = new ArrayList<>();
    @OneToMany(mappedBy = "initiator")
    private List <ChatRoomEntity> initiator;
    @OneToMany(mappedBy = "recipient")
    private List<ChatRoomEntity> recipient;
    @OneToMany(mappedBy = "sender")
    private List<MessageEntity> messageEntityList;
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !locked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}