package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Offers")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter

public class OfferEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int offerId;

    private double price;
    private String offerText;
    @CreationTimestamp
    private Instant createdAt;
    @UpdateTimestamp
    private Instant updatedAt;

    @ManyToOne
    @JoinColumn(name = "companyId")
    private CompanyEntity companyEntity;

    @ManyToOne
    @JoinColumn(name="projectId")
    private ProjectEntity projectEntity;

    @OneToMany(mappedBy = "reportedOfferEntity", cascade = CascadeType.ALL)
    private List<ReportEntity> reportEntityList;

    @OneToMany (mappedBy = "offerEntity")
    private List <RatingEntity> ratingEntityList;




    private boolean accepted = false;
    private boolean confirmed = false;
    private boolean rejected = false;
    private boolean deleted = false;
    private boolean reported = false;
}
