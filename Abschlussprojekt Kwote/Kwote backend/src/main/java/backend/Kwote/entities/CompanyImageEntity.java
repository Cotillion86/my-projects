package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Entity
@Table(name="CompanyProfileImages")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class CompanyImageEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer imageId;
    private String imageName;
    private String type;
    @OneToOne
    @JoinColumn (name= "User")
    private UserEntity userEntity;

    @Lob
  @Column(length = 100000)
    private byte[] imageData;
}
