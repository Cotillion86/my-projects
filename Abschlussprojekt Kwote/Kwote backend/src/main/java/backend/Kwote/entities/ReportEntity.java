package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.apache.catalina.User;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.util.List;

@Entity
@Table(name = "reports", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"reported_by_user", "reported_user"}),
        @UniqueConstraint(columnNames = {"reported_by_user", "reported_offer"}),
        @UniqueConstraint(columnNames = {"reported_by_user", "reported_project"}),
})

@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter
public class ReportEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer reportId;

    private String reportText;
    @CreationTimestamp
    private Instant createdAt;
    @UpdateTimestamp
    private Instant updatedAt;

    private Boolean finished = false;

    @ManyToOne
    @JoinColumn(name = "reported_by_user")
    private UserEntity reportedByUser;

    @ManyToOne
    @JoinColumn(name = "reported_user")
    private UserEntity reportedUser;

    @ManyToOne
    @JoinColumn(name = "reported_offer")
    private OfferEntity reportedOfferEntity;
    @ManyToOne
    @JoinColumn(name = "reported_project")
    private ProjectEntity reportedProjectEntity;
}
