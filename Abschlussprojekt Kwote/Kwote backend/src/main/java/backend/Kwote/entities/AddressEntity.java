package backend.Kwote.entities;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="Address")
@Accessors(chain = true, fluent = true)
@NoArgsConstructor
@Getter
@Setter

public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int addressId;
    private String streetName;
    private String streetNumber;
    private int zip;
    private String city;
    private String country;
    private double latitude;
    private double longitude;
    private Boolean primaryAddress = false;
    @ManyToOne
    @JoinColumn(name="userId")
    private UserEntity userEntity;

    @OneToMany(mappedBy = "addressEntity")
    private List<ProjectEntity> projectEntityList = new ArrayList<>();

}
