package backend.Kwote.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutHandler;

@Configuration
@EnableWebSecurity //(debug = true)
public class SecurityConfig {

    @Autowired
    AuthenticationProvider authenticationProvider;
    @Autowired
    JwtAuthenticationFilter jwtAuthenticationFilter;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {


        http
                .cors()
                .and()
                .csrf().disable()
                .authorizeHttpRequests()
//                .headers(headers -> headers.frameOptions().disable())//Damit die H2 angezeigt werden kann
                .requestMatchers("/api/**").permitAll() // Während der Entwicklung drinnen
                .requestMatchers("/api/offer/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/chat/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/messages/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/users/**").hasAnyAuthority("ADMIN", "USER")
                .requestMatchers(HttpMethod.GET,"/api/trade/").permitAll()
                .requestMatchers("/api/trade/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/address/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/project/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/api/companies/**").hasAnyAuthority("COMPANY","ADMIN")
                .requestMatchers(HttpMethod.POST,"api/report/**").hasAnyAuthority("ADMIN", "COMPANY", "USER")
                .requestMatchers(HttpMethod.PUT,"api/report/**").hasAnyAuthority("ADMIN")
                .requestMatchers(HttpMethod.GET,"api/report/**").hasAnyAuthority("ADMIN", "COMPANY", "USER")
                .requestMatchers("/api/statistics/**").permitAll()
//                .requestMatchers("/api/images/**").permitAll()
//                . in unserem Setup werden die Bildpfade direkt eingebunden und da
                .requestMatchers(HttpMethod.GET,"/api/images/project/**").permitAll()
                .requestMatchers(HttpMethod.GET,"/api/images/profile/**").permitAll()
                .requestMatchers("/api/images/**").hasAnyAuthority("ADMIN", "USER", "COMPANY")
                .requestMatchers("/v3/api-docs/**", "/swagger-ui/**", "/api/auth/**", "/api/auth/register", "/api/auth/login", "/ws/**", "/api/rating/**").permitAll()
//                .requestMatchers("/h2-console/**").permitAll()
                .and()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .authenticationProvider(authenticationProvider);

        return http.build();
    }

}
